<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to Commute together</title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.selectricWrapper {
  position: relative;
  margin: 0 0 10px;
  width: 0px;
  cursor: pointer;
  float:left;
}
</style>
</head>
<body>

	<!-- Header Start Here -->
	<header>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 logo">
					<a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="top_links">
						<div class="login_register">
							<ul>
							</ul>
						</div>
						<div class="post_ride">
								<a onclick="javascript:alert('Login to start riding!'); return false;">Post Ride</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- Header End Here -->

	<!-- Recent Benefits Feedback Section Start Here -->
	<section class="about_me_main">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="bs-example">
						<div class="ItemHeader">
							<span class="ProfileHeader"> <span id="header_label">ABOUT
									ME</span></span>
						</div>
						<form enctype="multipart/form-data" role="form"
							class="form-horizontal" action="registerSecond" method="POST">
							<div class="form-section-wrapper">
								<div class="form_section_title">Basic details</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Body Type</label>
										<div class="col-sm-8">
											<select name="body" style="width:200px" id="bodyType" class="selectrik">
												<option value="-1" disabled selected>Select Body Type</option>
												<option value="0">Slim</option>
												<option value="1">Fat</option>
												<option value="2">Average</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Language</label>
										<div class="col-sm-8">
											<select  style="width:200px" id="language" multiple>
												<option value="0">English</option>
												<option value="1">Hindi</option>
												<option value="2">Telugu</option>
												<option value="3">Tamil</option>
												<option value="4">Kannada</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Ethnicity</label>
										<div class="col-sm-8">
											<select  style="width:200px"  name="ethnicity" id="ethnicity" class="selectrik">
												<option value="-1" disabled selected>Select Ethnicity</option>
												<option value="0">North Indian</option>
												<option value="1">South Indian</option>
												<option value="2">East Indian</option>
												<option value="3">West Indian</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group">
										<label class="col-sm-4 control-label photo-label" for="">Photo</label>
										<span class="col-sm-8 file-wrapper"> <img
											src="images/avatar.png" alt="photo" id="avatar"
											style="width: 100px; height: 100px;" /> <input type="file"
											name="photo" id="photo" onchange="javascript:previewImage();" />
											<span class="button upload_pic_btn">Upload Photo</span>
										</span>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-section-wrapper">
								<div class="form_section_title">Background Values</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Religion</label>
										<div class="col-sm-8">
											<select  style="width:200px"  name="religion" id="religion" class="selectrik">
												<option value="-1" disabled selected>Select Religion</option>
												<option value="0">Hindu</option>
												<option value="1">Christian</option>
												<option value="2">Muslim</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Religious
											Views</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" placeholder=""
												name="religiousViews" maxlength="40">
										</div>
									</div>
								</div>

							</div>
							<hr />
							<div class="form-section-wrapper">
								<div class="form_section_title">Life Style</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Diet</label>
										<div class="col-sm-8">
											<select  style="width:200px"  name="diet" id="diet" class="selectrik">
												<option value="-1" disabled selected>Select Diet</option>
												<option value="0">Vegetarian</option>
												<option value="1">Non-vegetarian</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-section-wrapper">
								<div class="form_section_title">Habbits While Travelling</div>
								<div class="col-xs-12 col-sm-12 col-md-12 row">
									<div class="col-xs-12 col-sm-3 col-md-3">
										<div class="form-group chlbx1">
											<div class="squaredFour">
												<input type="checkbox" value="1" id="chkTerms1" class="css-checkbox" 
													name="habitReading"> <label class="css-label"
													for="chkTerms1">Reading</label>
											</div><br/>
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms2" class="css-checkbox"
													name="habitSleeping"> <label
													class="css-label" for="chkTerms2">Sleeping</label>
											</div><br/>
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms3" class="css-checkbox"
													name="habitChat"> <label class="css-label"
													for="chkTerms3">Chit Chat</label>
											</div><br/>
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms4" class="css-checkbox"
													name="habitEating"> <label class="css-label"
													for="chkTerms4">Eating</label>
											</div><br/>
										</div>
									</div>

									<div class="col-xs-12 col-sm-4 col-md-3">
										<div class="form-group">
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms5" class="css-checkbox"
													name="habitSmoking"><label class="css-label"
													for="chkTerms5">Smoking</label>
											</div><br/>
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms6" class="css-checkbox"
													name="habitWorking"><label class="css-label"
													for="chkTerms6">Working</label>
											</div><br/>
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms7" class="css-checkbox"
													name="habitPhone"><label class="css-label"
													for="chkTerms7">Phone Calls</label>
											</div><br/>
											<div class="checkbox checkbox check-success">
												<input type="checkbox" value="1" id="chkTerms8" class="css-checkbox"
													name="habitMusic"><label class="css-label"
													for="chkTerms8">Listening Music</label>
											</div><br/>
										</div>
									</div>

									<div class="col-xs-12 col-sm-5 col-md-6">
										<div class="form-group">
											<label class="col-sm-4 control-label" for="">Likes</label>
											<div class="col-sm-8">
												<textarea rows="3" class="form-control" name="likes" style="resize:none" maxlength="512"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label" for="">Dislikes</label>
											<div class="col-sm-8">
												<textarea rows="3" class="form-control" name="dislikes" style="resize:none" maxlength="512"></textarea>
											</div>
										</div>
									</div>

								</div>

							</div>
							<hr />
							<div class="page_submit">
								<input type="button" value="Submit"
									onclick="javascript:doSubmit();">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Recent Benefits Feedback Section End Here -->

	<!-- Footer Start Here -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4 about_us">
					<h3>About Carpooling</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum fermentum placerat convallis. Quisque pharetra cursus
						enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate
						quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh.
						Aenean at quam faucibus lacus ullamcorper dapibus sit amet at
						neque. Etiam porttitor erat id libero dapibus, at dictum nulla
						tempus. <a href="Javascript:void(0)">More >></a>
					</p>
				</div>
				<div class="col-md-3">
					<div class="footer_links">
						<ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-5">
					<div class="follow_us">
						<div class="clearfix">
							<h3>Follow Us:</h3>
							<div class="social_icons">
								<ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="news_letter_main">
							<h3>Sign Up for Newsletter</h3>
							<input type="text" placeholder="Enter email address"
								name="newsletter"><input type="button" value="Sign Up">
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer End Here -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/checkbox.css" rel="stylesheet">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/moment.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/QapTcha.jquery.js"></script>
	<script src="js/custom.js"></script>
	<link href="css/selectric.css" rel="stylesheet">
	<script src="js/jquery.selectric.js"></script>
	<script src="js/jquery.multi-select.js"></script>
	<link href="css/multi-select.css" rel="stylesheet">
	<script>
	$(document).ready(function() {
		$("#language").multiSelect({}); 
		$('.selectrik').selectric({inheritOriginalWidth	:true});
	});
		function previewImage() {
			if (document.getElementById("photo").files[0].name.substring(
					document.getElementById("photo").files[0].name.length - 3,
					document.getElementById("photo").files[0].name.length) != "jpg"
					&& document.getElementById("photo").files[0].name
							.substring(
									document.getElementById("photo").files[0].name.length - 3,
									document.getElementById("photo").files[0].name.length) != "png") {
				document.getElementById('photo').value = '';
				$("#photo").val("");
				alert("Only JPG or PNG files please.");
				return;
			}
			var oFReader = new FileReader();
			setTimeout(function() {
				$("span").remove('.file-holder');
			}, 500);
			oFReader.readAsDataURL(document.getElementById("photo").files[0]);

			oFReader.onload = function(oFREvent) {
				document.getElementById("avatar").src = oFREvent.target.result;
			};
			setTimeout(function() {
				$("span").remove('.file-holder');
			}, 500);
		};
		
		function doSubmit() {
			var languages = "";
			var array = $('#language').val();
			if (array != null) {
				for (var i=0;i<array.length;i++) {
					  languages += array[i];
					  if (i < array.length - 1) {
						  languages += ",";
					  }
					}
			}
			$('form').append("<input type='hidden' name='language' value='" + languages + "'/>");
			$('form').submit();
		}
	</script>
</body>
</html>