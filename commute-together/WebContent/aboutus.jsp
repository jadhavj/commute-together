<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Header Start Here -->
    <header>
        <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 logo">
                <a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="top_links">
                    <div class="login_register">
                        <ul>
                            <li><a href="index.jsp">Login</a></li>
                            <li><a href="registration.jsp">Register</a></li>
                        </ul>
                    </div>
                    <div class="post_ride">
								<a onclick="javascript:alert('Login to start riding!'); return false;">Post Ride</a>
                    </div>
                </div>
            </div>
            </div>
        </div>  
    </header>
    <!-- Header End Here -->
    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">Home</a></li>
      <li class="active">About us</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
<div class="container">
<div class="row">
  
<div id="divMain" class="col-sm-8 col-md-8">
<h1 class="page-header">About Us</h1>
 
    <h3>Our Approach</h3>
    <p><strong>There are many variations of passages of Lorem Ipsum available.</strong></p>
    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don’t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn’t anything embarrassing hidden in the middle of text. </p>
    <h3>Our Team</h3>
    <div class="row">     
                <div class="col-md-2">                           
                    <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="images/services-image-1.jpg">   </div>          
                <div class="col-md-10">            
                    <p>Lorem ipsum dolor sit amet, consectetueradipiscing elied diam nonummy nibh euisod tincidunt ut laoreet dolore magna aliquam erat volutpatorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>       
            </div>
            <hr>
       
            <div class="row">     
                <div class="col-md-2">                           
                    <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="images/services-image-3.jpg">   </div>          
                <div class="col-md-10">              
                    <p>Lorem ipsum dolor sit amet, consectetueradipiscing elied diam nonummy nibh euisod tincidunt ut laoreet dolore magna aliquam erat volutpatorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>       
            </div>
            <hr>
            <div class="row">     
                <div class="col-md-2">                           
                    <img alt="" style="margin:5px 0px 15px;" class="img-polaroid" src="images/services-image-2.jpg">   </div>          
                <div class="col-md-10">              
                    <p>Lorem ipsum dolor sit amet, consectetueradipiscing elied diam nonummy nibh euisod tincidunt ut laoreet dolore magna aliquam erat volutpatorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                </div>       
            </div>
</div>
<div class="col-sm-4 col-md-4 sidebar aboutus-sidebar">

                    <div class="sidebox">
                        <h3 class="sidebox-title">Sample Sidebar Content</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and <a href="#">typesetting industry</a>.</p>
                        <p> Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s.</p>     
                    
                        <h4 class="sidebox-title">Sidebar Categories</h4>
                          <ul>
                            <li><a href="#">Quisque diam lorem sectetuer adipiscing</a></li>
                            <li><a href="#">Interdum vitae, adipiscing dapibus ac</a></li>
                            <li><a href="#">Scelerisque ipsum auctor vitae, pede</a></li>
                            <li><a href="#">Donec eget iaculis lacinia non erat</a></li>
                            <li><a href="#">Lacinia dictum elementum velit fermentum</a></li>
                            <li><a href="#">Donec in velit vel ipsum auctor pulvinar</a></li>
                          </ul>                 

                   
                          <h4 class="sidebox-title">Our Skills</h4>                    
                            
                                <h5>Web Design ( 40% )</h5>
                                <div class="progress progress-info">
                                <div style="width: 20%" class="bar"></div>
                                </div>                          
                            
                                <h5>Wordpress ( 80% )</h5>
                                <div class="progress progress-success">
                                <div style="width: 40%" class="bar"></div>
                                </div>                          
                            
                                <h5>Branding ( 100% )</h5>
                                <div class="progress progress-warning">
                                <div style="width: 60%" class="bar"></div>
                                </div>                          
                            
                                <h5>SEO Optmization ( 60% )</h5>
                                <div class="progress progress-danger">
                                <div style="width: 80%" class="bar"></div>
                                </div>          
                        
                            
                    </div>
                    
                </div>

</div>
</div>
    
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="privacy.jsp">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>