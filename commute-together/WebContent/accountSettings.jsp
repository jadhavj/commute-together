<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<%
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
		    %>
		    <script type="text/javascript">
		    alert("<%=entry.getValue()%>");
		    </script>
		    <%
		    break;
		}
	}
%>

    <!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->

 <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="dashboard.jsp">Home</a></li>
      <li class="active">Settings</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->




    <section class="search_results_main">
        <div class="container">
        <div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>
  <div id="#page-content-wrapper" class="col-sm-8 col-md-10">
          <div class="bs-example">
          <!-- start header title -->
           <div class="ItemHeader">
            <span class="ProfileHeader">
             <span id="header_label">Account Settings</span></span>
                </div>
                <!-- end header title -->
     <form role="form" class="form-horizontal acnt_stngs" action="updateSettings" method="post">
     <div class="form-section-wrapper">
       <div class="form_section_title">Security Settings</div>
  <div class="panel-group sec_sttng" id="accordion2" data-toggle="collapse">
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseEmail">
          Email
        </a>
        </h4>
      </div>
      <div id="collapseEmail" class="panel-collapse collapse">
        <div class="panel-body">
        <div class="row">
         
         <div class="settings_account_email">
         <div class="col-md-3 data">
         <label class="control-label">Current Email ID</label></div>
         <div class="col-md-9 data">
         <label class="control-label SettingsRemovablesLabel uiInputLabel" for="">${sessionScope.user.email}</label></div>
         <hr>
         <div class="col-md-3 data">
           <a href="#" class="newmail_link">Change Email Address</a>
         </div>
         <div class="clearfix"></div>
      <div class="hidden new-emailinput">
        <div class="col-md-3 data">
         <label for="" class="control-label">New Email</label>
         </div>
         <div class="col-md-6">
           <input type="email" class="form-control" id="" placeholder="Email" name="email">
         </div>
      
      </div>
      <div class="actn-sett-btns data">
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:changeEmail()">Save Changes</button>
      </div>
         </div>
         </div>
        </div>
      </div>
      </div>
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2"  href="#collapsePassword">
          Password
        </a>
        </h4>
      </div>
      <div id="collapsePassword" class="panel-collapse collapse">
        <div class="panel-body">        
                   <div class="form-group">
    <label for="" class="col-md-3 control-label">Current</label>
    <div class="col-md-6">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="currentPwd">
    </div>
  </div>
    <div class="form-group">
    <label for="inputPassword3" class="col-md-3 control-label">New</label>
    <div class="col-md-6">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="newPwd">
    </div>
  </div>
    <div class="form-group">
    <label for="inputPassword3" class="col-md-3 control-label">Retype new</label>
    <div class="col-md-6">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="confirmPwd">
    </div>
  </div>
           <hr> 
           <div class="actn-sett-btns data">
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:changePassword()">Save Changes</button>
      </div>    
        </div>
      </div>
      </div>
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapseMobileNo">
         Mobile Number 
        </a>
        </h4>
      </div>
      <div id="collapseMobileNo" class="panel-collapse collapse">
        <div class="panel-body">
                 <div class="row">
           <div class="settings_account_mobile">
         <div class="col-md-3 data">
         <label>Current Mobile No.</label></div>
         <div class="col-md-9 data">
         <label class="SettingsRemovablesLabel uiInputLabel" for="">${sessionScope.user.mobile}</label></div>
         <hr>
         <div class="col-md-3 data">
           <a href="#" class="newmail_link">Change Mobile No.</a>
         </div>
         <div class="clearfix"></div>
      <div class="hidden new-emailinput">
        <div class="col-md-3 data">
         <label for="" class="control-label">New Mobile No.</label>
         </div>
         <div class="col-md-6">
           <input type="text" class="form-control" id="" placeholder="Mobile No." name="mobile" onkeypress="return numbersonly(this, event)">
         </div>
      
      </div>
      <div class="actn-sett-btns data">
        <button class="btn btn-sm sav_btn" type="button" onclick="javascript:changeMobile()">Save Changes</button>
      </div>
         </div>
         </div>
        </div>
      </div>
      </div>
    </div>

     </div>
     <hr>
     <div class="form-section-wrapper">
       <div class="form_section_title">Remove Account</div>
         <div class="panel-group" id="accordion1">
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseDeActiv">
          DeActivate My Account
        </a>
        </h4>
      </div>
      <div id="collapseDeActiv" class="panel-collapse collapse">
        <div class="panel-body">
       
        <h4>DeActivating your account will:</h4>
         <ul class="norms-list">
           <li>Make you InActive for any type of matching</li>
           <li>Stop all system generated emails</li>
           <li>Enable you to ReActivate your account without re-entering your information in the future</li>
           <li>Your account will be automatically deleted if it remains DeActivated for one year</li>

         </ul> 
         <h4>Reason for leaving (Required):</h4>
         <ul class="radio-btn-list">
         <li class="radio">
        <label>
          <input type="radio" checked="" value="0" id="optionsRadios1" name="deactivateReason">
          My Account was hacked
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" checked="" value="1" id="optionsRadios2" name="deactivateReason">
          I have a privacy concern
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" value="1" id="optionsRadios3" name="deactivateReason">
          I dont feel safe to use CommuteTogether
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" checked="" value="2" id="optionsRadios4" name="deactivateReason">
          This is temporary, I wil be back
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" checked="" value="3" id="optionsRadios5" name="deactivateReason">
          I dont understand how to use Commute Together
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" checked="" value="4" id="optionsRadios6" name="deactivateReason">
          I dont find CommuteTogether usefull
        </label>
      </li>
      </ul>
     <h4>Please explain further:</h4>
     <textarea class="form-control" rows="3" name="deactivateMore"></textarea>
     <div class="checkbox checkbox check-success">
          <input type="checkbox" id="confirmDeactivate" value="1" name="confirmDeactivate">
        <label for="confirmDeactivate" class="checkbox-inline"> By checking this box, I hereby declare that I want my account to be DeActivated.</label></div>
        <div class="actn-sett-btns data">
        <button class="btn btn-sm sav_btn" type="button" onclick="javascript:deactivate()">Confirm</button>
      </div>
        </div>
      </div>
      </div>
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion1"  href="#collapseDelete">
          Delete My Account
        </a>
        </h4>
      </div>
      <div id="collapseDelete" class="panel-collapse collapse">
        <div class="panel-body">        
               <h4>Removing your account will:</h4>
         <ul class="norms-list">
           <li>Remove your UserID from the system</li>
           <li>If you wish to re-register in the future, you will need to re-enter all of your information again in full</li>
           
         </ul> 
         <h4>Reason for leaving (Required):</h4>
         <ul class="radio-btn-list">
         <li class="radio">
        <label>
          <input type="radio" checked="" value="0" id="optionsRadios1" name="deleteReason">
          My Account was hacked
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" checked="" value="1" id="optionsRadios2" name="deleteReason">
          I have a privacy concern
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" value="2" id="optionsRadios3" name="deleteReason">
          I dont feel safe to use CommuteTogether
        </label>
      </li>
      
      <li class="radio">
        <label>
          <input type="radio" checked="" value="3" id="optionsRadios4" name="deleteReason">
          I dont understand how to use Commute Together
        </label>
      </li>
      <li class="radio">
        <label>
          <input type="radio" checked="" value="4" id="optionsRadios5" name="deleteReason">
          I dont find CommuteTogether usefull
        </label>
      </li>
      </ul>
     <h4>Please explain further:</h4>
     <textarea class="form-control" rows="3" name="deleteMore"></textarea>
     <div class="checkbox checkbox check-success">
          <input type="checkbox" id="confirmDelete" value="1" name="confirmDelete">
        <label for="confirmDelete" class="checkbox-inline"> By checking this box, I hereby declare that I want my account to be Removed.</label></div>
        <div class="actn-sett-btns data">
        <button class="btn btn-sm sav_btn" type="button" onclick="javascript:doDelete()">Confirm</button>
      </div>

        </div>
      </div>
      </div>

    </div>

       </div>





   </form>

 </div>
  
</div>
</div>

 
  </div>
    </section>
    <!-- Recent Benefits Feedback Section End Here -->
    
    <!-- Footer Start Here -->
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enter email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    <link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css" media="screen" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/clone-form-td.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/scripts.js"></script>
    <script>
    $(document).ready(function () {
    	var tid = setTimeout(refreshEdit, 100);
    	
    	function refreshEdit() {
    		
    	}
    	
    });
    function doSubmit() {
    	$('form').submit();
    }

	function numbersonly(myfield, e, dec) {
		var key;
		var keychar;

		if (window.event)
			key = window.event.keyCode;
		else if (e)
			key = e.which;
		else
			return true;
		keychar = String.fromCharCode(key);

		if ((key==null) || (key==0) || (key==8) || 
	    (key==9) || (key==13) || (key==27) )
	   		return true;

		else if ((("0123456789").indexOf(keychar) > -1))
	   		return true;
		else if (dec && (keychar == ".")) {
			myfield.form.elements[dec].focus();
			return false;
		} else
	   		return false;
	}
	
	function changeEmail() {
		$("form").attr("action", "updateSettings?emailYes=true&");
		$("form").submit();
	}
	function changeMobile() {
		$("form").attr("action", "updateSettings?mobileYes=true&");
		$("form").submit();
	}
	function changePassword() {
		$("form").attr("action", "updateSettings?passwordYes=true&");
		$("form").submit();
	}
	function deactivate() {
		if (!$("#confirmDeactivate").is(':checked')) {
			alert("Please confirm you want to deactivate your account.");
			return;
		}
		$("form").attr("action", "updateSettings?deacYes=true&");
		$("form").submit();
	}
	function doDelete() {
		if (!$("#confirmDelete").is(':checked')) {
			alert("Please confirm you want to delete your account.");
			return;
		}
		$("form").attr("action", "updateSettings?delYes=true&");
		$("form").submit();
	}
</script>
  </body>
</html>