<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
		    %>
		    <script type="text/javascript">
		    alert("<%=entry.getValue()%>");
		    </script>
		    <%
		    break;
		}
	}
%>
    <jsp:include page="adminHeader.jsp"/>
	<form action="activateAccount" method="post">
		Activate account with e-mail <input type="text" name="email" />
		<input type="submit" value="Activate"/>
	</form>

</body>
</html>