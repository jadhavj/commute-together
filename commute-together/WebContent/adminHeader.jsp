<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<header>
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-6 logo">
				<a href="dashboard.jsp"><img src="images/logo.png"
					alt="Commute together" /></a>
			</div>
		</div>
	</div>
	<hr />
</header>
