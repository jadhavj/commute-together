<!DOCTYPE html>
<html lang="en">
<% 
    if (request.getAttribute("allClear") == null) { session.removeAttribute("user"); }
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
%>
<script type="text/javascript">
		    alert("<%=entry.getValue()%>");
</script>
<%
    break;
		}
	}
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
    function isEmail(email) {
    	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    	  return regex.test(email);
    	}
    function doSubmit() {
    	if ($("#fullName").val() == "") {
    		alert("Please enter your full name.")
    		return false;
    	}
    	if ($("#email").val() == "") {
    		alert("Please enter your email.")
    		return false;
    	}
    	if (!isEmail($("#email").val())) {
    		alert("Please enter a valid email.");
    		return false;
    	}
    	if ($("#subject").val() == "") {
    		alert("Please enter the subject for your inquiry.")
    		return false;
    	}
    	if ($("#message").val() == "") {
    		alert("Please enter the message you want to convey to us.")
    		return false;
    	}
    	
    	$("form").submit();
    	
    }
    </script>
  </head>
  <body>

    <!-- Header Start Here -->
    <div id="cotactus-page">
    <header>
        <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 logo">
                <a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="top_links">
                    <div class="login_register">
                        <ul>
                            <li><a href="index.jsp">Login</a></li>
                            <li><a href="registration.jsp">Register</a></li>
                        </ul>
                    </div>
                    <div class="post_ride">
								<a onclick="javascript:alert('Login to start riding!'); return false;">Post Ride</a>
                    </div>
                </div>
            </div>
            </div>
        </div>  
    </header>
    <!-- Header End Here -->
    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">Home</a></li>
      <li href="contactus.jsp" class="active">Contact us</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
<div class="container">
<div class="row">
<div class="hero-unit">
          <h1 class="page-header">Let's Keep In Touch!</h1>
          <p class="tagline">Thank you for visiting out little slice of the internet. If you would like to get into contact with our team simply fill out the nifty form below. Cheers!</p>  
        </div>
        <section class="col-md-6">

          <form class="contactus_form" role="form" method="post" action="contactUs">

            <div class="form-group">
            <label for="inputname"><i class="fa fa-user"></i> Full Name</label>
<input type="text" placeholder="Your name" class="form-control" name="fullName" id="fullName">
            </div>

    <div class="form-group">
    <label for="exampleInputEmail1"><i class="fa fa-envelope"></i> Email</label>
    <input type="email" class="form-control" placeholder="you@yourdomain.com" name="email" id="email">
  </div>

            <div class="form-group">
              <label for="inputsubject"><i class="fa fa-question-circle"></i> Subject</label>
                <input type="text" placeholder="what's up?" class="form-control" name="subject" id="subject">
            </div>

            <div class="form-group">
              <label for="inputmessage" class="control-label"><i class="fa fa-pencil"></i> Message</label>
                <textarea placeholder="What's on your mind?" class="form-control" rows="3" name="message" id="message"></textarea>      
            </div>

            <div class="control-group">
    <div class="controls">
      <button class="btn button" type="button" onclick="javascript:doSubmit()">Send Message</button>
    </div>

          
          <div class="clearfix"></div>


                
        </div></form></section>
        <section class="col-md-6">
          <div class="well">
             <iframe width="100%" height="350" frameborder="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Hyderabad+Telangan+India&amp;aq=&amp;sll=17.3660,78.4760&amp;sspn=17.3660,78.4760&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Hyderabad+Telangana+India&amp;z=11&amp;iwloc=A&amp;output=embed" marginwidth="0" marginheight="0" scrolling="no"></iframe>
          </div><!--end well -->
        </section>
  

</div>
</div>
    
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="privacy.jsp">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    </div>
    <!-- Footer End Here -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>