<%@page import="commutetogether.data.Ridesearches"%>
<%@page import="commutetogether.data.Createdrides"%>
<%@page import="commutetogether.dao.RidesDAO"%>
<%@page import="commutetogether.dao.MessagesDAO"%>
<%@page import="commutetogether.models.Message"%>
<%@page import="commutetogether.data.User"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<%
	String moreString = request.getParameter("more");
	int more = 6;
	if (moreString != null && !moreString.isEmpty()) {
	    more = Integer.parseInt(moreString);
	}
	int nextMore = more + 3;
	if (nextMore > RidesDAO.findLatestRides().size()) {
	    nextMore = RidesDAO.findLatestRides().size();
	}
	
	User user = (commutetogether.data.User) session.getAttribute("user");
	java.util.List<Createdrides> requests = RidesDAO.findLatestRides(more);
	request.setAttribute("recent", requests);
	java.util.List<Ridesearches> searches = RidesDAO.findLatestSearches(6);
	request.setAttribute("searches", searches);
	java.util.List<Createdrides> pools = RidesDAO.findLatestPools();
	request.setAttribute("pools", pools);
	List<Createdrides> created = RidesDAO.getRidesCreatedByAndJoinedBy(user.getEmail());
	request.setAttribute("created", created);
	Integer rideId = (Integer) request.getAttribute("rideId");
	if (rideId == null && request.getParameter("rideId") != null) {
	    rideId = Integer.parseInt(request.getParameter("rideId"));
	} if (rideId == null && created.size() != 0) {
	    rideId = created.get(0).getId();
	}
    request.setAttribute("rideId", rideId);
	if (!created.isEmpty()) {
		List<Message> messages = MessagesDAO.getAllMessagesForRide(rideId);
		request.setAttribute("messages", messages);
    }
	
	int hyd = RidesDAO.findHyderabadRides().size();
	int ben = RidesDAO.findBengaluruRides().size();
	int che = RidesDAO.findChennaiRides().size();
	request.setAttribute("hyd", hyd);
	request.setAttribute("ben", ben);
	request.setAttribute("che", che);
%>
<!DOCTYPE html>
<html lang="en">
  <head>
	<link href="css/selectric.css" rel="stylesheet">
	<script src="js/jquery.selectric.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <%
    	
    %>
<style>
.selectricWrapper {
  position: relative;
  margin: 0 0 10px;
  width: 0px;
  cursor: pointer;
  float:left;
}
</style>
<script>
$(document).ready(function() {
	$('select').selectric({inheritOriginalWidth	:true});
});
</script>
  </head>
  <body>

    <!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->

 <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
      <li class="active">Dashboard</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->




    <section class="search_results_main">
        <div class="container">
        <div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-3 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>
        <!-- Message From Admin Start Here -->
        <div class="message_admin">
            <h3>Message from Admin</h3>
            <p>Sed ut perspiciatis unde omnis are iste natus error sit voluptatem the accusantium.
dratactual befort nostbside.</p>
        </div>
        <!-- Message From Admin End Here -->

         <!-- Feedback Start Here -->
        <div class="feedback_main">
            <h3>Feedback</h3>
            <div class="note_main">  
                <p>Sed ut perspiciatis unde omnis are iste natus error.</p>
                <p class="feed_name">Jonthan</p>
            </div>
             <div class="note_main">  
                <p>Sed ut perspiciatis unde omnis are iste natus error.</p>
                <p class="feed_name">Philip</p>
            </div>
             <div class="note_main">  
                <p>Sed ut perspiciatis unde omnis are iste natus error.</p>
                <p class="feed_name">sourabh</p>
            </div>
             <div class="note_main">  
                <p>Sed ut perspiciatis unde omnis are iste natus error.</p>
                <p class="feed_name">Vinay Kumar</p>
            </div>
        </div>
        <!-- Feedback End Here -->
  </div>  
 </div>
<!-- Middle Section Start Here -->
<div class="col-sm-6 col-md-7">
    <div class="middle_section">
      <!-- User Message Start Here -->
      <div class="user_message_main">
          <h2>Hello ${user.fname }</h2>
		</div>
		<c:if test="${not empty created }">
		<div style="border:1px solid #d3d3d3;padding:5px" class="re_ben_fed user_list">
      <form action="createMessage" method="GET">
      <div>
		  <c:if test="${rideId ne null}">
          <p>Message to your co passengers:</p>
          	<select style="width:100%" onchange="getMessages()" name="rideId">
			<c:forEach items="${created}" var="ride">
				<option value="${ride.id}" <c:if test="${rideId eq ride.id}">selected</c:if>>${ride.origin} to ${ride.destination}</option>	 
            </c:forEach>
          	</select>
          	</c:if>
      </div>
      <!-- User Message End Here -->

      <!-- User List Start Here -->
		  <c:if test="${rideId ne null}">
      <div class="user_list">
          <div class="lr_space" style="overflow-y:auto;height:100px" id="messages">
          	  <c:forEach items="${messages}" var="message">
              <div class="user_cmm">
                  <div class="user_image">
                      <img id="" alt="" src="${pageContext.request.contextPath}/imageServlet?email=${message.fromUser.email}" class="" height="19" width="19">
                  </div>
                  <div class="user_details">
                      <p class="user_comm">${message.messages.message }</p>
                  </div>
              </div>
              </c:forEach>
          </div>
          <br/>
              <div class="comment_post">
                  <textarea class="comment_post_txt" style="resize:none" name="message" id="message"></textarea>
              </div>
              <p class="reply"><a href="javascript:return false;" onclick="javascript:postRideMessage()">Reply</a></p>
      </div>
      </c:if>  
	  </form>
	  </div>
	  </c:if>
      <!-- User List End Here -->

      <!-- Recent Post Rider Start Here -->
      <div class="riders_list">
            <h2>Recently Posted Rides</h2>
            <div class="re_ben_fed recent_post_main">
			<c:forEach items="${recent}" var="ride"> 
                <div class="clearfix tpad10">
                    <div class="rides_name">${ride.user.fname} ${ride.user.lname}</div>
                    <div class="street_name">${ride.origin} <fmt:formatDate pattern="(hh:mm a)" value="${ride['starttime']}"/></div><div class="l_r_arrow"><img src="images/l_r_arrow.png" alt="Commute together"></div><div class="street_name">${ride.destination} <fmt:formatDate pattern="(hh:mm a)" value="${ride['returntime']}"/></div>
                </div>
                </c:forEach>
                <div align="right">
                <a href="dashboard.jsp?more=<%=nextMore%>">More</a> | <a href="dashboard.jsp?more=6">Less</a>
                </div>
            </div>
        </div>
        <!-- Recent Post Rider End Here -->

        <!-- Recent Searched Rides Start Here -->
      <div class="riders_list">
            <h2>Recently Searched Rides</h2>
            <div class="re_ben_fed recent_post_main">
			<c:forEach items="${searches}" var="search"> 
                <div class="clearfix tpad10">
                    <div class="rides_name">${search.createdrides.user.fname} ${search.createdrides.user.lname}</div>
                    <div class="street_name">${search.createdrides.origin} <fmt:formatDate pattern="(hh:mm a)" value="${search.createdrides['starttime']}"/></div><div class="l_r_arrow"><img src="images/l_r_arrow.png" alt="Commute together"></div><div class="street_name">${search.createdrides.destination} <fmt:formatDate pattern="(hh:mm a)" value="${search.createdrides['returntime']}"/></div>
                </div>
                </c:forEach>
            </div>
        </div>
        <!-- Recent Searched Rides End Here -->

        <!-- Latest Pools Start Here -->
      <div class="riders_list">
            <h2>Latest Pools</h2>
            <div class="re_ben_fed recent_post_main">
            <c:set var="i" value="${0}"/>
			<c:forEach items="${pools}" var="pool"> 
                <div class="clearfix tpad10">
                <c:if test="${i < hyd}">
                    </c:if>
                <c:if test="${i < (hyd + ben -2)}">
                    </c:if>
                <c:if test="${i < (hyd + ben + che - 3)}">
                    </c:if>
                <div class="clearfix tpad10">
                    <div class="rides_name">${pool.user.fname} ${pool.user.lname}</div>
                    <div class="street_name">${pool.origin} <fmt:formatDate pattern="(hh:mm a)" value="${pool['starttime']}"/></div><div class="l_r_arrow"><img src="images/l_r_arrow.png" alt="Commute together"></div><div class="street_name">${pool.destination} <fmt:formatDate pattern="(hh:mm a)" value="${pool['returntime']}"/></div>
                </div>
                </div>
            <c:set var="i" value="${i+1}"/>
	                </c:forEach>
            </div>
        </div>
        <!-- Latest Pools Start Here -->
    </div>
</div>
<!-- Middle Section End Here -->

<!-- Right Side Section Start Here -->
<div class="col-sm-3 col-md-3">
  <div class="right-sidebar">
      <!-- Find a Ride Start Here -->
      <div class="find_ride_form">
        <div class="finda_ride_steps"><img src="images/findaride_steps.png" alt="Banner"></div>
        <div class="ride_form">
            <select id="city">
                <option>Hyderabad</option>
                <option>Mumbai</option>
                <option>Bangalore</option>
                <option>Chennai</option>
                <option>Kolkata</option>
                <option>Lucknow</option>
                <option>Trivandrum </option>
                <option>New Delhi</option>
                <option>Pune</option>
            </select>
            <input name="Starting Point" placeholder="Starting Point" type="text" id="origin">
            <input name="Destination" placeholder="Destination" type="text" id="destination">
            <input class="submit_btn" value="Find" type="button" onclick="findRides()">
        </div>
      </div>
        <!-- Find a Ride End Here -->
      <!-- Face Book Block Start Here -->
      <div class="face_book_block">
      <br/>
<div class="fb-page" data-href="https://www.facebook.com/pages/Commute-Together/637377536390247" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pages/Commute-Together/637377536390247"><a href="https://www.facebook.com/pages/Commute-Together/637377536390247">Commute Together</a></blockquote></div></div></div>
      <!-- Face Book Block End Here -->

      <!-- Twitter Block Start Here -->
      <div class="twitter_block">
<a class="twitter-timeline" href="https://twitter.com/cmttogether" data-widget-id="575006446137421825">Tweets by @cmttogether</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>   </div> 
</div>
<!-- Right Side Section End Here -->

  </div>
    </section>
    <!-- Recent Benefits Feedback Section End Here -->
    
    <!-- Footer Start Here -->
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="Javascript:void(0)">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enter email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    <link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css" media="screen" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dashboard.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/clone-form-td.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/scripts.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.view_msg a').click(function(){
                $('.user_list').fadeIn(1000);
            });
			$('select').selectric({inheritOriginalWidth	:true});
        });
        
        function findRides() {
			window.location.href = "searchRides?city="+ $("#city").val() + "&startLoc=" + $("#origin").val() + "&endLoc=" + $("#destination").val();         	
        }
        $("#messages").scrollTop($("#messages")[0].scrollHeight);
        
        function postRideMessage() {
        	if ($("#message").val() == null || $("#message").val().trim() == "") {
        		return;
        	}
       		$("form").submit(); 	
        }  
        function getMessages() {
       		$("form").attr("action", "dashboard.jsp");
       		$("form").submit(); 	
        }  
    </script>
  </body>
</html>