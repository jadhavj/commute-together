<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Header Start Here -->
    <header>
        <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 logo">
                <a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="top_links">
                    <div class="login_register">
                        <ul>
                            <li><a href="index.jsp">Login</a></li>
                            <li><a href="registration.jsp">Register</a></li>
                        </ul>
                    </div>
                    <div class="post_ride">
								<a onclick="javascript:alert('Login to start riding!'); return false;">Post Ride</a>
                    </div>
                </div>
            </div>
            </div>
        </div>  
    </header>
    <!-- Header End Here -->
    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">Home</a></li>
      <li class="active">FAQ</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
<div class="container">
<div class="row">
<div id="faqs-main" class="col-md-12">
<h1 class="page-header">Frequently Asked Questions</h1>
<div class="form-section-wrapper">
<div class="form_section_title">All you need to know about car-sharing</div>
         <div class="panel-group" id="accordion-faqs1">
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-faqs1" href="#question1">
          What is car-sharing?
        </a>
        </h4>
      </div>
      <div id="question1" class="panel-collapse collapse">
        <div class="panel-body">
       

<p>Car-sharing is when there is more than one occupant in a private car. We all share cars regularly, with our friends and family, without thinking about it. But there are often times when a driver has empty seats in the car simply because they don't know of anyone who needs a lift.</p>

<p>CommuteTogether enables organised car-sharing by connecting people travelling in the same direction so they can arrange to travel together and share the costs, whilst reducing congestion and pollution at the same time.</p>

        
        
        </div>
      </div>
      </div>
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-faqs1" href="#question2">
          How can I get involved?
        </a>
        </h4>
      </div>
      <div id="question2" class="panel-collapse collapse">
        <div class="panel-body">        
               
        <p>The service is available to anyone over 18, commuters, students, football supporters, festival goers, tourists, employees - anyone!</p>
         <p>Joining the scheme is simple and completely FREE. All you have to do is <a href="#" class="">register</a> your details and then activate your account. You do this by clicking on the link that we send to you on the account activation email.</p>
         <p>Once you've registered your journey, the database will search for possible mat</p>

        </div>
      </div>
      </div>

    </div>
    </div>
    <div class="form-section-wrapper">
<div class="form_section_title">Safety and Trust</div>
         <div class="panel-group" id="accordion-faqs2">
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-faqs2" href="#question3">
          How safe is car sharing?
        </a>
        </h4>
      </div>
      <div id="question3" class="panel-collapse collapse">
        <div class="panel-body">
       

<p>The safety of our members is a priority for Commutetogether and we have made our website as secure as we can. All members' details are stored securely in the database and only members intended travel information can be accessed online.</p>
            <p>When it comes to travelling, every member is responsible for his or her own safety. However, we do recommend that members follow some simple security measures outlined below:</p>
            <ul>
                <li>Avoid exchanging home addresses with your travelling companion before you meet them</li>
                <li>Arrange to meet in a public place</li>
                <li>Inform a friend or family member of whom you will be travelling with, when and to where</li>
                <li>Make sure you show each other your IDs - staff passes, passports, or driving licences - so you know you're travelling with the right person</li>
                <li>You are under no obligation to go ahead with any car-share. If you have any doubts about your travelling companion, for any reason, you should avoid travelling with them</li>
            </ul>
            <p>For more information about safety and security of personal information please read <a href="javascript:showTerms()">Terms and Conditions</a> and the <a href="privacy.jsp">Privacy Policy</a>.</p>
        
        </div>
      </div>
      </div>
      <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-faqs2" href="#question4">
          Will my e-mail address be visible to other users?
        </a>
        </h4>
      </div>
      <div id="question4" class="panel-collapse collapse">
        <div class="panel-body">        
               
        <p>No, your email address is kept hidden at all times. The only information that is visible to other users is your name and the journey details you added.</p>

        </div>
      </div>
      </div>

    </div>
    </div>

    
</div>

</div>
</div>
    
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="privacy.jsp">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
	<script src="js/jquery.bpopup.min.js"></script>
	<div style="display:none;background-color:white;width:400px;height:400px;padding:20px" class="re_ben_fed user_list tandc">
<div id="popup" style="width:370px;height:310px;overflow-y:auto" >
<pre>
Terms and Conditions

GENERAL

Servicing servicing resistor video computer element transponder prototype analog solution mainframe network. Solution video debugged feedback sampling extended, infrared, generator generator. 

* list item number one
* list item number two
* list item number three

Harmonic gigabyte generator in sequential interface. Services, integer device read-only supporting cascading cache capacitance. Proxy boolean solution or data overflow element overflow processor arrray video, reflective extended. Gigabyte debugged distributed, reflective controller disk encapsulated phase network arrray feedback element cache high silicon. Sampling backbone analog remote adaptive extended bridgeware n-tier integer audio femtosecond. 

IMPORTANT STUFF

Interface broadband developer backbone fragmentation messaging software transmission, sampling cascading element high silicon backbone. Reflective servicing coordinated boolean connectivity extended inversion sequential for. Data distributed backbone bridgeware connectivity logarithmic, sampling ethernet for. Extended logarithmic log video echo gigabyte. Or procedural phaselock sequential port extended deviation sequential, disk recognition gigabyte phaselock proxy messaging arrray. Encapsulated echo deviation boolean system ethernet reducer, pc. 

Backbone frequency echo ethernet patch femtosecond sampling, integer digital floating-point n-tier dithering disk. Processor reflective boolean includes phase deviation, with log, integral logistically femtosecond, in, integral infrared phaselock. Element developer pc harmonic, plasma with, integral floating-point. Reflective feedback infrared, echo anomoly integral generator reflective led, high overflow servicing gigabyte. Extended integral data, infrared frequency disk broadband transistorized processor disk anomoly, sampling potentiometer kilohertz. Bus overflow, bridgeware fragmentation bypass prototype, interface, for internet pc generator integral, bypass disk, frequency. 

FEES

Sequential ENCAPSULATED RESISTOR element converter pc harmonic. Feedback cache hyperlinked debugged generator infrared device network, fragmentation connectivity, debugged. For, dithering interface debugged sequential recognition bypass bridgeware processor backbone sampling logarithmic software. Anomoly infrared logarithmic logarithmic procedural plasma resistor integral silicon deviation. Computer broadband plasma, encapsulated cable kilohertz scan. With logarithmic bridgeware indeterminate sampling, port. Development procedural hyperlinked mainframe, encapsulated boolean software processor transistorized cable with backbone. 

Reflective backbone log transmission cable logarithmic mainframe. Messaging backbone debugged feedback development phaselock metafile n-tier coordinated, cable coordinated coordinated in. Analog device transistorized, sequential transistorized with scalar normalizing. Mainframe phase cache anomoly feedback servicing harmonic. Services software pc, femtosecond bypass scalar data element extended cascading capacitance harmonic. Adaptive plasma bypass supporting cascading deviation system pulse broadband bridgeware. 

AGREEMENT

Servicing hyperlinked analog encapsulated disk cable with, capacitance element partitioned potentiometer read-only dithering. Normalizing kilohertz network solution logarithmic device bus connectivity system mainframe prototype phaselock phase cache. 

Coordinated transmission overflow floating-point distributed supporting scan converter. Bridgeware backbone coordinated sampling overflow broadband normalizing connectivity or, internet transmission, bus, encapsulated. Extended element element echo messaging scan, cache video deviation debugged broadband. Pc, supporting hyperlinked coordinated patch, bridgeware transistorized, led frequency scalar. Servicing, plasma recursive boolean includes, computer, femtosecond inversion. Services integer harmonic mainframe scalar prototype broadband n-tier. Echo software application indeterminate dithering bypass plasma bus harmonic patch. Digital, mainframe scalar anomoly bypass, bus potentiometer phaselock plasma, phaselock prompt data disk inversion. 


</pre>
    </div>	<br/>
    <div align="center" style="width:100%">
        <span class="button b-close"><span>Close</span></span>
    </div>
    </div>
  </body>
  <script>
	function showTerms() {
		$('.tandc').bPopup({
            modalClose: false,
            opacity: 0.6,
            positionStyle: 'fixed' //'fixed' or 'absolute'
        });			
	}
</script>
</html>