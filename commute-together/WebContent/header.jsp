<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
	commutetogether.data.User user = (commutetogether.data.User) request.getSession().getAttribute("user");
	java.util.List<commutetogether.models.Message> latest = commutetogether.dao.MessagesDAO.getMessagesFor(user.getEmail(), 3);
	request.setAttribute("latests", latest);
	java.util.List<commutetogether.models.RideRequest> requests = commutetogether.dao.RidesDAO.findLatestRequests(user.getEmail(), 3);
	request.setAttribute("requests", requests);
	java.util.List<commutetogether.models.RideRequest> subs = commutetogether.dao.RidesDAO.findLatestSubscriptions(user.getEmail(), 3);
	request.setAttribute("subs", subs);
	java.util.List<commutetogether.models.RideRequest> joinies = commutetogether.dao.RidesDAO.findLatestJoinies(user.getEmail(), 3);
	request.setAttribute("joinies", joinies);
%>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />

<script>
function acceptRequest(id) {
	$("form").attr("action", "updateRequest?id=" + id + "&action=approve");
	$("form").submit();
}

function rejectRequest(id) {
	$("form").attr("action", "updateRequest?id=" + id + "&action=reject");
	$("form").submit();
}

function sendMessage(email) {
	
	$("form").attr("action", "newMessage?toUser="+ email);
	$("form").submit();
}

function updateReadNotifications() {
	$(".notification").html("");
	$.ajax({
		  url: "updateNotificationsRead?redirectUrl=" + "${uri}"
		});
}
</script>
<style>
.pos {
    position:relative;
}

.notification {
    position:relative;
    right:-15px;
    top:-7px;
    background-color:#FF6600;
    color:white;
    line-height:20px;
    width:20px;
    height:20px;
    border-radius:10px;
    font-size:12px;
}
</style>
    <header>
    <form action="" method="post">
        <div class="container">
        <div class="row">
        
            <div class="col-xs-12 col-sm-6 logo">
           <a href="dashboard.jsp"><img src="images/logo.png" alt="Commute together" /></a>
            </div>
            <div class="col-xs-12 col-sm-6" style="float:left">
               <div class="pull-right" style="float:left">
               <ul class="header_links">
               <li>
                    <div id="" class="userRequests">

                    <span><a href="#" class="dropdown-toggle iconset user-icon pos" id="my-user-list" data-placement="bottom" data-content="" data-toggle="dropdown" data-original-title=""></a>
                    <c:if test="${not empty requests}">
                    <span class="notification">&nbsp;${fn:length(requests)}&nbsp;</span>
                    </c:if>
                    </span>  

          <div id="subscriber-list" style="display:none">
          <div class="user-list-header">
          <h3 class="header-title">Join Requests</h3>
          </div>
            <ul class="usr_lst sbcbr_lst">
            <c:if test="${empty requests}"><li align="center">No Join Requests yet.</li></c:if>
	<c:forEach items="${requests}" var="request"> 
            <li>
              <!-- BEGIN MESSAGE -->
              <div class="notification-messages info">
                <div class="user-profile">
                  <img src="${pageContext.request.contextPath}/imageServlet?email=${request.requester.email}" alt="" data-src="assets/img/profiles/d.jpg" data-src-retina="assets/img/profiles/d2x.jpg" width="35" height="35">
                </div>
                <div class="pfname-wrapper">
                  <span class="user-profile-name">${request.requester.fname} ${request.requester.lname}
                  </span> 
                        
                
                <div class="btns-wrapper">
                  <button name="" class="btn btn-default btn-accept" onclick="javascript:acceptRequest(${request.request.id})">Accept</button>
                  <button name="" class="btn btn-default btn-white" onclick="javascript:rejectRequest(${request.request.id})">Reject</button>
                  </div>
                  </div>
                
                <div class="clearfix"></div>                  
              </div>
              <!-- END MESSAGE -->  
            </li>
            </c:forEach>
            </ul>
            <div class="user-list-header">
          <h3 class="header-title sub-usr-ttl">Subscribed Users</h3>
          </div>
            <ul class="usr_lst user-sbscr">
            <c:if test="${empty subs}"><li align="center">No Subscriptions yet.</li></c:if>
	<c:forEach items="${subs}" var="sub"> 
            <li>
<!--             <div class="remove-btn">
                <a href="#" ><span class="glyphicon glyphicon-remove"></span></a>
                  </div>
 -->              <!-- BEGIN MESSAGE -->
              <div class="notification-messages info">
                <div class="user-profile">
                  <img src="${pageContext.request.contextPath}/imageServlet?email=${sub.requester.email}" alt="" data-src="assets/img/profiles/d.jpg" data-src-retina="assets/img/profiles/d2x.jpg" width="35" height="35">
                </div>
                <div class="pfname-wrapper">
                  <span class="user-profile-name">${sub.requester.fname} ${sub.requester.lname}
                  </span> 
                        
                
                <div class="btns-wrapper">
                  <button onclick="javascript:sendMessage('${sub.requester.email}')" class="btn btn-default btn-accept mesg-btn">Send Message</button>
                  
                  </div>

                  
                  </div>
                
                <div class="clearfix"></div>                  
              </div>
              <!-- END MESSAGE -->  
            </li>
            </c:forEach>
            </ul>
                  
          </div>
          <!-- END NOTIFICATION CENTER -->

                    </div>
                    </li>
            <li>
                    <div id="" class="messages-toggle">
                    <a href="#" class="dropdown-toggle iconset message-icon pos" id="my-message-list" data-placement="bottom" data-content="" data-toggle="dropdown" data-original-title="Messages"></a>  
                    <c:if test="${not empty latests}">
                    <span class="notification">&nbsp;${fn:length(latests)}&nbsp;</span>
                    </c:if>
            

          <div id="message-list" style="display:none">
          <div class="mesg-list-header">
          <a href="javascript:window.location.href='newMessage'" class="uiheaderactn">Send a new message</a>
          </div>
            <ul class="mesg_lst">
	<c:forEach items="${latests}" var="latest"> 
            <li>
              <!-- BEGIN MESSAGE -->
              <div class="notification-messages info" onclick="javascript:window.location.href='viewMessage?id=${latest.messages.id}'">
                <div class="user-profile">
                  <img src="${pageContext.request.contextPath}/imageServlet?email=${latest.fromUser.email}" alt="" width="35" height="35">
                </div>
                <div class="message-wrapper">
                  <div class="heading">${latest.fromUser.fname} ${latest.fromUser.lname}</div>
                  <div class="description">${latest.messages.subject}</div>
                  <div class="date pull-left"><fmt:formatDate pattern='dd/MM/yyyy' value='${latest.messages.createdtime}'/></div>                    
                </div>
                <div class="clearfix"></div>                  
              </div>
              <!-- END MESSAGE -->  
            </li>
            </c:forEach>
            </ul>
          </div>
          <!-- END NOTIFICATION CENTER -->
                    </div>
        </li>
        <li>
                    <div id="" class="userRequests">
                    <a href="#" onclick="javascript:updateReadNotifications()" class="dropdown-toggle iconset notifications-icon" id="my-notify-list" data-placement="bottom" data-content="" data-toggle="dropdown" data-original-title="Notifications"></a>
                    <c:if test="${not empty joinies}">
                    <span class="notification">&nbsp;${fn:length(joinies)}&nbsp;</span>
                    </c:if>
                      

          <div id="notification-list" style="display:none" style="cursor:pointer">
          <ul class="ntfctn_lst" style="cursor:pointer">
	<c:forEach items="${joinies}" var="joinee"> 
            <li>
              <!-- BEGIN NOTIFICATION MESSAGE -->
              <div class="notification-messages info" style="cursor:pointer">
                <div class="user-profile">
                  <img src="${pageContext.request.contextPath}/imageServlet?email=${joinee.request.user.email}" alt="" data-src="assets/img/profiles/d.jpg" data-src-retina="assets/img/profiles/d2x.jpg" width="35" height="35">
                </div>
                <div class="message-wrapper" style="cursor:pointer">
                  <div class="heading" style="cursor:pointer">Request Approved</div>
                  <div class="description" style="cursor:pointer">${joinee.request.createdrides.origin} to ${joinee.request.createdrides.destination}</div>
                </div>
                <div class="clearfix"></div>                  
              </div>
              <!-- END NOTIFICATION MESSAGE --> 
            </li> 
	</c:forEach>
            </ul>         
          </div>
          <!-- END NOTIFICATION CENTER -->
                    </div>
                    </li>





  						<li class="user-details"><a title="Timeline" href="#"
							class=""> <img id="" width="29" height="29" alt=""
								src="${pageContext.request.contextPath}/imageServlet?email=${sessionScope.user.email}"
								class=""> <span class="profile-name"><c:out
										value="${sessionScope.user.fname}" /> <c:out
										value="${sessionScope.user.lname}" /></span>
						</a></li>
						<li class="quicklinks"><a data-toggle="dropdown"
							class="dropdown-toggle pull-right" href="#" id="user-options">
								<div class="iconset caret"></div>
						</a>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a href="accountSettings.jsp">Settings</a></li>
								<li class="divider"></li>
								<li><a href="logout"> <i class="fa fa-power-off"></i>&nbsp;&nbsp;Log
										Out
								</a></li>
							</ul></li>

<!-- END SETTINGS -->
                </ul>
               </div> 
            </div>
							<div class="post_ride" align="right">
								<a href="rideDetails.jsp">Post Ride</a>
							</div>
            </div>
        </div>
        </form>  
    </header>
<script>
$(".popover").attr("style", "cursor:pointer");
</script>