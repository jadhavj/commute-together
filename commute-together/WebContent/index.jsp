<%@page import="commutetogether.data.Createdrides"%>
<%@page import="commutetogether.dao.RidesDAO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
<c:set var="rememberMe" value="${cookie.email.value}" />
<%
	java.util.List<Createdrides> requests = RidesDAO.findLatestRides(6);
	request.setAttribute("recent", requests);
	if (request.getParameter("success") != null && request.getParameter("success").equals("true")) {
	    Cookie rememberMe = new Cookie("email", "");
	    response.addCookie(rememberMe);
	    %>
<script type="text/javascript">
	    alert("Registration successful. Please check your email for your password.");
	    </script>
	    <%
	}
if (request.getParameter("login") != null && request.getParameter("login").equals("true")) {
    %>
<script type="text/javascript">
    alert("Your session has expired. Please login.");
    </script>
    <%
}

    if (request.getAttribute("allClear") == null) { session.removeAttribute("user"); }
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
%>
<script type="text/javascript">
		    alert("<%=entry.getValue()%>");
</script>
<%
    break;
		}
	}
%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to Commute together</title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<!-- Header Start Here -->
	<header>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 logo">
					<a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
				</div>
				<form action="login" method="post">
					<div class="col-xs-12 col-sm-6">
						<div class="top_links">
							<div class="login_register" style="width: 650px" align="left">
								<ul>
									<li><input type="text" required="required"
										class="form-control" id="email" placeholder="Email"
										name="email" value="${cookie.email.value}"></li>
									<li><div id="passwordDiv">
											<input type="password" required="required"
												class="form-control" id="password" placeholder="Password"
												name="password">
										</div></li>
									<li><div>
											<a href="" onclick="javascript:doLogin();return false;">Login</a>
										</div></li>
									<li><div>
											<a href="registration.jsp">Register</a>
										</div></li>
									<li><div>
											<a href=""
												onclick="javascript:forgotPassword(); return false;">Forgot
												Password?</a>
										</div></li>
								</ul>
							</div>
							<div align="left"
								class="col-xs-12 col-sm-8 checkbox checkbox check-success">
								<input type="checkbox" value="1" id="chkTerms1" class="css-checkbox"
									name="rememberMe"> <label class="css-label"
									for="chkTerms1" value="${cookie.email.value}"
									<c:choose><c:when test="${not empty rememberMe}">checked</c:when></c:choose>>Remember
									me</label>
							</div>
							<div class="post_ride">
								<a onclick="javascript:alert('Login to start riding!'); return false;">Post Ride</a>
							</div>
						</div>

					</div>
				</form>
			</div>
		</div>
	</header>
	<!-- Header End Here -->

	<!-- Banner Start Here -->
	<section class="banner_form">
		<div class="">
			<div class="col-md-8">
				<div class="banner">
					<div id="carousel-example-generic" class="carousel slide"
						data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0"
								class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="item active">
								<img src="images/banner/banner_one.jpg" alt="Banner">
								<!-- <div class="carousel-caption">
                          </div> -->
							</div>
							<div class="item">
								<img src="images/banner/banner_one.jpg" alt="Banner 2">
								<!-- <div class="carousel-caption">
                          </div> -->
							</div>
							<div class="item">
								<img src="images/banner/banner_one.jpg" alt="Banner 3">
								<!-- <div class="carousel-caption">
                          </div> -->
							</div>

						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic"
							role="button" data-slide="prev"> <span
							class="glyphicon glyphicon-chevron-left"></span>
						</a> <a class="right carousel-control"
							href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="find_ride_main">
					<div class="finda_ride_steps">
						<img src="images/findaride_steps.png" alt="Banner">
					</div>
					<div class="ride_form">
						<select>
							<option>Hyderabad</option>
							<option>Mumbai</option>
							<option>Bangalore</option>
							<option>Chennai</option>
							<option>Kolkata</option>
							<option>Lucknow</option>
							<option>Trivandrum</option>
							<option>New Delhi</option>
						</select> <input type="text" name="Starting Point"
							placeholder="Starting Point"> <input type="text"
							name="Destination" placeholder="Destination"> <input
							type="button" class="submit_btn" value="Find" onclick="javascript:showLogin()">
					</div>
				</div>
			</div>
		</div>

	</section>
	<!-- Banner Start Here -->

	<!-- Recent Benefits Feedback Section Start Here -->
	<section class="re_be_fe_main">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h2>Recently Posted Rides</h2>
					<div class="re_ben_fed recent_post_main">
			<c:forEach items="${recent}" var="ride"> 
						<div class="clearfix tpad10">
                    <div class="street_name">${ride.origin} <fmt:formatDate pattern="(hh:mm a)" value="${ride['starttime']}"/></div><div class="l_r_arrow"><img src="images/l_r_arrow.png" alt="Commute together"></div><div class="street_name">${ride.destination} <fmt:formatDate pattern="(hh:mm a)" value="${ride['returntime']}"/></div>
						</div>
						</c:forEach>
					</div>
				</div>
				<div class="col-md-4 why_to_use">
					<h2>Benefits</h2>
					<div class="re_ben_fed">
						<div class="head">WHY to Use</div>
						<ul>
							<li>Reduce Stress</li>
							<li>Wider the Social Network</li>
							<li>Cuts Fuel Costs</li>
							<li>Decreases Pollution</li>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<h2>Feedback</h2>
					<div class="re_ben_fed">
						<div id="owl-demo" class="owl-carousel owl-theme">
							<div class="item">
								<div class="feed_back_form">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										Vestibulum fermentum placerat convallis. Quisque pharetra
										cursus enim et fermentum. Etiam at erat enim.</p>
									<p>Dr. Reddy</p>
								</div>
							</div>
							<div class="item">
								<div class="feed_back_form">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
										Vestibulum fermentum placerat convallis. Quisque pharetra
										cursus enim et fermentum. Etiam at erat enim.</p>
									<p>Dr. Reddy</p>
								</div>
							</div>
						</div>
						<!-- <div class="customNavigation">
                      <a class="btn prev">Previous</a>
                      <a class="btn next">Next</a>
                      <a class="btn play">Autoplay</a>
                      <a class="btn stop">Stop</a>
                  </div> -->


					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Recent Benefits Feedback Section End Here -->

	<!-- Footer Start Here -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4 about_us">
					<h3>About Carpooling</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum fermentum placerat convallis. Quisque pharetra cursus
						enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate
						quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh.
						Aenean at quam faucibus lacus ullamcorper dapibus sit amet at
						neque. Etiam porttitor erat id libero dapibus, at dictum nulla
						tempus. <a href="Javascript:void(0)">More >></a>
					</p>
				</div>
				<div class="col-md-3">
					<div class="footer_links">
						<ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-5">
					<div class="follow_us">
						<div class="clearfix">
							<h3>Follow Us:</h3>
							<div class="social_icons">
								<ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="news_letter_main">
							<h3>Sign Up for Newsletter</h3>
							<input type="text" placeholder="Enrer email address"
								name="newsletter"><input type="button" value="Sign Up">
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer End Here -->
	<link href="css/checkbox.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/custom.js"></script>
	<script type="text/javascript">
		function doLogin() {
			if ($("#email").val() != null && $("#email").val().trim() != "") {
				if ($("#password").val() != null
						&& $("#password").val().trim() != "") {
					$('form').submit();
				} else {
					alert("Please enter your Password.");
					return false;
				}
			} else {
				alert("Please enter your Email.");
				return false;
			}
		}

		function forgotPassword() {
			if ($("#email").val() != null && $("#email").val().trim() != "") {
				$('form').attr("action", "forgotPassword");
				$('form').submit();
			} else {
				alert("Please enter your Email.");
				return false;
			}
		}
		
		function showLogin() {
		alert('Awesome! You\'re all set! Please loging to find the best rides');
		}
		
	    $("input").keyup(function (e) {
	        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
	        	doLogin();
	        }
	    });
		</script>
</body>
</html>