var SITE = SITE || {};
 
SITE.fileInputs = function() {
var $this = $(this),
$val = $this.val(),
valArray = $val.split('\\'),
newVal = valArray[valArray.length-1],
$button = $this.siblings('.button'),
$fakeFile = $this.siblings('.file-holder');
if(newVal !== '') {
$button.text('Photo Chosen');
if($fakeFile.length === 0) {
$button.after('<span class="file-holder">' + newVal + '</span>');
} else {
$fakeFile.text(newVal);
}
}
};

    $(document).ready(function() {
     
    var owl = $("#owl-demo");
     
    owl.owlCarousel({
    items : 1, //10 items above 1000px browser width
    itemsDesktop : [1000,1], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,1], // betweem 900px and 601px
    itemsTablet: [600,1], //2 items between 600 and 0
    itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });
     
    // Custom Navigation Events
    $(".next").click(function(){
    owl.trigger('owl.next');
    })
    $(".prev").click(function(){
    owl.trigger('owl.prev');
    })
    $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
    })
    $(".stop").click(function(){
    owl.trigger('owl.stop');
    })

     // Toggle Left Menu
   $('.menu-list > a').click(function() {
      var parent = $(this).parent();
      var sub = parent.find('> ul');
      
   
         if(sub.is(':visible')) {
            sub.slideUp(200, function(){
               parent.removeClass('nav-active');
                           });
         } else {
            visibleSubMenuClose();
            parent.addClass('nav-active');
            sub.slideDown(200);  
         }
      return false;
   });

   function visibleSubMenuClose() {
      $('.menu-list').each(function() {
         var t = $(this);
         if(t.hasClass('nav-active')) {
            t.find('> ul').slideUp(200, function(){
               t.removeClass('nav-active');
            });
         }
      });
   }


   
    // Ride ways tabs
$(".details-types a").click(function(event) {
        event.preventDefault();
        var tab = $(this).attr("href");
         $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

  // // Menu Toggle Script
  //   $(".toggle-btn").click(function(e) {
  //       e.preventDefault();
  //       $("#wrapper").toggleClass("toggled");
  //   });

// Profile Edit tabs Script
$('#editTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  e.target // activated tab
  e.relatedTarget // previous tab
})

   
}); // document.ready ends here


// Banner and Fom Height 
$(document).ready(function() {
    if ($(window).width() >= 1024){
        var banner_height = $('.banner').height();
        var banner_height = banner_height - 50;
        $('.find_ride_main').height(banner_height);
    }
    
    $(window).resize(function() {
        if ($(window).width() >= 1024){
            var banner_height = $('.banner').height();
            var banner_height = banner_height - 50;
            $('.find_ride_main').height(banner_height);
        }
    });

 

    // custom-file upload
     $('.file-wrapper input[type=file]').bind('change focus click', SITE.fileInputs);

     // custom-checkboxes upload
         $(".datepicker").datetimepicker({
                    pickTime: false
                });
          $(".timepicker").datetimepicker({
            pickDate: false
                });

   // boostrap tooltip
         $("a.info").hover(function(){
        $(this).tooltip('toggle');
    });
          // captcha call
           $('.QapTcha').QapTcha({
      autoSubmit : true,
      autoRevert : true,
    });



          

//  // ride-details tabs 
//   $('.details-types a').click(function (e) {
//   e.preventDefault();
// $(this).attr("href").css('display','none');
// }) ;    



$('#collapsesection').on('shown.bs.collapse', function () {
   $(".adv-srh i").removeClass("glyphicon-plus-sign").addClass("glyphicon-minus-sign");
});

$('#collapsesection').on('hidden.bs.collapse', function () {
   $(".adv-srh i").removeClass("glyphicon-minus-sign").addClass("glyphicon-plus-sign");
});

$('.popoverData').popover();
$('#my-user-list').popover({ 
        html : true, 
        title:'',
        content: function() {
          return $('#subscriber-list').html();
        }
    });
$('#my-message-list').popover({ 
        html : true, 
        content: function() {
          return $('#message-list').html();
        }
    });
$('#my-notify-list').popover({ 
        html : true, 

        content: function() {
          return $('#notification-list').html();
        }

    });
  
 $('body').on('click', function (e) {
    $('[data-toggle="dropdown"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });  
}); 

 $('.newmail_link').click(function(){
 $(this).parent('.data').siblings('.new-emailinput').removeClass('hidden');
 $(this).addClass('hidden');
 });


   // custom selectbox calling
     $('.selectpicker').selectpicker();
  
}); // document.ready ends here








  
