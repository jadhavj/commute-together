  var directionsDisplay;
  var twoWayDirectionsDisplay;
  var directionsService = new google.maps.DirectionsService();
  var map;
  var twoWayMap;
  var loc = new google.maps.LatLng(17.3660, 78.4760);
  var multiwayMaps = [];
  var multiwayDirectionsDisplays = [];

	function customFilter(array, terms) {
        arrayOfTerms = terms.split(" ");
        var term = $.map(arrayOfTerms, function (tm) {
             return $.ui.autocomplete.escapeRegex(tm);
        }).join('|');
       var matcher = new RegExp("\\b" + term, "i");
        return $.grep(array, function (value) {
           return matcher.test(value.label || value.value || value);
        });
    };
    
	function addMultiwayKeyup(rideNum) {
		var prefix = "#ID";
		var appendPrefix = "";
		if (rideNum != null) {
			prefix = prefix + rideNum + "_";
			appendPrefix = "ID" + rideNum + "_";
		}
		$(prefix + "multiwayWayPoint").keyup(function(e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
					+ $(prefix + "multiwayWayPoint").val();
				$.ajax({
					dataType : "json",
					url : address,
					success : function(data) {
						var multiwayWayPts = [];
						$.each(data.results, function ( index, val ) {
							multiwayWayPts.push({label:val.formatted_address});
						});
						$(prefix + "multiwayWayPoint").autocomplete({
					        source: multiwayWayPts,
					        multiple: true,
					        mustMatch: false
					        ,source: function (request, response) {
					            // delegate back to autocomplete, but extract the last term
					            response(customFilter(
					            		multiwayWayPts, request.term));
					        },
					        select: function (event, ui) {
					        	var wayPoint = $(prefix + "multiwayWayPoint").val();
					    		var found = false;
					        	$(prefix + "multiwayWayPoints > option").each(function() {
					        	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
					        	    	found = true;
					        	    }  
					        	});
					        	if (!found) {
					        		$(prefix + "multiwayWayPoints").append("<option value='" + ui.item.value  + "'>" + ui.item.value + "</option>");
					        	}

					        	$("form").find(prefix + "multiwayForwardCheckpoints").remove();
					        	var multiwayCheckpoints = "";
					        	$(prefix + "multiwayWayPoints > option").each(function() {
					        		multiwayCheckpoints += this.value + "~";
					        	});
					        	$("form").append("<input type='hidden' name='" + appendPrefix + "multiwayForwardCheckpoints' id='" + appendPrefix + "multiwayForwardCheckpoints' value='" + multiwayCheckpoints + "'/>");
					        }
					    });
						$(prefix + "multiwayWayPoint").autocomplete("search");
					}
				});
			}
		});
	}

$(document).ready(function () {
	
	$("#startTime").timepicker({ 'timeFormat': 'h:i A' });
	$("#twoWayStartTime").timepicker({ 'timeFormat': 'h:i A' });
	$("#twoWayReturnTime").timepicker({ 'timeFormat': 'h:i A' });
	$("#multiwayStartTime").timepicker({ 'timeFormat': 'h:i A' });
	$("#startDate").datepicker({dateFormat:"dd/mm/yy"});
	$("#endDate").datepicker({dateFormat:"dd/mm/yy"});
	
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }	
	initialize();
	google.maps.event.addDomListener(window, 'load', initialize);

	$("#startLoc").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#startLoc").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var startLocs = [];
					$.each(data.results, function ( index, val ) {
						startLocs.push({label:val.formatted_address});
					});
					$("#startLoc").autocomplete({
				        source: startLocs,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            startLocs, request.term));
				        },
				    });
					$("#startLoc").autocomplete("search");
				}
			});
		}
	});
	$("#endLoc").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#endLoc").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var endLocs = [];
					$.each(data.results, function ( index, val ) {
						endLocs.push({label:val.formatted_address});
					});
					$("#endLoc").autocomplete({
				        source: endLocs,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            endLocs, request.term));
				        },
				    });
					$("#endLoc").autocomplete("search");
				}
			});
		}
	});
	$("#wayPoint").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#wayPoint").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var wayPts = [];
					$.each(data.results, function ( index, val ) {
						wayPts.push({label:val.formatted_address});
					});
					$("#wayPoint").autocomplete({
				        source: wayPts,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            		wayPts, request.term));
				        },
				        select: function (event, ui) {
				        	var wayPoint = $("#wayPoint").val();
				    		var found = false;
				        	$("#wayPoints > option").each(function() {
				        	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
				        	    	found = true;
				        	    }  
				        	});
				        	if (!found) {
				        		$("#wayPoints").append("<option value='" + ui.item.value  + "'>" + ui.item.value + "</option>");
				        	}

				        	$("form").find("#forwardCheckpoints").remove();
				        	var forwardCheckpoints = "";
				        	$("#wayPoints > option").each(function() {
				        	    forwardCheckpoints += this.value + "~";
				        	});
				        	$("form").append("<input type='hidden' name='forwardCheckpoints' id='forwardCheckpoints' value='" + forwardCheckpoints + "'/>");
				        }
				    });
					$("#wayPoint").autocomplete("search");
				}
			});
		}
	});
	                  
	$("#twoWayStartLoc").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#twoWayStartLoc").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var startLocs = [];
					$.each(data.results, function ( index, val ) {
						startLocs.push({label:val.formatted_address});
					});
					$("#twoWayStartLoc").autocomplete({
				        source: startLocs,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            startLocs, request.term));
				        },
				    });
					$("#twoWayStartLoc").autocomplete("search");
				}
			});
		}
	});
	$("#twoWayEndLoc").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#twoWayEndLoc").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var endLocs = [];
					$.each(data.results, function ( index, val ) {
						endLocs.push({label:val.formatted_address});
					});
					$("#twoWayEndLoc").autocomplete({
				        source: endLocs,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            endLocs, request.term));
				        },
				    });
					$("#twoWayEndLoc").autocomplete("search");
				}
			});
		}
	});
	$("#forwardWayPoint").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#forwardWayPoint").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var wayPts = [];
					$.each(data.results, function ( index, val ) {
						wayPts.push({label:val.formatted_address});
					});
					$("#forwardWayPoint").autocomplete({
				        source: wayPts,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            		wayPts, request.term));
				        },
				        select: function (event, ui) {
				        	var wayPoint = $("#forwardWayPoint").val();
				    		var found = false;
				        	$("#forwardWayPoints > option").each(function() {
				        	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
				        	    	found = true;
				        	    }  
				        	});
				        	if (!found) {
				        		$("#forwardWayPoints").append("<option value='" + ui.item.value  + "'>" + ui.item.value + "</option>");
				        	}

				        	$("form").find("#twoWayForwardCheckpoints").remove();
				        	var forwardCheckpoints = "";
				        	$("#forwardWayPoints > option").each(function() {
				        	    forwardCheckpoints += this.value + "~";
				        	});
				        	$("form").append("<input type='hidden' name='twoWayForwardCheckpoints' id='twoWayForwardCheckpoints' value='" + forwardCheckpoints + "'/>");
				        }
				    });
					$("#forwardWayPoint").autocomplete("search");
				}
			});
		}
	});
	$("#backwardWayPoint").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#backwardWayPoint").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var wayPts = [];
					$.each(data.results, function ( index, val ) {
						wayPts.push({label:val.formatted_address});
					});
					$("#backwardWayPoint").autocomplete({
				        source: wayPts,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            		wayPts, request.term));
				        },
				        select: function (event, ui) {
				        	var wayPoint = $("#backwardWayPoint").val();
				    		var found = false;
				        	$("#backwardWayPoints > option").each(function() {
				        	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
				        	    	found = true;
				        	    }  
				        	});
				        	if (!found) {
				        		$("#backwardWayPoints").append("<option value='" + ui.item.value  + "'>" + ui.item.value + "</option>");
				        	}

				        	$("form").find("#twoWayBackwardCheckpoints").remove();
				        	var backwardCheckpoints = "";
				        	$("#backwardWayPoints > option").each(function() {
				        		backwardCheckpoints += this.value + "~";
				        	});
				        	$("form").append("<input type='hidden' name='twoWayBackwardCheckpoints' id='twoWayBackwardCheckpoints' value='" + backwardCheckpoints + "'/>");
				        }
				    });
					$("#backwardWayPoint").autocomplete("search");
				}
			});
		}
	});
	
	$("#multiwayStartLoc").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#multiwayStartLoc").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var multiwayLocs = [];
					$.each(data.results, function ( index, val ) {
						multiwayLocs.push({label:val.formatted_address});
					});
					$("#multiwayStartLoc").autocomplete({
				        source: multiwayLocs,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            		multiwayLocs, request.term));
				        },
				    });
					$("#multiwayStartLoc").autocomplete("search");
				}
			});
		}
	});
	$("#multiwayEndLoc").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#multiwayEndLoc").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var multiwayLocs = [];
					$.each(data.results, function ( index, val ) {
						multiwayLocs.push({label:val.formatted_address});
					});
					$("#multiwayEndLoc").autocomplete({
				        source: multiwayLocs,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            		multiwayLocs, request.term));
				        },
				    });
					$("#multiwayEndLoc").autocomplete("search");
				}
			});
		}
	});
	$("#multiwayWayPoint").keyup(function(e) {
		var code = e.keyCode || e.which;
		if (code == 13) {
			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
				+ $("#multiwayWayPoint").val();
			$.ajax({
				dataType : "json",
				url : address,
				success : function(data) {
					var multiwayWayPts = [];
					$.each(data.results, function ( index, val ) {
						multiwayWayPts.push({label:val.formatted_address});
					});
					$("#multiwayWayPoint").autocomplete({
				        source: multiwayWayPts,
				        multiple: true,
				        mustMatch: false
				        ,source: function (request, response) {
				            // delegate back to autocomplete, but extract the last term
				            response(customFilter(
				            		multiwayWayPts, request.term));
				        },
				        select: function (event, ui) {
				        	var wayPoint = $("#multiwayWayPoint").val();
				    		var found = false;
				        	$("#multiwayWayPoints > option").each(function() {
				        	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
				        	    	found = true;
				        	    }  
				        	});
				        	if (!found) {
				        		$("#multiwayWayPoints").append("<option value='" + ui.item.value  + "'>" + ui.item.value + "</option>");
				        	}

				        	$("form").find("#multiwayForwardCheckpoints").remove();
				        	var multiwayCheckpoints = "";
				        	$("#multiwayWayPoints > option").each(function() {
				        		multiwayCheckpoints += this.value + "~";
				        	});
				        	$("form").append("<input type='hidden' name='multiwayForwardCheckpoints' id='multiwayForwardCheckpoints' value='" + multiwayCheckpoints + "'/>");
				        }
				    });
					$("#multiwayWayPoint").autocomplete("search");
				}
			});
		}
	});
	var tid = setTimeout(refreshMap, 1000);
	
	function refreshMap() {
		google.maps.event.trigger(twoWayMap, 'resize');
		google.maps.event.trigger(map, 'resize');
		google.maps.event.trigger(multiwayMaps[1], 'resize');
		tid = setTimeout(refreshMap, 1000); 
	}
});


function showPosition(position) {
    loc =  new google.maps.LatLng(position.coords.latitude, position.coords.longitude); 
}

function initialize() {
	  directionsDisplay = new google.maps.DirectionsRenderer();
	  twoWayDirectionsDisplay = new google.maps.DirectionsRenderer();
	  multiwayDirectionsDisplays[1] = new google.maps.DirectionsRenderer();
	  var mapOptions = {
			    zoom: 9,
			    center: loc
			  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  twoWayMap = new google.maps.Map(document.getElementById('twoway-map-canvas'), mapOptions);
  multiwayMaps[1] = new google.maps.Map(document.getElementById('multiway-map-canvas'), mapOptions);
  directionsDisplay.setMap(map);
  twoWayDirectionsDisplay.setMap(twoWayMap);
  multiwayDirectionsDisplays[1].setMap(multiwayMaps[1]);
}

function setupMultiwayMap(groupId) {
	  $("form").find("#multiways").remove();
	  $("form").append("<input type='hidden' name='multiways' id='multiways' value='" + groupId + "'/>");
	  var multiwayMapOptions = {
			    zoom: 9,
			    center: loc
			  };
	  multiwayMaps[groupId] = new google.maps.Map(document.getElementById("ID" + groupId + "_multiway-map-canvas"), multiwayMapOptions);
	  multiwayDirectionsDisplays[groupId] = new google.maps.DirectionsRenderer();
	  multiwayDirectionsDisplays[groupId].setMap(multiwayMaps[groupId]);
	  
		$("#ID" + groupId + "_multiwayStartLoc").keyup(function(e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
					+ $("#ID" + groupId + "_multiwayStartLoc").val();
				$.ajax({
					dataType : "json",
					url : address,
					success : function(data) {
						var multiwayLocs = [];
						$.each(data.results, function ( index, val ) {
							multiwayLocs.push({label:val.formatted_address});
						});
						$("#ID" + groupId + "_multiwayStartLoc").autocomplete({
					        source: multiwayLocs,
					        multiple: true,
					        mustMatch: false
					        ,source: function (request, response) {
					            // delegate back to autocomplete, but extract the last term
					            response(customFilter(
					            		multiwayLocs, request.term));
					        },
					    });
						$("#ID" + groupId + "_multiwayStartLoc").autocomplete("search");
					}
				});
			}
		});

		$("#ID" + groupId + "_multiwayEndLoc").keyup(function(e) {
			var code = e.keyCode || e.which;
			if (code == 13) {
				var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
					+ $("#ID" + groupId + "_multiwayEndLoc").val();
				$.ajax({
					dataType : "json",
					url : address,
					success : function(data) {
						var multiwayLocs = [];
						$.each(data.results, function ( index, val ) {
							multiwayLocs.push({label:val.formatted_address});
						});
						$("#ID" + groupId + "_multiwayEndLoc").autocomplete({
					        source: multiwayLocs,
					        multiple: true,
					        mustMatch: false
					        ,source: function (request, response) {
					            // delegate back to autocomplete, but extract the last term
					            response(customFilter(
					            		multiwayLocs, request.term));
					        },
					    });
						$("#ID" + groupId + "_multiwayEndLoc").autocomplete("search");
					}
				});
			}
		});
}

function calcRoute() {
	  var start = $("#startLoc").val();
	  var end = $("#endLoc").val();
	  var waypts = [];
	  $("#wayPoints > option").each(function() {
		  waypts.push({
		      location: this.value,
		      stopover:true});
		});
	  var request = {
	      origin:start,
	      destination:end,
	      waypoints: waypts,
	      optimizeWaypoints: false,
	      travelMode: google.maps.TravelMode.DRIVING
	  };
	  directionsService.route(request, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	      	directionsDisplay.setDirections(response);
	    } else {
	    	alert("Please enter valid route data.");
	    }
	  });
	}
function calcForwardRoute() {
	  var start = $("#twoWayStartLoc").val();
	  var end = $("#twoWayEndLoc").val();
	  var waypts = [];
	  $("#forwardWayPoints > option").each(function() {
		  waypts.push({
		      location: this.value,
		      stopover:true});
		});
	  var request = {
	      origin:start,
	      destination:end,
	      waypoints: waypts,
	      optimizeWaypoints: false,
	      travelMode: google.maps.TravelMode.DRIVING
	  };
	  directionsService.route(request, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	      twoWayDirectionsDisplay.setDirections(response);
	    } else {
	    	alert("Please enter valid route data.");
	    }
	  });
	}
function calcBackwardRoute() {
	  var end = $("#twoWayStartLoc").val();
	  var start = $("#twoWayEndLoc").val();
	  var waypts = [];
	  $("#backwardWayPoints > option").each(function() {
		  waypts.push({
		      location: this.value,
		      stopover:true});
		});
	  var request = {
	      origin:start,
	      destination:end,
	      waypoints: waypts,
	      optimizeWaypoints: false,
	      travelMode: google.maps.TravelMode.DRIVING
	  };
	  directionsService.route(request, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	      twoWayDirectionsDisplay.setDirections(response);
	    } else {
	    	alert("Please enter valid route data.");
	    }
	  });
	}
function calcMultiwayRoute(groupId) {
	  var prefix = "#";
	  if (groupId != null) {
		  prefix = "#ID" + groupId + "_";
	  }
	
	  var start = $(prefix + "multiwayStartLoc").val();
	  var end = $(prefix + "multiwayEndLoc").val();
	  var waypts = [];
	  $(prefix + "multiwayWayPoints > option").each(function() {
		  waypts.push({
		      location: this.value,
		      stopover:true});
		});
	  var request = {
	      origin:start,
	      destination:end,
	      waypoints: waypts,
	      optimizeWaypoints: false,
	      travelMode: google.maps.TravelMode.DRIVING
	  };
	  directionsService.route(request, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	    	if (groupId == null) {
	    		groupId = 1;
	    	}
	      multiwayDirectionsDisplays[groupId].setDirections(response);
	    } else {
	    	alert("Please enter valid route data.");
	    }
	  });
	}

function remAllWaypts() {
	$("#wayPoints").empty();
}
function remSelWaypts() {
	$("#wayPoints").find('option:selected').remove();
}
function remAllForwardWaypts() {
	$("#forwardWayPoints").empty();
}
function remSelForwardWaypts() {
	$('#forwardWayPoints').find('option:selected').remove();
}
function remAllTwoWayForwardWaypts() {
	$("#twoWayForwardWayPoints").empty();
	$("form").find("#twoWayForwardCheckpoints").remove();
	var forwardCheckpoints = "";
	$("#twoWayForwardWayPoints > option").each(function() {
	    forwardCheckpoints += this.value + "~";
	});
	$("form").append("<input type='hidden' name='twoWayForwardCheckpoints' id='twoWayForwardCheckpoints' value='" + forwardCheckpoints + "'/>");
}
function remSelTwoWayForwardWaypts() {
	$('#twoWayForwardWayPoints').find('option:selected').remove();
	$("form").find("#twoWayForwardCheckpoints").remove();
	var forwardCheckpoints = "";
	$("#twoWayForwardWayPoints > option").each(function() {
	    forwardCheckpoints += this.value + "~";
	});
	$("form").append("<input type='hidden' name='twoWayForwardCheckpoints' id='twoWayForwardCheckpoints' value='" + forwardCheckpoints + "'/>");
}
function remAllBackwardWaypts() {
	$("#backwardWayPoints").empty();
}
function remSelBackwardWaypts() {
	$("#backwardWayPoints").find('option:selected').remove();
}
function remAllMultiwayWaypts(groupId) {
	if (groupId == null) {
		$("#multiwayWayPoints").empty();
	} else {
		$("#ID" + groupId + "_multiwayWayPoints").empty();
	}
}
function remSelMultiwayWaypts(groupId) {
	if (groupId == null) {
		$('#multiwayWayPoints').find('option:selected').remove();
	} else {
		$("#ID" + groupId + "_multiwayWayPoints").find('option:selected').remove();
	}
}

function setupWay(whichWay) {
	if($("#option" + whichWay).is(':checked')) {
		return false;
	} else {
		$("form").find("#whichWay").remove();
		$("form").append("<input type='hidden' name='whichWay' id='whichWay' value='" + whichWay + "'/>");
	}
}

function previewImage() {
	if (document.getElementById("photo").files[0].name.substring(
			document.getElementById("photo").files[0].name.length - 3,
			document.getElementById("photo").files[0].name.length) != "jpg"
			&& document.getElementById("photo").files[0].name
					.substring(
							document.getElementById("photo").files[0].name.length - 3,
							document.getElementById("photo").files[0].name.length) != "png") {
		document.getElementById('photo').value = '';
		$("#photo").val("");
		alert("Only JPG or PNG files please.");
		setTimeout(function() {
			$("span").remove('.file-holder');
		}, 500);
		return;
	}
	var oFReader = new FileReader();
	if (document.getElementById("photo").files[0].size > 51200) {
		alert("Please upload an image of size less than 50kB.");
		$("#photo").val("");
		setTimeout(function() {
			$("span").remove('.file-holder');
		}, 500);
		return;
	}
	oFReader.readAsDataURL(document.getElementById("photo").files[0]);

	oFReader.onload = function(oFREvent) {
		document.getElementById("avatar").src = oFREvent.target.result;
	};
	setTimeout(function() {
		$("span").remove('.file-holder');
	}, 500);
}

function submitRide() {
	if ($("#startDate").val() == "") {
		alert("Please enter Start Date for the ride.");
		return false;
	}

	if ($("#whichWay").val() == "1") {
		if ($("#startLoc").val() == "") {
			alert("Please enter Starting location.");
			return false;
		}
		if ($("#endLoc").val() == "") {
			alert("Please enter Destination location.");
			return false;
		}
		if ($("#startTime").val() == "") {
			alert("Please enter Start Time for the ride.");
			return false;
		}
	} else if ($("#whichWay").val() == "2") {
		if ($("#twoWayStartLoc").val() == "") {
			alert("Please enter Starting location.");
			return false;
		}
		if ($("#twoWayEndLoc").val() == "") {
			alert("Please enter Destination location.");
			return false;
		}
		if ($("#twoWayStartTime").val() == "") {
			alert("Please enter Start Time for the ride.");
			return false;
		}
		if ($("#twoWayReturnTime").val() == "") {
			alert("Please enter Return Time for the ride.");
			return false;
		}
	} else if ($("#whichWay").val() == "3") {
		var multiways = parseInt($("#multiways").val());
		for (i = 1; i <= multiways; i++) {
			var prefix = "#";
			if (i != 1) {
				prefix = "#ID" + i + "_";
			}
			if ($(prefix + "multiwayStartLoc").val() == "") {
				alert("Please enter Starting location for Ride " + i);
				return false;
			}
			if ($(prefix + "multiwayEndLoc").val() == "") {
				alert("Please enter Destination location for Ride " + i);
				return false;
			}
			if ($(prefix + "multiwayStartTime").val() == "") {
				alert("Please enter Start Time for Ride " + i);
				return false;
			}
		}
	}
	
	if ($("#vehicleNo").val() == "") {
		alert("Please enter vehicle no.");
		return false;
	}
	if ($("#licenseNo").val() == "") {
		alert("Please enter license no.");
		return false;
	}
	
	var startDate = $('#startDate').datepicker('getDate');
	var now = new Date();
	now.setHours(0,0,0,0)
	if (startDate < now) {
		alert("Start Date for the journey cannot be in past.");
		return false;
	}

	var endDate = $('#endDate').datepicker('getDate');
	if (endDate < startDate) {
		alert("End Date should be on or after Start Date");
		return false;
	}
	
	$("form").submit();
}

function addWayPoint() {
	if ($("#wayPoint").val() != "") {
		var wayPoint = $("#wayPoint").val();
		var found = false;
    	$("#wayPoints > option").each(function() {
    	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
    	    	found = true;
    	    }  
    	});
    	if (!found) {
    		$("#wayPoints").append("<option value='" + $("#wayPoint").val()  + "'>" + $("#wayPoint").val() + "</option>");
    	}

    	$("form").find("#forwardCheckpoints").remove();
    	var forwardCheckpoints = "";
    	$("#wayPoints > option").each(function() {
    	    forwardCheckpoints += this.value + "~";
    	});
    	$("form").append("<input type='hidden' name='forwardCheckpoints' id='forwardCheckpoints' value='" + forwardCheckpoints + "'/>");
    	$("#wayPoint").val("")
	}
}
function addForwardWayPoint() {
	if ($("#forwardWayPoint").val() != "") {
		var wayPoint = $("#forwardWayPoint").val();
		var found = false;
    	$("#forwardWayPoints > option").each(function() {
    	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
    	    	found = true;
    	    }  
    	});
    	if (!found) {
    		$("#forwardWayPoints").append("<option value='" + $("#forwardWayPoint").val()  + "'>" + $("#forwardWayPoint").val() + "</option>");
    	}

    	$("form").find("#twoWayForwardCheckpoints").remove();
    	var forwardCheckpoints = "";
    	$("#forwardWayPoints > option").each(function() {
    	    forwardCheckpoints += this.value + "~";
    	});
    	$("form").append("<input type='hidden' name='twoWayForwardCheckpoints' id='twoWayForwardCheckpoints' value='" + forwardCheckpoints + "'/>");
    	$("#forwardWayPoint").val("");
	}
}
function addBackwardWayPoint() {
	if ($("#backwardWayPoint").val() != "") {
		var wayPoint = $("#backwardWayPoint").val();
		var found = false;
    	$("#backwardWayPoints > option").each(function() {
    	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
    	    	found = true;
    	    }  
    	});
    	if (!found) {
    		$("#backwardWayPoints").append("<option value='" + $("#backwardWayPoint").val()  + "'>" + $("#backwardWayPoint").val() + "</option>");
    	}

    	$("form").find("#twoWayBackwardCheckpoints").remove();
    	var backwardCheckpoints = "";
    	$("#backwardWayPoints > option").each(function() {
    		backwardCheckpoints += this.value + "~";
    	});
    	$("form").append("<input type='hidden' name='twoWayBackwardCheckpoints' id='twoWayBackwardCheckpoints' value='" + backwardCheckpoints + "'/>");
    	$("#backwardWayPoint").val("");
	}
}
function addTwoWayForwardWayPoint() {
	if ($("#twoWayForwardWayPoint").val() != "") {
		var wayPoint = $("#twoWayForwardWayPoint").val();
		var found = false;
    	$("#twoWayForwardWayPoints > option").each(function() {
    	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
    	    	found = true;
    	    }  
    	});
    	if (!found) {
    		$("#twoWayForwardWayPoints").append("<option value='" + $("#twoWayForwardWayPoint").val()  + "'>" + $("#twoWayForwardWayPoint").val() + "</option>");
    	}

    	$("form").find("#twoWayForwardCheckpoints").remove();
    	var forwardCheckpoints = "";
    	$("#twoWayForwardWayPoints > option").each(function() {
    	    forwardCheckpoints += this.value + "~";
    	});
    	$("form").append("<input type='hidden' name='twoWayForwardCheckpoints' id='twoWayForwardCheckpoints' value='" + forwardCheckpoints + "'/>");
    	$("#twoWayForwardWayPoint").val("");
	}
}
function addMultiwayWayPoint(groupId) {
	var prefix = "#";
	var appendPrefix = "";
	if (groupId != null) {
		prefix = "#ID" + groupId + "_";
		appendPrefix = "ID" + groupId + "_";
	}
	if ($(prefix + "multiwayWayPoint").val() != "") {
		var found = false;
    	$(prefix + "multiwayWayPoints > option").each(function() {
    	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
    	    	found = true;
    	    }  
    	});
    	if (!found) {
    		$(prefix + "multiwayWayPoints").append("<option value='" + $(prefix + "multiwayWayPoint").val()  + "'>" + $(prefix + "multiwayWayPoint").val() + "</option>");
    	}

    	$("form").find(prefix + "multiwayForwardCheckpoints").remove();
    	var multiwayCheckpoints = "";
    	$(prefix + "multiwayWayPoints > option").each(function() {
    		multiwayCheckpoints += this.value + "~";
    	});
    	$("form").append("<input type='hidden' name='" + appendPrefix + "multiwayForwardCheckpoints' id='" + appendPrefix + "multiwayForwardCheckpoints' value='" + multiwayCheckpoints + "'/>");
    	$(prefix + "multiwayWayPoint").val("");
	}
}
