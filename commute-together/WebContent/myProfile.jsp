<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<%
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
		    %>
		    <script type="text/javascript">
		    alert("<%=entry.getValue()%>");
		    </script>
		    <%
		    break;
		}
	}
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->
      <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="dashboard.jsp">Home</a></li>
      <li class="active">Settings</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
 <section class="search_results_main">
        <div class="container">
        <div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>
  <div id="#page-content-wrapper" class="col-sm-8 col-md-10">
          <div class="bs-example">
          <!-- start header title -->
           <div class="ItemHeader">
            <span class="ProfileHeader">
             <span id="header_label">My Profile</span></span>
                </div>
                <!-- end header title -->
     <form enctype="multipart/form-data" role="form" class="form-horizontal prfl_stngs" action="updateProfile" method="post">
     <div class="form-section-wrapper">
  
<ul class="nav nav-tabs" id="editTabs" role="tablist">
							<li class="active">
								<a data-toggle="tab" role="tab" href="#PiInfo">Personal Info</a>
							</li>
							<li class="">
								<a data-toggle="tab" role="tab" href="#CiInfo">Contact Info</a>
							</li>
							<li class="">
								<a data-toggle="tab" role="tab" href="#WiInfo">Work Info</a>
							</li>
							<li class="">
								<a data-toggle="tab" role="tab" href="#AmInfo">About Me</a>
							</li>
							
						</ul>


						<div class="tab-content tab-content-bordered">
							<div id="PiInfo" class="tab-pane fade active in">
								<div class="form-group">
        <label for="" class="col-sm-2 control-label">First Name</label>
        <div class="col-sm-6">    
    <input type="text" placeholder="" class="form-control" name="fname" value="${sessionScope.user.fname}">
        </div>
      </div>
      <div class="form-group">
        <label for="" class="col-xs-12 col-sm-2 control-label">Last Name</label>
        <div class="col-xs-9 col-sm-6">    
    <input type="text" placeholder="" class="form-control" value="${sessionScope.user.lname}" name="lname">
        </div>
        <div class="dropdown visibility-dropdown">
        <a href="#" data-toggle="dropdown" role="button" id="drop4">
        <span class="fa fa-users"></span>
        <span class="caret"></span></a>
        <ul aria-labelledby="drop4" role="menu" class="dropdown-menu pull-right" id="menu1">
          <li role="presentation"><a href="" id="lnamevisible0" onclick="javascript:changeVisibility('lnamevisible' , '0', this); return false;" tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-globe"></i>Public<c:if test="${sessionScope.user.lnamevisible eq 0}"><span id="lnamevisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="lnamevisible" value="${pageScope.user.lnamevisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="lnamevisible1" onclick="javascript:changeVisibility('lnamevisible', '1', this); return false;" tabindex="-1" role="menuitem"><img class="visibilty_icons" src="images/users.png"/>Friends<c:if test="${sessionScope.user.lnamevisible eq 1}"><span id="lnamevisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="lnamevisible" value="${pageScope.user.lnamevisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="lnamevisible2" onclick="javascript:changeVisibility('lnamevisible', '2', this); return false;"tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-lock"></i>Only me<c:if test="${sessionScope.user.lnamevisible eq 2}"><span id="lnamevisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="lnamevisible" value="${pageScope.user.lnamevisible}"/></span></c:if></a></li>
          <li class="divider" role="presentation"></li>
        </ul>
      </div>
      </div>
      <div class="form-group required">


        <label for="" class="col-xs-12 col-sm-2 control-label">D.O.B</label>
   
              <div class="col-xs-9 col-sm-3 input-group date datepicker">
	<input type="text" class="form-control" name="dob" id="dob"	placeholder="dd/mm/yyyy" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${user['dob']}"/>">
											</div>



      </div>



 
      <div class="form-group required">
        <label for="" class="col-xs-12 col-sm-2 control-label">Gender</label>
        <div class="col-xs-12 col-sm-6">    
    <select required="required" class="tooltip-parent selectrik" name="gender" style="width:200px" class="selectrik">
<option value="0" <c:if test="${sessionScope.user.gender eq '0'}">selected="selected"</c:if>>Male</option>
<option value="1" <c:if test="${sessionScope.user.gender eq '1'}">selected="selected"</c:if>>Female</option>
</select>

        </div>

      </div>
      <div class="form-group">
<label for="" class="col-sm-2 control-label">License No.</label>
<div class="col-sm-4">
  <input type="text" required="required" placeholder="" class="form-control" value="${sessionScope.user.idproof}" name="idproof">
</div>
</div>
<hr>
<div class="page_submit">
<input type="button" value="Update" onclick="javascript:doUpdate()"></div>
							</div> <!-- / .tab-pane -->
							<div id="CiInfo" class="tab-pane fade">
								<div class="form-group required">
    <label class="col-xs-12 col-sm-2 control-label" for="inputEmail3">Email Address</label>
    <div class="col-xs-9 col-sm-6">
      <input type="email" placeholder="Email" id="inputEmail3" class="form-control" required="required" name="email" value="${sessionScope.user.email}">
    </div>
    <div class="dropdown visibility-dropdown">
        <a href="#" data-toggle="dropdown" role="button" id="drop1">
        <span class="fa fa-users"></span>
        <span class="caret"></span></a>
        <ul aria-labelledby="drop1" role="menu" class="dropdown-menu pull-right" id="menu4">
          <li role="presentation"><a href="" id="emailvisible0" onclick="javascript:changeVisibility('emailvisible' , '0', this); return false;" tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-globe"></i>Public<c:if test="${sessionScope.user.emailvisible eq 0}"><span id="emailvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="emailvisible" value="${pageScope.user.emailvisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="emailvisible1" onclick="javascript:changeVisibility('emailvisible', '1', this); return false;" tabindex="-1" role="menuitem"><img class="visibilty_icons" src="images/users.png"/>Friends<c:if test="${sessionScope.user.emailvisible eq 1}"><span id="emailvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="emailvisible" value="${pageScope.user.emailvisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="emailvisible2" onclick="javascript:changeVisibility('emailvisible', '2', this); return false;"tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-lock"></i>Only me<c:if test="${sessionScope.user.emailvisible eq 2}"><span id="emailvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="emailvisible" value="${pageScope.user.emailvisible}"/></span></c:if></a></li>
          <li class="divider" role="presentation"></li>
          
        </ul>
      </div>
  </div>
  <div class="form-group required">
        <label for="" class="col-xs-12 col-sm-2 control-label">Mobile Number</label>
        <div class="col-xs-9 col-sm-6">    
    <input type="text" placeholder="" onkeypress="return numbersonly(this, event)" class="form-control" required="required" value="${sessionScope.user.mobile}" name="mobile">
        </div>
        <div class="dropdown visibility-dropdown">
        <a href="#" data-toggle="dropdown" role="button" id="drop7">
        <span class="fa fa-users"></span>
        <span class="caret"></span></a>
        <ul aria-labelledby="drop7" role="menu" class="dropdown-menu pull-right" id="menu7">
          <li role="presentation"><a href="" id="mobilevisible0" onclick="javascript:changeVisibility('mobilevisible' , '0', this); return false;" tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-globe"></i>Public<c:if test="${sessionScope.user.mobilevisible eq 0}"><span id="mobilevisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="mobilevisible" value="${pageScope.user.mobilevisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="mobilevisible1" onclick="javascript:changeVisibility('mobilevisible', '1', this); return false;" tabindex="-1" role="menuitem"><img class="visibilty_icons" src="images/users.png"/>Friends<c:if test="${sessionScope.user.mobilevisible eq 1}"><span id="mobilevisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="mobilevisible" value="${pageScope.user.mobilevisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="mobilevisible2" onclick="javascript:changeVisibility('mobilevisible', '2', this); return false;"tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-lock"></i>Only me<c:if test="${sessionScope.user.mobilevisible eq 2}"><span id="mobilevisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="mobilevisible" value="${pageScope.user.mobilevisible}"/></span></c:if></a></li>
          <li class="divider" role="presentation"></li>
          
        </ul>
      </div>
      </div>
      <hr>
  <div class="page_submit">
<input type="button" value="Update" onclick="javascript:doUpdate()"></div>
							</div> <!-- / .tab-pane -->
							<div id="WiInfo" class="tab-pane fade">
								<div class="form-group">
        <label for="" class="col-xs-12 col-sm-2 control-label">Occupation</label>
        <div class="col-xs-9 col-sm-6">    
    <input type="text" placeholder="" class="form-control" value="${sessionScope.user.occupation}" name="occupation">
        </div>
        <div class="dropdown visibility-dropdown">
        <a href="#" data-toggle="dropdown" role="button" id="drop3">
        <span class="fa fa-users"></span>
        <span class="caret"></span></a>
        <ul aria-labelledby="drop3" role="menu" class="dropdown-menu pull-right" id="menu2">
          <li role="presentation"><a href="" id="occupationvisible0" onclick="javascript:changeVisibility('occupationvisible' , '0', this); return false;" tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-globe"></i>Public<c:if test="${sessionScope.user.occupationvisible eq 0}"><span id="occupationvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="occupationvisible" value="${pageScope.user.occupationvisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="occupationvisible1" onclick="javascript:changeVisibility('occupationvisible', '1', this); return false;" tabindex="-1" role="menuitem"><img class="visibilty_icons" src="images/users.png"/>Friends<c:if test="${sessionScope.user.occupationvisible eq 1}"><span id="occupationvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="occupationvisible" value="${pageScope.user.occupationvisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="occupationvisible2" onclick="javascript:changeVisibility('occupationvisible', '2', this); return false;"tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-lock"></i>Only me<c:if test="${sessionScope.user.occupationvisible eq 2}"><span id="occupationvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="occupationvisible" value="${pageScope.user.occupationvisible}"/></span></c:if></a></li>
          <li class="divider" role="presentation"></li>
          
        </ul>
      </div>
            
      </div>
      <div class="form-group">
        <label for="" class="col-xs-12 col-sm-2 control-label">Organization Name</label>
        <div class="col-xs-9 col-sm-6">    
    <input type="text" placeholder="" class="form-control" value="${sessionScope.user.organization}" name="organization">
        </div>
        <div class="dropdown visibility-dropdown">
        <a href="#" data-toggle="dropdown" role="button" id="drop2">
        <span class="fa fa-users"></span>
        <span class="caret"></span></a>
        <ul aria-labelledby="drop2" role="menu" class="dropdown-menu pull-right" id="menu3">
          <li role="presentation"><a href="" id="organizationvisible0" onclick="javascript:changeVisibility('organizationvisible' , '0', this); return false;" tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-globe"></i>Public<c:if test="${sessionScope.user.organizationvisible eq 0}"><span id="organizationvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="organizationvisible" value="${pageScope.user.organizationvisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="organizationvisible1" onclick="javascript:changeVisibility('organizationvisible', '1', this); return false;" tabindex="-1" role="menuitem"><img class="visibilty_icons" src="images/users.png"/>Friends<c:if test="${sessionScope.user.organizationvisible eq 1}"><span id="organizationvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="organizationvisible" value="${pageScope.user.organizationvisible}"/></span></c:if></a></li>
          <li role="presentation"><a href="" id="organizationvisible2" onclick="javascript:changeVisibility('organizationvisible', '2', this); return false;"tabindex="-1" role="menuitem"><i class="visibilty_icons fa fa-lock"></i>Only me<c:if test="${sessionScope.user.organizationvisible eq 2}"><span id="organizationvisible">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="images/greenCheck.jpg"/><input type="hidden" name="organizationvisible" value="${pageScope.user.organizationvisible}"/></span></c:if></a></li>
          <li class="divider" role="presentation"></li>
          
        </ul>
      </div>
      </div>
   <hr>
<div class="page_submit">
<input type="button" value="Update" onclick="javascript:doUpdate()"></div>	
							</div> <!-- / .tab-pane -->
							<div id="AmInfo" class="tab-pane fade">
								
    <div class="form-section-wrapper">
    <div class="form_section_title">Basic details</div>
    <div class="col-xs-12 col-sm-12 col-md-6 row">
    <div class="form-group">
        <label for="" class="col-sm-4 control-label">Body Type</label>
        <div class="col-sm-8">    
    <select name="body" class="selectrik">
	   <option value="-1" <c:if test="${sessionScope.user.body eq -1}">disabled selected="selected"</c:if>>Select Body Type</option>
	   <option value="0" <c:if test="${sessionScope.user.body eq 0}">selected="selected"</c:if>>Slim</option>
	   <option value="1" <c:if test="${sessionScope.user.body eq 1}">selected="selected"</c:if>>Fat</option>
	   <option value="2" <c:if test="${sessionScope.user.body eq 2}">selected="selected"</c:if>>Average</option>
</select>
        </div>
      </div>
          <div class="form-group">
        <label for="" class="col-sm-4 control-label">Language</label>
        <div class="col-sm-8">    
    <select id="language" multiple>
<option value="0">English</option>
<option value="1">Hindi</option>
<option value="2">Telugu</option>
<option value="3">Tamil</option>
<option value="4">Kannada</option>
</select>
        </div>
      </div>
           <div class="form-group">
        <label for="" class="col-sm-4 control-label">Ethnicity</label>
        <div class="col-sm-8">    
    <select name="ethnicity" class="selectrik">
<option value="-1" <c:if test="${sessionScope.user.ethnicity eq -1}">disabled selected="selected"</c:if>>Select Ethnicity</option>
<option value="0" <c:if test="${sessionScope.user.ethnicity eq 0}">selected="selected"</c:if>>North Indian</option>
<option value="1" <c:if test="${sessionScope.user.ethnicity eq 1}">selected="selected"</c:if>>South Indian</option>
<option value="2" <c:if test="${sessionScope.user.ethnicity eq 2}">selected="selected"</c:if>>East Indian</option>
<option value="3" <c:if test="${sessionScope.user.ethnicity eq 3}">selected="selected"</c:if>>West Indian</option>
</select>
        </div>
      </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group">
										<label class="col-sm-4 control-label photo-label" for="">Photo</label>
										<span class="col-sm-8 file-wrapper"> <img
											<c:if test = "${not empty sessionScope.user.photo}">src="${pageContext.request.contextPath}/imageServlet?email=${sessionScope.user.email}"</c:if>
											<c:if test = "${empty sessionScope.user.photo}"> src="images/avatar.png" </c:if>
											 alt="photo" id="avatar"
											style="width: 100px; height: 100px;" /> <input type="file"
											name="photo" id="photo" onchange="javascript:previewImage();" />
											<span class="button upload_pic_btn">Upload Photo</span>
										</span>
									</div>
</div>
</div>
<hr>
<div class="form-section-wrapper">
<div class="form_section_title">Background Values</div>
 <div class="col-xs-12 col-sm-12 col-md-6 row">
         <div class="form-group">
        <label for="" class="col-sm-4 control-label">Religion</label>
        <div class="col-sm-8">    
    <select name="religion" class="selectrik">
<option value="-1" <c:if test="${sessionScope.user.religion eq -1}">disabled selected="selected"</c:if>>Select Religion</option>
<option value="0" <c:if test="${sessionScope.user.religion eq 0}">selected="selected"</c:if>>Hindu</option>
<option value="1" <c:if test="${sessionScope.user.religion eq 1}">selected="selected"</c:if>>Christian</option>
<option value="2" <c:if test="${sessionScope.user.religion eq 2}">selected="selected"</c:if>>Muslim</option>
</select>
        </div>
      </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 row">
      <div class="form-group">
        <label for="" class="col-sm-4 control-label">Religious Views</label>
        <div class="col-sm-8">    
<input type="text" placeholder="" class="form-control" maxlength="40" name="religiousViews" value="${sessionScope.user.religiousview}" >
        </div>
      </div>
      </div>
	
</div>
<hr>
<div class="form-section-wrapper">
<div class="form_section_title">Life Style</div>
 <div class="col-xs-12 col-sm-12 col-md-6 row">
         <div class="form-group">
        <label for="" class="col-sm-4 control-label">Diet</label>
        <div class="col-sm-8">    
    <select name="diet" class="selectrik">
<option value="-1" <c:if test="${sessionScope.user.diet eq -1}">selected="selected"</c:if>>Select Diet</option>
<option value="0" <c:if test="${sessionScope.user.diet eq 0}">selected="selected"</c:if>>Vegetarian</option>
<option value="1" <c:if test="${sessionScope.user.diet eq 1}">selected="selected"</c:if>>Non-vegetarian</option>
</select>
        </div>
      </div>
      </div>	
</div>
<hr>
<div class="form-section-wrapper">
<div class="form_section_title">Habbits While Travelling</div>
 <div class="col-xs-12 col-sm-12 col-md-12 row">
         <div class="col-xs-12 col-sm-3 col-md-3">
         <div class="form-group chlbx1">
            <div class="checkbox checkbox check-success">
		     	<input type="checkbox" id="chkTerms1" name="habitreading" value="1" name="habitreading" class="css-checkbox"
		     	<c:if test="${sessionScope.user.habitreading eq 1}">checked="checked"</c:if>>
				<label for="chkTerms1" class="css-label">Reading</label>
				</div>
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox" name="habitsleeping" class="css-checkbox"<c:if test="${sessionScope.user.habitsleeping eq 1}">checked="checked"</c:if> name="habitsleeping" id="chkTerms2" value="1">
				<label for="chkTerms2" class="css-label">Sleeping</label>
				</div>
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox" id="chkTerms3" name="habitchat" value="1" class="css-checkbox" <c:if test="${sessionScope.user.habitchat eq 1}">checked="checked"</c:if>>
				<label for="chkTerms3"  class="css-label">Chit Chat</label></div>
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox" class="css-checkbox"<c:if test="${sessionScope.user.habiteating eq 1}">checked="checked"</c:if> name="habiteating" id="chkTerms4" value="1">
				<label for="chkTerms4" class="css-label">Eating</label></div>
				</div>
				</div>
			
				<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="form-group">
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox"  id="chkTerms5" class="css-checkbox" <c:if test="${sessionScope.user.habitsmoking eq 1}">checked="checked"</c:if> name="habitsmoking" value="1"><label for="chkTerms5" class="css-label">Smoking</label></div>
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox" class="css-checkbox" <c:if test="${sessionScope.user.habitworking eq 1}">checked="checked"</c:if> name="habitworking" id="chkTerms6" value="1"><label for="chkTerms6" class="css-label">Working</label></div>
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox"  class="css-checkbox" <c:if test="${sessionScope.user.habitphone eq 1}">checked="checked"</c:if> id="chkTerms7" name="habitphone" value="1"><label for="chkTerms7" class="css-label">Phone Calls</label></div>
				<div class="checkbox checkbox check-success">
		     	<input type="checkbox" class="css-checkbox" <c:if test="${sessionScope.user.habitmusic eq 1}">checked="checked"</c:if> name="habitmusic" id="chkTerms8" value="1"><label for="chkTerms8" class="css-label">Listening Music</label></div>
		     	</div>
			</div>
  
      <div class="col-xs-12 col-sm-5 col-md-6">
      <div class="form-group">
        <label for="" class="col-sm-4 control-label">Likes</label>
        <div class="col-sm-8">    
<textarea class="form-control" rows="3" style="resize:none" name="likes">${sessionScope.user.likes}</textarea>
        </div>
      </div>
      <div class="form-group">
        <label for="" class="col-sm-4 control-label">Dislikes</label>
        <div class="col-sm-8">    
<textarea class="form-control" rows="3" style="resize:none" name="dislikes">${sessionScope.user.dislikes}</textarea>
        </div>
      </div>
      </div>
 
      </div>	

</div>
<hr>
<div class="page_submit">
<input type="button" value="Update" onclick="javascript:doUpdate()"></div>
 
							</div> <!-- / .tab-pane -->
						</div>



     </div>
 





   </form>

 </div>
  
</div>
</div>

 
  </div>
    </section>
    <!-- Recent Benefits Feedback Section End Here -->
    
    <!-- Footer Start Here -->
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enter email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    	<link href="css/checkbox.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap-select.js"></script> 
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
	<script src="js/jquery-ui.js"></script>
	<script>
		$(document).ready(function() {
			$("#dob").datepicker({dateFormat:"dd/mm/yy",changeYear:true, changeMonth:true, showMonthAfterYear:true,yearRange:"1900:2000"});
		});
		
		function changeVisibility(property, visibility, element) {
			var a = property + visibility;
			var tag = "";
			tag = tag.concat("<span id='", property, "'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='images/greenCheck.jpg'/><input type='hidden' name='", property, "' value='", visibility, "'/></span>");
			$("#" + property).remove();
			$("#" + a).append(tag);
		}

		function doUpdate() {
			var languages = "";
			var array = $('#language').val();
			
			if (array != null) {
				for (var i=0;i<array.length;i++) {
					  languages += array[i];
					  if (i < array.length - 1) {
						  languages += ",";
					  }
					}
			}
			$('form').append("<input type='hidden' name='language' value='" + languages + "'/>");
			$('form').submit();
		}

		function numbersonly(myfield, e, dec) {
			var key;
			var keychar;

			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);

			if ((key==null) || (key==0) || (key==8) || 
		    (key==9) || (key==13) || (key==27) )
		   		return true;

			else if ((("0123456789").indexOf(keychar) > -1))
		   		return true;
			else if (dec && (keychar == ".")) {
				myfield.form.elements[dec].focus();
				return false;
			} else
		   		return false;
		}
		
		function previewImage() {
			if (document.getElementById("photo").files[0].name.substring(
					document.getElementById("photo").files[0].name.length - 3,
					document.getElementById("photo").files[0].name.length) != "jpg"
					&& document.getElementById("photo").files[0].name
							.substring(
									document.getElementById("photo").files[0].name.length - 3,
									document.getElementById("photo").files[0].name.length) != "png") {
				document.getElementById('photo').value = '';
				$("#photo").val("");
				alert("Only JPG or PNG files please.");
				setTimeout(function() {
					$("span").remove('.file-holder');
				}, 500);
				return;
			}
			var oFReader = new FileReader();
			if (document.getElementById("photo").files[0].size > 51200) {
				alert("Please upload an image of size less than 50kB.");
				$("#photo").val("");
				setTimeout(function() {
					$("span").remove('.file-holder');
				}, 500);
				return;
			}
			oFReader.readAsDataURL(document.getElementById("photo").files[0]);

			oFReader.onload = function(oFREvent) {
				document.getElementById("avatar").src = oFREvent.target.result;
			};
			setTimeout(function() {
				$("span").remove('.file-holder');
			}, 500);
		};
		</script>
<style>
.selectricWrapper {
  position: relative;
  margin: 0 0 10px;
  width: 0px;
  cursor: pointer;
  float:left;
}
</style>
	<link href="css/jquery-ui.css" rel="stylesheet">
	<script src="js/jquery-ui.js"></script>
	<link href="css/selectric.css" rel="stylesheet">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<script src="js/jquery.selectric.js"></script>
	<link href="css/style.css" rel="stylesheet">
	<script src="js/jquery.multi-select.js"></script>
	<link href="css/multi-select.css" rel="stylesheet">
	<script>
		$(document).ready(function() {
			var array = "${sessionScope.user.language}".split(","); 
			$('#language').multiSelect();
			$("#language").multiSelect('select', array);
			$('.selectrik').selectric({inheritOriginalWidth	:true});
			$("#dob").datepicker({dateFormat:"dd/mm/yy",changeYear:true, changeMonth:true, showMonthAfterYear:true,yearRange:"1900:2000"});
		});
		</script>
</body>
</html>