<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


<style>
.select2-search-choice-close:before {
  content: "xxx";
}
</style>
  </head>
  <body>

    <!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->
    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
      <li href="message.html" class="active">Write Message</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
<div class="container">
<div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>

 <div id="page-message-wrapper" class="col-sm-8 col-md-10 page-mail show-mail">
<div class="mail-container">
  <div class="mail-container-header show"> New Message </div>
  <form class="new-mail-form form-horizontal" action="createMessage" method="post">
  <div class="row form-group">
          <label for="s2id_autogen1" class="col-sm-2 control-label">To:</label>
          <div class="col-sm-10 select2-primary">
            <select id="users" class="select2" multiple="multiple">
<c:forEach var="user" items="${users}">
<option value="${user.email}">${user.fname } ${user.lname }</option>
</c:forEach>
  </select>
             
          </div>
        </div>
        <div class="row form-group">
        <div class="col-sm-offset-2 col-sm-10 ">
          <textarea class="form-control" rows="3" placeholder="Write message here..." name="message" id="textMessage">${message.message}</textarea>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-offset-2 col-sm-6">
            <button class="btn btn-primary" type="button" onclick="javascript:doSubmit()">Send</button>
            <button class="btn btn-primary" type="button" onclick="javascript:window.location.href='messages.jsp'">Cancel</button>
          </div>
          
        </div>
  </form>
</div>

          </div>



  <!-- Right sidebar end Here -->
   
      </div><!-- row end Here -->



</div><!-- container end Here -->
    
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="Javascript:void(0)">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  

    <!-- Footer End Here -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/select2.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
   

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
     <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/custom.js"></script>
    <script type="text/javascript">
    // select2 block.
    $( document ).ready(function() {
     $("#users").select2();
     var toUser = "${toUser}";
     if (toUser != null && toUser != "" && toUser != "null")
     $("#users").select2("val", "${toUser}");
 	var myClass = toUser.replace(/\./g, "-").replace(/\@/g, "-");
 	$("form").append("<input type='hidden' name='emails' value='" + toUser + "' + class='" + myClass + "'/>");
    });
    
    $("#users").on("select2-selecting", function(e) {
    	var myClass = e.val.replace(/\./g, "-").replace(/\@/g, "-");
    	$("form").append("<input type='hidden' name='emails' value='" + e.val + "' + class='" + myClass + "'/>");
    });
    $("#users").on("select2-removing", function(e) {
    	var myClass = e.val.replace(/\./g, "-").replace(/\@/g, "-");
    	$("form").find("." + myClass).remove();
    });
    
    function doSubmit() {
    	if ($("#textMessage").val() == null || $("#textMessage").val().trim() == "") {
    		return;
    	}
    	if ($("#users").select2('data') == "") {
    		alert("Please select at least one user.");
    		return false;
    	}
    	$("form").submit();
    }
    </script>

  </body>
</html>
