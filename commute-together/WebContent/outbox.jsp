<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
    function checkMessages(option) {
    	if (option == 0) {
    		$('input:checkbox').not(this).prop('checked', false);
    		$('input:checkbox').not(this).prop('checked', 'checked');
    	} else if (option == 1) {
    		$('input:checkbox').not(this).prop('checked', false);
    		$('.read').not(this).prop('checked', 'checked');
    	} else if (option == 2) {
    		$('input:checkbox').not(this).prop('checked', false);
    		$('.unread').not(this).prop('checked', 'checked');
    	} else if (option == 3) {
    		$('input:checkbox').not(this).prop('checked', false);
    	} else if (option == 4) {
    		$('.read').not(this).prop('checked', false);
    	} else if (option == 5) {
    		$('.unread').not(this).prop('checked', false);
    	}
    }
    </script>
  </head>
  <body>

    <!-- Header Start Here -->
    <jsp:include page="header.jsp"></jsp:include>
    <!-- Header End Here -->
    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
      <li href="showMessages" class="active">Outbox</li>
   
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
<div class="container">
<div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>

 <div id="page-message-wrapper" class="col-sm-8 col-md-10 page-mail">
          <div class="bs-example">
          <form action="deleteMessages" method="post">
          <div class="mail-container">
    <div class="ItemHeader">
            <span class="ProfileHeader">
             <span id="header_label">Outbox</span></span>
                </div>

    <div class="mail-controls clearfix">
      <div role="toolbar" class="btn-toolbar wide-btns pull-left">

        <div class="btn-group">
          <div class="btn-group">
            <button data-toggle="dropdown" class="btn dropdown-toggle" type="button"><i class="fa fa-check-square-o"></i>&nbsp;<i class="fa fa-caret-down"></i></button>
            <ul role="menu" class="dropdown-menu">
              <li><a href="#" onclick="javascript:checkMessages(0); return false;">Check all</a></li>
              <li><a href="#" onclick="javascript:checkMessages(1); return false;">Check read</a></li>
              <li><a href="#" onclick="javascript:checkMessages(2); return false;">Check unread</a></li>
              <li class="divider"></li>
              <li><a href="#" onclick="javascript:checkMessages(3); return false;">Uncheck all</a></li>
              <li><a href="#" onclick="javascript:checkMessages(4); return false;">Uncheck read</a></li>
              <li><a href="#" onclick="javascript:checkMessages(5); return false;">Uncheck unread</a></li>
            </ul>
          </div>
          <button class="btn" type="button" onclick="window.location.href='showMessages'"><i class="fa fa-repeat"></i></button>
        </div>

        <div class="btn-group">
          <button class="btn" type="button" onclick="window.location.href='newMessage'"><i class="fa fa fa-file-text-o"></i></button>
          <button class="btn trashBtn" type="button" onclick="javascript:doSubmit()"><i class="fa fa-trash-o"></i></button>
        </div>

      </div>

<!--       <div role="toolbar" class="btn-toolbar pull-right">
        <div class="btn-group">
          <button class="btn" type="button"><i class="fa fa-chevron-left"></i></button>
          <button class="btn" type="button"><i class="fa fa-chevron-right"></i></button>
        </div>
      </div>
      <div class="pages pull-right">
        1-50 of 825
      </div>
 -->    </div>


    <ul class="mail-list">
	<c:forEach items="${messages}" var="message"> 
      <li class="mail-item unread">
        <div class="m-chck"><label class="px-single"><input type="checkbox" <c:if test="${message.messages.isread == 0}">class="unread"</c:if> <c:if test="${message.messages.isread == 1}">class="read"</c:if> value="${message.messages.id}" name="ids"><span class="lbl"></span></label></div>
        <!--<div class="m-star"><a href="#"></a></div> -->
        <div class="m-from"><a href="userProfile.jsp?profileId=${message.toUser.email}">${message.toUser.fname} ${message.toUser.lname}</a></div>
        <div class="">&nbsp;&nbsp;<a href="viewMessage?id=${message.messages.id}&outbox=true">
        <c:if test="${message.messages.isread == 0}"><b>${message.messages.subject}</b></c:if>
        <c:if test="${message.messages.isread == 1}">${message.messages.subject}</c:if>
        </a></div>
        <div class="m-date"><fmt:formatDate pattern='dd/MM/yyyy' value='${message.messages.createdtime}'/></div>
      </li>
    </c:forEach>
    </ul>

  </div></form>

          </div>
          </div>



  <!-- Right sidebar end Here -->
   
      </div><!-- row end Here -->



</div><!-- container end Here -->
    
    <footer>
        <div class="container">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="Javascript:void(0)">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
    <script>
    function doSubmit() {
    	$('form').attr("action", "deleteMessages?outbox=true");
    	$('form').submit();
    }
  		var tid;
    	
    	function updateDelete() {
        	if ($("input:checkbox:checked").length > 0) {
        		$(".trashBtn").prop("disabled", false);
        	} else {
        		$(".trashBtn").prop("disabled", true);
        	}
    		tid = setTimeout(updateDelete, 300); 
    	}
    	tid = setTimeout(updateDelete, 300);
    </script>
  </body>
</html>