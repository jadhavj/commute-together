<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <!-- Header Start Here -->
    <header>
        <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 logo">
                <a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="top_links">
                    <div class="login_register">
                        <ul>
                            <li><a href="index.jsp">Login</a></li>
                            <li><a href="registration.jsp">Register</a></li>
                        </ul>
                    </div>
                    <div class="post_ride">
								<a onclick="javascript:alert('Login to start riding!'); return false;">Post Ride</a>
                    </div>
                </div>
            </div>
            </div>
        </div>  
    </header>
    <!-- Header End Here -->
    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/">Home</a></li>
      <li class="active">FAQ</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->
<div class="container">
<div class="row">
<div id="faqs-main" class="col-md-12">
<h1 class="page-header">Privacy Policy</h1>
<div class="form-section-wrapper">
<div class="form_section_title">All you need to know about car-sharing</div>
Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Praesent Vestibulum Molestie Lacus. Aenean Nonummy Hendrerit Mauris. Phasellus Porta Fusce Suscipit Varius Mi. Cum Sociis Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus Mus. Nulla Dui Usce Feugiat Malesuada Odio. Morbi Nunc Odio, Gravida At, Cursus Nec, Luctus A, Lorem. Maecenas Tristique Orci Ac Sem. Duis Ultricies Pharetra Magna. Donec Accumsan Malesuada Orci. Donec Sit Amet Eros. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Mauris Fermentum Dictum Magna. Sedume Laoreet Aliquam Leo. Ut Tellus Dolor, Dapibus Eget, Elementum Vel, Cursus Eleifend, Elit. Aenean Auctor Wisi Et Urna. Aliquam Erat Volutpat. Duis Ac Turpis. Integer Rutrum Ante Eu Lacus. Quisque Nulla. Vestibulum Libero Nisl, Porta Vel, Scelerisque Eget, Malesuada At, Neque. Vivamus Eget Nibh. Etiam Cursus Leo Vel Metus. Nulla Facilisi. Aenean Nec Eros. Vestibulum Ante Ipsum Primis In Faucibus Orci Luctus Et Ultrices Posuere Cubilia Curae; Suspendisse Sollicitudin Velit Sed Leo. Ut Pharetra Augue Nec Augue. Nam Elit Magna, Hendrerit Sit Amet, Tincidunt Ac, Viverra Sed, Nulla. Donec Porta Diam Eu Massa. Quisque Diam Lorem, Interdum Vitae, Dapibus Ac, Scelerisque Vitae, Pede.
Donec Eget Tellus Non Erat Lacinia Fermentum. Donec In Velit Vel Ipsum Auctor Pulvinar. Proin Ullamcorper Urna Et Felis. Vestibulum Iaculis Lacinia Est. Proin Dictum Elementum Velit. Fusce Euismod Consequat Ante. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Pellentesque Sed Dolor. Aliquam Congue Fermentum Nisl. Mauris Accumsan Nulla Vel Diam. Sed In Lacus Ut Enim Adipiscing Aliquet. Nulla Venenatis. In Pede Mi, Aliquet Sit Amet, Euismod In, Auctor Ut, Ligula. Aliquam Dapibus Tincidunt Metus. Praesent Justo Dolor, Lobortis Quis, Lobortis Dignissim, Pulvinar Ac, Lorem. Vestibulum Sed Ante. Donec Sagittis Euismod Purus. Sed Ut Perspiciatis Unde Omnis Iste Natus Error Sit Voluptatem Accusantium Doloremque Laudantium, Totam Rem Aperiam, Eaque Ipsa Quae Ab Illo Inventore Veritatis Et Quasi Architecto Beatae Vitae Dicta Sunt Explicabo. Nemo Enim Ipsam Voluptatem Quia Voluptas Sit Aspernatur Aut Odit Aut Fugit, Sed Quia Consequuntur Magni Dolores Eos Qui Ratione Voluptatem Sequi Nesciunt. Neque Porro Quisquam Est, Qui Dolorem Ipsum Quia Dolor Sit Amet, Consectetur, Adipisci Velit, Sed Quia Non Numquam Eius Modi Tempora Incidunt Ut Labore Et Dolore Magnam Aliquam Quaerat Voluptatem. Ut Enim Ad Minima Veniam, Quis.
Nostrum Exercitationem Ullam Corporis Suscipit Laboriosam, Nisi Ut Aliquid Ex Ea Commodi Consequatur? Quis Autem Vel Eum Iure Reprehenderit Qui In Ea Voluptate Velit Esse Quam Nihil Molestiae Consequatur, Vel Illum Qui Dolorem Eum Fugiat Quo Voluptas Nulla Pariatur. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Praesent Vestibulum Molestie Lacus. Aenean Nonummy Hendrerit Mauris. Phasellus Porta Fusce Suscipit Varius Mi. Cum Sociis Natoque Penatibus Et Magnis Dis Parturient Montes, Nascetur Ridiculus Mus. Nulla Dui Usce Feugiat Malesuada Odio. Morbi Nunc Odio, Gravida At, Cursus Nec, Luctus A, Lorem. Maecenas Tristique Orci Ac Sem. Duis Ultricies Pharetra Magna. Donec Accumsan Malesuada Orci. Donec Sit Amet Eros. Lorem Ipsum Dolor Sit Amet, Consectetuer Adipiscing Elit. Mauris Fermentum Dictum Magna. Sedume Laoreet Aliquam Leo. Ut Tellus Dolor, Dapibus Eget, Elementum Vel, Cursus Eleifend, Elit. Aenean Auctor Wisi Et Urna. Aliquam Erat Volutpat. Duis Ac Turpis. Integer Rutrum Ante Eu Lacus. Quisque Nulla. Vestibulum Libero Nisl, Porta Vel, Scelerisque Eget, Malesuada At, Neque. Vivamus Eget Nibh. Etiam Cursus Leo Vel Metus. Nulla Facilisi. Aenean Nec Eros. Vestibulum Ante Ipsum Primis In Faucibus Orci Luctus Et Ultrices Posuere.
E-Mail: INFO@DEMOLINK.ORG
    
</div>

</div>
</div>
    
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
                        <li><a href="aboutus.jsp">About Us</a></li>
                        <li><a href="faqs.jsp">FAQs</a></li>
                        <li><a href="Javascript:void(0)">Privacy Policy</a></li>
                        <li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/custom.js"></script>
  </body>
</html>