<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<%
    if (request.getAttribute("allClear") == null) { session.removeAttribute("user"); }
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
		    %>
		    <script type="text/javascript">
		    alert("<%=entry.getValue()%>");
		    </script>
		    <%
		    break;
		}
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<style>
.selectricWrapper {
  position: relative;
  margin: 0 0 10px;
  width: 0px;
  cursor: pointer;
  float:left;
}
</style>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Registration</title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<!-- Header Start Here -->
	<header>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 logo">
					<a href="${fn:substring(url, 0, fn:length(url) - fn:length(uri))}${req.contextPath}/"><img src="images/logo.png" alt="Commute together" /></a>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="top_links">
						<div class="login_register">
							<ul>
							</ul>
						</div>
 						</div>
				</div>
			</div>
		</div>
	</header>
	<!-- Header End Here -->

	<!-- Recent Benefits Feedback Section Start Here -->
	<section class="reg_main">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="bs-example">
						<div class="ItemHeader">
							<span class="ProfileHeader"> <span id="header_label">Registration</span></span>
						</div>
						<form role="form" class="form-horizontal" action="registerFirst"
							method="POST">
							<div class="form-section-wrapper">
								<div class="form_section_title">User Details</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group required">
										<label for="inputEmail3" class="col-sm-4 control-label">Email
											Address<sup class="required">*</sup>
										</label>
										<div class="col-sm-8">
											<input type="email" required="required" class="form-control" maxlength="255"
												id="inputEmail3" placeholder="Email" name="email"
												value="${user['email']}"
												<c:forEach var="email" items="${errors['email']}">style="border-color:red"</c:forEach>>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">First
											Name</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" placeholder="" maxlength="45"
												name="fname" value="${user['fname']}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Organization
											Name</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" placeholder="" maxlength="45"
												name="organization" value="${user['organization']}">
										</div>
									</div>
									<div class="form-group required">
										<label class="col-xs-12 col-sm-4 control-label" for="">Gender<sup
											class="required">*</sup></label>
										<div class="col-xs-12 col-sm-8" style="float:left">
											<select required="required" name="gender" style="width:200px" id="gender">
												<option disabled selected>Select Gender</option>
												<option value="0">Male</option>
												<option value="1">Female</option>
											</select>&nbsp;&nbsp;&nbsp;&nbsp; <a class="info" href="#" data-trigger="hover"
												data-toggle="tooltip" data-placement="top"
												title="For Co-passengers comfort level."><span>?</span></a>

										</div>

									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group required">
										<label class="col-sm-4 control-label" for="">Mobile
											Number<sup class="required">*</sup>
										</label>
										<div class="col-sm-8">
											<input type="text" maxlength="10" required="required"
												class="form-control" placeholder="" name="mobile" 
												id="mobile" value="${user['mobile']}"
												onkeypress="return numbersonly(this, event)"
												<c:forEach var="mobile" items="${errors['mobile']}">style="border-color:red"</c:forEach>>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Last Name</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" placeholder="" maxlength="45"
												name="lname" value="${user['lname']}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label" for="">Occupation</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" placeholder="" maxlength="45"
												name="occupation" value="${user['occupation']}">
										</div>

									</div>
									<div class="form-group required">
										<label class="col-xs-12 col-sm-4 control-label" for="">D.O.B<sup
											class="required">*</sup></label>

										<div class="col-xs-10 col-sm-4">
											<input type="text" class="form-control" name="dob" id="dob" placeholder="dd/mm/yyyy"
												value="<fmt:formatDate pattern="dd/MM/yyyy" value="${user['dob']}"/>"
												<c:forEach var="dob" items="${errors['dob']}">style="border-color:red"</c:forEach>>
												
										</div>
										<a class="info dob" href="#" data-trigger="hover"
											data-toggle="tooltip" data-placement="top"
											title="As per regulations user with age under 18 require parent guidance."><span>?</span></a>
									</div>
								</div>
							</div>
							<hr />

							<div class="form-section-wrapper">
								<div class="form_section_title">Details for security</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group required">
										<label class="col-sm-4 control-label" for="">ID proof<sup
											class="required">*</sup></label>
										<div class="col-sm-8">
											<input type="text" required="required" class="form-control"
												placeholder="" name="idproof" value="${user['idproof']}" id="idproof" maxlength="45"
												<c:forEach var="idproof" items="${errors['idproof']}">style="border-color:red"</c:forEach>>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-4 control-label" for="">Emergency
											Contact No.<sup class="required">*</sup>
										</label>
										<div class="col-sm-8">
											<input type="text" maxlength="10" required="required" id="emergency"
												class="form-control"
												onkeypress="return numbersonly(this, event)" placeholder=""
												name="emergency" value="${user['emergency']}"
												<c:forEach var="emergency" items="${errors['emergency']}">style="border-color:red"</c:forEach>>
										</div>
									</div>
									<!-- Add this line in your form -->
									<div class="form-group captcha_section">
										<label class="col-sm-4 control-label" for=""><div
												class="robot-class">
												<div class="human">Human or Robot</div>
												<span class="slide-submit">Slide before submitting</span>
											</div></label>
										<div class="QapTcha col-xs-12 col-sm-8"></div>
									</div>

								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 row">
									<div class="form-group required">
										<label class="col-sm-4 control-label" for="">Blood
											Group<sup class="required">*</sup>
										</label>
										<div class="col-sm-8">
											<select name="bloodgroup" style="width:200px" id="bloodgroup">
												<option disabled selected>Select Blood Group</option>
												<option value="0">A1 Negative (A1 -ve)</option>
												<option value="1">A1 Positive (A1 +ve)</option>
												<option value="2">A1B Negative (A1B -ve)</option>
												<option value="3">A1B Positive (A1B +ve)</option>
												<option value="4">A2 Negative (A2 -ve)</option>
												<option value="5">A2 Positive (A2 +ve)</option>
												<option value="6">A2B Negative (A2B -ve)</option>
												<option value="7">A2B Positive (A2B +ve)</option>
												<option value="8">B Negative (B -ve)</option>
												<option value="9">B Positive (B +ve)</option>
												<option value="10">B1 Positive (B1 +ve)</option>
												<option value="11">O Negative (O -ve)</option>
												<option value="12">O Positive (O +ve)</option>
											</select>
										</div>
									</div>
									<div class="form-group required">
										<label class="col-sm-4 control-label" for="">Insurance
											No.<sup class="required">*</sup>
										</label>
										<div class="col-sm-8">
											<input type="text" required="required" class="form-control" id="insurance" maxlength="45"
												placeholder="" name="insurance" value="${user['insurance']}"
												<c:forEach var="insurance" items="${errors['insurance']}">style="border-color:red"</c:forEach>>
										</div>
									</div>
								</div>

							</div>
							<div class="req-section">
								<p class="required">
									<sup class="required">*</sup>Required Fields
								</p>
							</div>

							<div class="page_submit" align="right">
								<p class="required" align="right">
									<sup class="required">*</sup>By clicking "Save and Continue", you agree to the <a href="javascript:showTerms()">Term and Conditions</a>.
								</p>
								<a href="javascript:doSubmit()"><input type="button"
									value="Save & Continue"></a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Recent Benefits Feedback Section End Here -->

	<!-- Footer Start Here -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4 about_us">
					<h3>About Carpooling</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum fermentum placerat convallis. Quisque pharetra cursus
						enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate
						quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh.
						Aenean at quam faucibus lacus ullamcorper dapibus sit amet at
						neque. Etiam porttitor erat id libero dapibus, at dictum nulla
						tempus. <a href="Javascript:void(0)">More >></a>
					</p>
				</div>
				<div class="col-md-3">
					<div class="footer_links">
						<ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-5">
					<div class="follow_us">
						<div class="clearfix">
							<h3>Follow Us:</h3>
							<div class="social_icons">
								<ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
								</ul>
							</div>
						</div>
						<div class="news_letter_main">
							<h3>Sign Up for Newsletter</h3>
							<input type="text" placeholder="Enter email address"
								name="newsletter"><input type="button" value="Sign Up">
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer End Here -->
	<link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css"
		media="screen" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/jquery-ui.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/bootstrap-select.min.css" rel="stylesheet">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/clone-form-td.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/bootstrap-select.js"></script>
	<script src="js/moment.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.touch.js"></script>
	<script type="text/javascript" src="js/QapTcha.jquery.js"></script>
	<script src="js/custom.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<link href="css/selectric.css" rel="stylesheet">
	<script src="js/jquery.selectric.js"></script>
	<script src="js/jquery.bpopup.min.js"></script>
	<div style="display:none;background-color:white;width:400px;height:400px;padding:20px" class="re_ben_fed user_list tandc">
<div id="popup" style="width:370px;height:310px;overflow-y:auto" >
<pre>
Terms and Conditions

GENERAL

Servicing servicing resistor video computer element transponder prototype analog solution mainframe network. Solution video debugged feedback sampling extended, infrared, generator generator. 

* list item number one
* list item number two
* list item number three

Harmonic gigabyte generator in sequential interface. Services, integer device read-only supporting cascading cache capacitance. Proxy boolean solution or data overflow element overflow processor arrray video, reflective extended. Gigabyte debugged distributed, reflective controller disk encapsulated phase network arrray feedback element cache high silicon. Sampling backbone analog remote adaptive extended bridgeware n-tier integer audio femtosecond. 

IMPORTANT STUFF

Interface broadband developer backbone fragmentation messaging software transmission, sampling cascading element high silicon backbone. Reflective servicing coordinated boolean connectivity extended inversion sequential for. Data distributed backbone bridgeware connectivity logarithmic, sampling ethernet for. Extended logarithmic log video echo gigabyte. Or procedural phaselock sequential port extended deviation sequential, disk recognition gigabyte phaselock proxy messaging arrray. Encapsulated echo deviation boolean system ethernet reducer, pc. 

Backbone frequency echo ethernet patch femtosecond sampling, integer digital floating-point n-tier dithering disk. Processor reflective boolean includes phase deviation, with log, integral logistically femtosecond, in, integral infrared phaselock. Element developer pc harmonic, plasma with, integral floating-point. Reflective feedback infrared, echo anomoly integral generator reflective led, high overflow servicing gigabyte. Extended integral data, infrared frequency disk broadband transistorized processor disk anomoly, sampling potentiometer kilohertz. Bus overflow, bridgeware fragmentation bypass prototype, interface, for internet pc generator integral, bypass disk, frequency. 

FEES

Sequential ENCAPSULATED RESISTOR element converter pc harmonic. Feedback cache hyperlinked debugged generator infrared device network, fragmentation connectivity, debugged. For, dithering interface debugged sequential recognition bypass bridgeware processor backbone sampling logarithmic software. Anomoly infrared logarithmic logarithmic procedural plasma resistor integral silicon deviation. Computer broadband plasma, encapsulated cable kilohertz scan. With logarithmic bridgeware indeterminate sampling, port. Development procedural hyperlinked mainframe, encapsulated boolean software processor transistorized cable with backbone. 

Reflective backbone log transmission cable logarithmic mainframe. Messaging backbone debugged feedback development phaselock metafile n-tier coordinated, cable coordinated coordinated in. Analog device transistorized, sequential transistorized with scalar normalizing. Mainframe phase cache anomoly feedback servicing harmonic. Services software pc, femtosecond bypass scalar data element extended cascading capacitance harmonic. Adaptive plasma bypass supporting cascading deviation system pulse broadband bridgeware. 

AGREEMENT

Servicing hyperlinked analog encapsulated disk cable with, capacitance element partitioned potentiometer read-only dithering. Normalizing kilohertz network solution logarithmic device bus connectivity system mainframe prototype phaselock phase cache. 

Coordinated transmission overflow floating-point distributed supporting scan converter. Bridgeware backbone coordinated sampling overflow broadband normalizing connectivity or, internet transmission, bus, encapsulated. Extended element element echo messaging scan, cache video deviation debugged broadband. Pc, supporting hyperlinked coordinated patch, bridgeware transistorized, led frequency scalar. Servicing, plasma recursive boolean includes, computer, femtosecond inversion. Services integer harmonic mainframe scalar prototype broadband n-tier. Echo software application indeterminate dithering bypass plasma bus harmonic patch. Digital, mainframe scalar anomoly bypass, bus potentiometer phaselock plasma, phaselock prompt data disk inversion. 


</pre>
    </div>	<br/>
    <div align="center" style="width:100%">
        <span class="button b-close"><span>Close</span></span>
    </div>
    </div>
	<script>
		$(document).ready(function() {
			$("div").remove(".dropError");
			$("#dob").datepicker({dateFormat:"dd/mm/yy",changeYear:true, changeMonth:true, showMonthAfterYear:true,yearRange:"1900:2000"});
			$('select').selectric({inheritOriginalWidth	:true});
		});
		
		function numbersonly(myfield, e, dec) {
			var key;
			var keychar;

			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);

			if ((key==null) || (key==0) || (key==8) || 
		    (key==9) || (key==13) || (key==27) )
		   		return true;

			else if ((("0123456789").indexOf(keychar) > -1))
		   		return true;
			else if (dec && (keychar == ".")) {
				myfield.form.elements[dec].focus();
				return false;
			} else
		   		return false;
		}
		
		function canSubmit() {
			if ($(".Slider").attr("style") == null || $(".Slider").attr("style") == "" || $(".Slider").attr("style") == "left: 0px; top: 0px;") {
				return false;
			} else {
				return true;
			} 
		}
		
		function doSubmit() {
			if ($("#gender").val() == null) {
				alert("Please specify your gender.");
				return;
			}
			if ($("#mobile").val().length != 10) {
				alert("Mobile no. should be 10 digit long.");
				return;
			}
			var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}$/
				if ($("#dob").val().match(dateReg) == null) {
					alert("Date of birth is in invalid format.");
					return;
				}
			if ($("#idproof").val() == "") {
				alert("ID Proof is required.");
				return;
			}
			if ($("#bloodgroup").val() == null) {
				alert("Please specify your bloodgroup.");
				return;
			}
			if ($("#emergency").val().length != 10) {
				alert("Emergency contact no. should be 10 digit long.");
				return;
			}
			if ($("#emergency").val() == $("#mobile").val()) {
				alert("Mobile no. and emergency contact no. are the same.");
				return;
			}
			if ($("#insurance").val() == "") {
				alert("Insurance no. is required.");
				return;
			}
			
			if (canSubmit()) {
				$('form').submit();
			} else {
				alert("Please slide the slider to verify that you're a human.")
			}
		}
		function showTerms() {
			$('.tandc').bPopup({
	            modalClose: false,
	            opacity: 0.6,
	            positionStyle: 'fixed' //'fixed' or 'absolute'
	        });			
		}
	</script>
</body>
</html>