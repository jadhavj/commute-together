<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-ui.js"></script>
  <script type="text/javascript" src="js/jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />
	<link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
	<style>
		.autocomplete-suggestions { border: 1px solid #999; background: #fff; cursor: default; overflow: auto; }
		.autocomplete-suggestion { padding: 10px 5px; font-size: 1.2em; white-space: nowrap; overflow: hidden; }
		.autocomplete-selected { background: #f0f0f0; }
		.autocomplete-suggestions strong { font-weight: normal; color: #3399ff; }

	.ui-autocomplete {
		max-height: 100px;
		overflow-y: auto;
		/* prevent horizontal scrollbar */
		overflow-x: hidden;
	}
	/* IE 6 doesn't support max-height
	 * we use height instead, but this forces the menu to always be this tall
	 */
	* html .ui-autocomplete {
		height: 100px;
	}
  </style>
  <script src="js/rideDetails.js"></script>
  </head>
  
  <body>
<c:if test="${sessionScope.created}">
<c:remove var="created" scope="session" />
<script>
alert("Ride successfully created.");
</script>
</c:if>

<!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->

    <!-- Recent Benefits Feedback Section Start Here -->
    <section class="ride_details_main">
        <div class="container">
        <div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>
        <div id="#page-content-wrapper" class="col-sm-8 col-md-10">
          <div class="bs-example">
          <div class="ItemHeader">
                        <span class="ProfileHeader">
                            <span id="header_label">Create Ride</span></span>
                    </div>
     <form role="form" class="form-horizontal" method="post" action="createRide" enctype="multipart/form-data">
     <input type="hidden" name="whichWay" id="whichWay" value="1"/>
     <input type="hidden" name="multiways" id="multiways" value="1"/>
    <div class="form-section-wrapper">
<div class="btn-group details-types" data-toggle="buttons">
  <a class="btn btn-primary active" href="#oneway" onclick="javascript:setupWay(1)">
    <input type="radio" name="options" id="option1" checked>One Way</a>
  <a class="btn btn-primary" href="#twoways" onclick="javascript:setupWay(2)">
    <input type="radio" name="options" id="option2" href="#twoways">Two Ways</a>
  <a class="btn btn-primary" href="#multipleways" onclick="javascript:setupWay(3)">
    <input type="radio" name="options" id="option3" href="#multipleways">Multiple Ways</a>
</div>
<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
        <label class="col-sm-4 control-label" for="">Ride Type</label>
        <div class="col-sm-8">    
    <select name="rideType" class="myselect">
     <option value="0">One Time Ride</option>
     <option value="1">Weekday Ride (MON-FRI)</option>
     <option value="2">Weekend Ride (SAT-SUN)</option>
</select>
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-8">
    <div class="row">
             <div class="form-group col-md-6">
             <div class="row">
        <label class="col-xs-12 col-sm-4 col-md-4 control-label" for="">Start Date<sup class="required">*</sup></label>
     <div class="col-sm-4 col-md-8 input-group date datepicker">
    <input type="text" class="form-control" id="startDate" name="startDate" placeholder="dd/mm/yyyy">
    </div>
    </div>
      </div>
      <div class="form-group col-md-6">
       <div class="row">
        <label class="col-xs-12 col-sm-4 col-md-4 control-label" for="">End Date</label>
              <div class="col-sm-4 col-md-8 input-group date datepicker">
    <input type="text" class="form-control" id="endDate" name="endDate" placeholder="dd/mm/yyyy">
    </div>
    </div>
      </div>
      </div>
    </div>
    </div>
    </div>
    <hr/>

 <div id="oneway" class="form-section-wrapper col-xs-12 col-sm-12 col-md-12 row tab-content">
 <div id="" name="" class="form_section_title">Ride Details <span style="color:#FF6600;text-transform:none;font-style:italic">(After entering address press enter key in text boxes to get matches)</span></div>
 
 <table width="100%"><tr><td width="50%">
 
         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Start<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control" name="startLoc" id="startLoc" placeholder="">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker start-icon"></i></span>
        </div>
        </div>
        <div class="form-group">
        <label class="col-sm-4 control-label" for="">Destination<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group">    
    <input type="text" required="required" class="form-control" name="endLoc" placeholder="" id="endLoc">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker dest-icon"></i></span>
        </div>
        </div>
<!--         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Route<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group route-selection">    
    <select class="selectpicker">
     
</select>
<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker route-icon"></i></span>
        </div>
      </div>
 -->
 
         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Checkpoints</label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control" name="wayPoint" id="wayPoint" placeholder="">
    <span class="input-group-addon" onclick="javascript:addWayPoint()"><i class="glyphicon"><img src="images/plus-add.png" height="15px"/></i></span>
        </div>
        </div>
         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group">    
      <select class="form-control" size="5" id="wayPoints" name="wayPoints" multiple>
      </select>
        </div>
        </div>


         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group" align="center">    
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:remAllWaypts()">Remove all</button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:remSelWaypts()">Remove selected</button>
        </div>
        </div>

      <div class="form-group">
       <label class="col-sm-4 control-label" for="">Start Time<sup class="required">*</sup></label>
              <div class="col-sm-6 col-md-2 input-group date timepicker">
                    <input type='text' class="form-control time" id="startTime" name="startTime" style="width:100px"/>
                </div>
            </div>
            </td>
            <td width="50%">
    <div id="map-canvas" class="map-canvas" style="width:500px;height:300px;border:2px solid"></div>
                <div class="actn-sett-btns data">
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:calcRoute()">Build route</button>
      </div>
            </td>
            </tr>
            </table>
            </div>
             <div id="twoways" class="form-section-wrapper col-xs-12 col-sm-12 col-md-12 row tab-content">
             <table width="100%">
             <tr><td colspan="2">
 <div id="" class="form_section_title">Ride Details <span style="color:#FF6600;text-transform:none;font-style:italic">(After entering address press enter key in text boxes to get matches)</span></div>
             </td></tr>
             <tr><td width="50%">
         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Start<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control" name="twoWayStartLoc" id="twoWayStartLoc" placeholder="">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker start-icon"></i></span>
        </div>
        </div>
        <div class="form-group">
        <label class="col-sm-4 control-label" for="">Destination<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group">    
    <input type="text" required="required" class="form-control" name="twoWayEndLoc" placeholder="" id="twoWayEndLoc">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker dest-icon"></i></span>
        </div>
        </div>
        <!-- <div class="form-group">
        <label class="col-sm-4 control-label" for="">Forward Route<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group route-selection">    
    <select class="selectpicker">
     
</select>
<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker route-icon"></i></span>
        </div>
      </div> -->
         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Forward checkpoints</label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control" name="forwardWayPoint" id="forwardWayPoint" placeholder="">
    <span class="input-group-addon" onclick="javascript:addForwardWayPoint()"><i class="glyphicon"><img src="images/plus-add.png" height="15px"/></i></span>
        </div>
        </div>
         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group">    
      <select class="form-control" size="5" id="forwardWayPoints" name="forwardWayPoints" multiple>
      </select>
        </div>
        </div>
         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group" align="center">    
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:remAllForwardWaypts()">Remove all</button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:remSelForwardWaypts()">Remove selected</button>
        </div>
        </div>
<!--       <div class="form-group">
        <label class="col-sm-4 control-label" for="">Backward Route<sup class="required">*</sup></label>
        <div class="col-sm-6 input-group route-selection">    
    <select class="selectpicker">
     
</select>
<span class="input-group-addon"><i class="glyphicon glyphicon-map-marker route-icon"></i></span>
        </div>
      </div>
 -->
         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Backward checkpoints</label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control" name="backwardWayPoint" id="backwardWayPoint" placeholder="">
    <span class="input-group-addon" onclick="javascript:addBackwardWayPoint()"><i class="glyphicon"><img src="images/plus-add.png" height="15px"/></i></span>
        </div>
        </div>
      <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group">    
      <select class="form-control" size="5" id="backwardWayPoints" name="backwardWayPoints" multiple>
      </select>
        </div>
        </div>
         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group" align="center">    
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:remAllBackwardWaypts()">Remove all</button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:remSelBackwardWaypts()">Remove selected</button>
</div>      </div>
      <div class="form-group">
       <label class="col-sm-4 control-label" for="">Start Time<sup class="required">*</sup></label>
              <div class="col-sm-3 col-md-2 input-group date timepicker">
                    <input type='text' class="form-control time" id="twoWayStartTime" name="twoWayStartTime" style="width:100px"/>
                    </span>
                </div>
            </div>
            <div class="form-group">
       <label class="col-sm-4 control-label" for="">Return Time<sup class="required">*</sup></label>
              <div class="col-sm-3 col-md-2 input-group date timepicker">
                    <input type='text' class="form-control time" id="twoWayReturnTime" name="twoWayReturnTime" style="width:100px"/>
                    </span>
                </div>
            </div>
            </td>
            <td width="50%">
<div id="twoway-map-canvas" style="width:500px;height:500px;border:2px solid;display:block"></div>
      <div class="actn-sett-btns data">
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:calcForwardRoute()">Build forward route</button>
        <button type="button" class="btn btn-sm sav_btn" onclick="javascript:calcBackwardRoute()">Build backward route</button>
      </div>
            </td>
            </tr></table>
            </div>
    <div id="multipleways" class="form-section-wrapper tab-content">
 <div id="entry1" class="col-xs-12 col-sm-12 col-md-12 row clonedInput">
 <table>
              <tr><td colspan="2">
 <div id="" class="form_section_title">Ride Details <span style="color:#FF6600;text-transform:none;font-style:italic">(After entering address press enter key in text boxes to get matches)</span></div>
             </td></tr>
 <tr><td width="50%">
         <div class="form-group">
         <label class="col-sm-4 control-label label_dest" for="strtplace">Start<sup class="required">*</sup></label>

        <div class="col-sm-6 input-group">    
    <input type="text" required="required" class="form-control multiwayStartLoc" name="multiwayStartLoc" id="multiwayStartLoc" placeholder="">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker start-icon"></i></span>
        </div>
        </div>
        <div class="form-group">
        <label class="col-sm-4 control-label label_dest" for="destplace">Destination
        <sup class="required">*</sup></label>
        <div class="col-sm-6 input-group">    
    <input type="text" required="required" class="form-control multiwayEndLoc" name="multiwayEndLoc" id="multiwayEndLoc" placeholder="">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker dest-icon"></i></span>
        </div>
        </div>
	         <div class="form-group">
        <label class="col-sm-4 control-label" for="">Checkpoints</label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control multiwayWayPoint" name="multiwayWayPoint" id="multiwayWayPoint" placeholder="">
    <span class="input-group-addon multiwayWayPointAdd" onclick="javascript:addMultiwayWayPoint()"><i class="glyphicon"><img src="images/plus-add.png" height="15px"/></i></span>
        </div>
        </div>
         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group">    
      <select class="form-control multiwayWayPoints" size="5" name="multiwayWayPoints" id="multiwayWayPoints" multiple>
      </select>
        </div>
        </div>

         <div class="form-group">
        <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group" align="center">    
        <button type="button" class="btn btn-sm sav_btn remAllMultiwayWaypts" onclick="javascript:remAllMultiwayWaypts()">Remove all</button>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <button type="button" class="btn btn-sm sav_btn remSelMultiwayWaypts" onclick="javascript:remSelMultiwayWaypts()">Remove selected</button>
</div>      </div>

      <div class="form-group">
       <label class="col-sm-4 control-label" for="">Start Time<sup class="required">*</sup></label>
              <div class="col-sm-3 col-md-2 input-group date timepicker">
                    <input type='text' class="form-control multiwayStartTime time" id="multiwayStartTime" name="multiwayStartTime" style="width:100px"/>
                    </span>
                </div>
            </div>
            </td>
            <td>
    <div id="multiway-map-canvas" class="multiway-map-canvas" style="width:500px;height:300px;border:2px solid"></div>
                <div class="actn-sett-btns data">
        <button type="button" class="btn btn-sm sav_btn calcMultiwayRoute" onclick="javascript:calcMultiwayRoute()">Build route</button>
      </div>
            </td>
            </tr>
            </table>
            </div>

      <div class="clearfix"></div>
            <div id="addDelButtons" class="form-group">
            <label class="col-sm-4 control-label" for=""></label>
             <div class="col-sm-8">
                  <button type="button" id="btnAdd" name="btnAdd" class="btn btn-info">add route</button>
          <button type="button" id="btnDel" name="btnDel" class="btn btn-danger">remove above route</button>
          </div>
            </div>
</div>



<hr/>
<div class="form-section-wrapper">
 <div class="col-xs-12 col-sm-12 col-md-6 row">
<div class="form-group">
<label class="col-sm-4 control-label" for="">Vehicle Type</label>
<div class="col-sm-8 veh-type">
<div class="btn-group" data-toggle="buttons">
  <label class="btn btn-primary active" onclick="javascript:setCar()">
    <input type="radio" name="wheels" id="wheels1" value="4" checked>4 Wheeler</label>
  <label class="btn btn-primary" onclick="javascript:resetCar()">
    <input type="radio" name="wheels" id="wheels2" value="2" >2 Wheeler</label>
</div>
<div class="btn-group" data-toggle="buttons">
  <label class="btn btn-primary active cartypes" id="carAcLabel">
    <input type="radio" name="ac" id="ac1" checked value="0" id="carAc">AC</label>
  <label class="btn btn-primary cartypes">
    <input type="radio" name="ac" id="ac2" value="1" id="carNonAc">Non AC</label>
</div>
<div class="btn-group" data-toggle="buttons">
  <label class="btn btn-primary active cartypes" id="hatchback">
    <input type="radio" name="cartype" id="cartype1" value="0" checked>Hatch Back</label>
  <label class="btn btn-primary cartypes">
    <input type="radio" name="cartype" id="cartype2" value="1">Sedan</label>
     <label class="btn btn-primary cartypes">
    <input type="radio" name="cartype" id="cartype3" value="2">Suv</label>
    <input type="radio" name="cartype" id="cartype0" value="-1" style="display:none">
</div>
</div>
  
</div>
</div>
 <div class="col-xs-12 col-sm-12 col-md-6 row">
 <div class="form-group">
<label class="col-sm-4 control-label" for="">Vehicle No.<sup class="required">*</sup></label>
<div class="col-sm-8">
  <input type="text" class="form-control" placeholder="" required="required" id="vehicleNo" name="vehicleNo"/>
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="">Vehicle Color</label>
<div class="col-sm-8">
  <input type="text" class="form-control" placeholder="" name="vehicleColor"/>
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="">License No.<sup class="required">*</sup></label>
<div class="col-sm-8">
  <input type="text" class="form-control" placeholder="" required="required" id="licenseNo" name="licenseNo"/>
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="">Vehicle Model</label>
<div class="col-sm-8">
  <input type="text" class="form-control" placeholder="" name="model"/>
</div>
</div>
<div class="form-group">
<label class="col-sm-4 control-label" for="">Vehicle Image</label>
<div class="col-sm-8 file-wrapper">
<img alt="photo" id="avatar" style="width: 100px; height: 100px;" src="images/car.jpg"/>
<input type="file" name="photo" id="photo" onchange="javascript:previewImage();"/>
<span class="button upload_pic_btn">Upload Photo</span>
</div>
</div>
</div>
</div>
<div class="req-section"><p><sup class="required">*</sup>Required Fields</p></div>

<div class="page_submit">
<input type="button" value="Submit" onclick="javascript:submitRide()"></div>

 </form>
  </div>
  </div>
  </div>
        </div>
    </section>
    <!-- Recent Benefits Feedback Section End Here -->
    
    <!-- Footer Start Here -->
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enrer email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  

    <!-- Footer End Here -->
    <link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css" media="screen" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/clone-form-td.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
<style>
.selectricWrapper {
  position: relative;
  margin: 0 0 10px;
  width: 0px;
  cursor: pointer;
  float:left;
}
</style>
	<link href="css/selectric.css" rel="stylesheet">
	<script src="js/jquery.selectric.js"></script>
	<script>
		$(document).ready(function() {
			$('.myselect').selectric({inheritOriginalWidth	:true});
		});
		
		function resetCar() {
			$("#cartype0").prop("checked", true);
			$("#cartype1").prop("checked", false);
			$("#cartype2").prop("checked", false);
			$("#cartype3").prop("checked", false);
			$("#carAc").prop("checked", false);
			$("#carNonAc").prop("checked", false);
			$(".cartypes").attr("class", "btn btn-primary cartypes");
			$(".cartypes").attr("disabled", true);
			$(".cartypes").attr("style", "background:white");
		}
		function setCar() {
			$("#cartype0").prop("checked", false);
			$("#cartype1").prop("checked", true);
			$("#cartype2").prop("checked", false);
			$("#cartype3").prop("checked", false);
			$("#carAc").prop("checked", true);
			$("#carNonAc").prop("checked", false);
			$(".cartypes").attr("class", "btn btn-primary cartypes");
			$("#hatchback").attr("class", "btn btn-primary cartypes active");
			$("#carAcLabel").attr("class", "btn btn-primary cartypes active");
			$(".cartypes").attr("disabled", false);
			$(".cartypes").attr("style", "");
		}
		</script>
  </body>
</html>