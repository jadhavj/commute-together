<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<%
	if (request.getAttribute("errors") != null) {
		for (java.util.Map.Entry<String, String> entry: ((java.util.Map<String, String>)request.getAttribute("errors")).entrySet()) {
		    %>
		    <script type="text/javascript">
		    alert("<%=entry.getValue()%>");
		    </script>
		    <%
		    break;
		}
	}
%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-ui.js"></script>
  	<script type="text/javascript" src="js/jquery.timepicker.js"></script>
  	<link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />
	<link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css" media="screen" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/clone-form-td.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
	<script src="js/jquery.webui-popover.min.js"></script>	
	<link rel="stylesheet" href="css/jquery.webui-popover.min.css">
    <script>
	function customFilter(array, terms) {
        arrayOfTerms = terms.split(" ");
        var term = $.map(arrayOfTerms, function (tm) {
             return $.ui.autocomplete.escapeRegex(tm);
        }).join('|');
       var matcher = new RegExp("\\b" + term, "i");
        return $.grep(array, function (value) {
           return matcher.test(value.label || value.value || value);
        });
    };

	var settings = {
			trigger:'hover',
			title:'Boarding Points',
			content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
			width:300,						
			multi:true,						
			closeable:false,
			style:'',
			delay:300,
			padding:true
	};

	$(document).ready(function () {
		//$('a.show-pop').webuiPopover('destroy').webuiPopover(settings);				
    	$("#startTime").timepicker({ 'timeFormat': 'h:i A' });
    	$("#returnTime").timepicker({ 'timeFormat': 'h:i A' });
    	
    	$("#startLoc").keyup(function(e) {
    		var code = e.keyCode || e.which;
    		if (code == 13) {
    			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
    				+ $("#startLoc").val();
    			$.ajax({
    				dataType : "json",
    				url : address,
    				success : function(data) {
    					var startLocs = [];
    					$.each(data.results, function ( index, val ) {
    						startLocs.push({label:val.formatted_address});
    					});
    					$("#startLoc").autocomplete({
    				        source: startLocs,
    				        multiple: true,
    				        mustMatch: false
    				        ,source: function (request, response) {
    				            // delegate back to autocomplete, but extract the last term
    				            response(customFilter(
    				            startLocs, request.term));
    				        },
    				    });
    					$("#startLoc").autocomplete("search");
    				}
    			});
    		}
    	});
    	$("#endLoc").keyup(function(e) {
    		var code = e.keyCode || e.which;
    		if (code == 13) {
    			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
    				+ $("#endLoc").val();
    			$.ajax({
    				dataType : "json",
    				url : address,
    				success : function(data) {
    					var startLocs = [];
    					$.each(data.results, function ( index, val ) {
    						startLocs.push({label:val.formatted_address});
    					});
    					$("#endLoc").autocomplete({
    				        source: startLocs,
    				        multiple: true,
    				        mustMatch: false
    				        ,source: function (request, response) {
    				            // delegate back to autocomplete, but extract the last term
    				            response(customFilter(
    				            startLocs, request.term));
    				        },
    				    });
    					$("#endLoc").autocomplete("search");
    				}
    			});
    		}
    	});
    	$("#wayPoint").keyup(function(e) {
    		var code = e.keyCode || e.which;
    		if (code == 13) {
    			var address = "https://maps.googleapis.com/maps/api/geocode/json?address="
    				+ $("#wayPoint").val();
    			$.ajax({
    				dataType : "json",
    				url : address,
    				success : function(data) {
    					var wayPts = [];
    					$.each(data.results, function ( index, val ) {
    						wayPts.push({label:val.formatted_address});
    					});
    					$("#wayPoint").autocomplete({
    				        source: wayPts,
    				        multiple: true,
    				        mustMatch: false
    				        ,source: function (request, response) {
    				            // delegate back to autocomplete, but extract the last term
    				            response(customFilter(
    				            		wayPts, request.term));
    				        },
    				        select: function (event, ui) {
    				        	var wayPoint = $("#wayPoint").val();
    				    		var found = false;
    				        	$("#wayPoints > option").each(function() {
    				        	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
    				        	    	found = true;
    				        	    }  
    				        	});
    				        	if (!found) {
    				        		$("#wayPoints").append("<option value='" + ui.item.value  + "'>" + ui.item.value + "</option>");
    				        	}

    				        	$("form").find("#forwardCheckpoints").remove();
    				        	var forwardCheckpoints = "";
    				        	$("#wayPoints > option").each(function() {
    				        	    forwardCheckpoints += this.value + "~";
    				        	});
    				        	$("form").append("<input type='hidden' name='forwardCheckpoints' id='forwardCheckpoints' value='" + forwardCheckpoints + "'/>");
    				        }
    				    });
    					$("#wayPoint").autocomplete("search");
    				}
    			});
    		}
    	});
    });

	function addWayPoint() {
		if ($("#wayPoint").val() != "") {
			var wayPoint = $("#wayPoint").val();
			var found = false;
	    	$("#wayPoints > option").each(function() {
	    	    if (this.value.toLowerCase() == wayPoint.toLowerCase()) {
	    	    	found = true;
	    	    }  
	    	});
	    	if (!found) {
	    		$("#wayPoints").append("<option value='" + $("#wayPoint").val()  + "'>" + $("#wayPoint").val() + "</option>");
	    	}

	    	$("form").find("#forwardCheckpoints").remove();
	    	var forwardCheckpoints = "";
	    	$("#wayPoints > option").each(function() {
	    	    forwardCheckpoints += this.value + "~";
	    	});
	    	$("form").append("<input type='hidden' name='forwardCheckpoints' id='forwardCheckpoints' value='" + forwardCheckpoints + "'/>");
		}
	}

	function clearAdvanced() {
		$("#wayPoints").empty();
		$("#wayPoint").val("");
		$("form").find("#forwardCheckpoints").remove();
	}

	function clearBasic() {
		$("#startLoc").val("");
		$("#endLoc").val("");
		$("#startTime").val("");
		$("#returnTime").val("");
	}
	
	function searchRides() {
		if ($("#startLoc").val() == "") {
			alert("Please enter starting location.");
			return;
		}
		if ($("#endLoc").val() == "") {
			alert("Please enter destination.");
			return;
		}
		$("form").submit();
		
	}
	
	function requestRide(rideId) {
		$("form").attr("action", "rideRequest");
		$("form").append("<input type='hidden' name='rideId' value='" + rideId + "'/>");
		$("form").submit();
	}
    </script>
  </head>
  <body>

    <!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->

    <!-- Recent Benefits Feedback Section Start Here -->
    <section class="search_results_main">
        <div class="container">
        <div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>
        <div id="#page-content-wrapper" class="col-sm-8 col-md-10">
          <div class="bs-example">
          <div class="ItemHeader">
                        <span class="ProfileHeader">
                            <span id="header_label">Find a ride</span></span>
                    </div>
     <form role="form" class="form-horizontal" method="post" action="searchRides">
     <input type="hidden" name="forwardCheckpoints" id="forwardCheckpoints" value="${wayPointsString}"/>
     <div class="row">
             <div id="" class="form-section-wrapper col-xs-12 col-sm-12 col-md-12">
             <div id="" class="form_section_title">Search <span style="color:#FF6600;text-transform:none;font-style:italic">(After entering address press enter key in text boxes to get matches)</span></div>
             <div class="row">
             <div class="col-md-6">
         <div class="form-group">
        <label class="col-md-3 control-label" for="">Start<sup class="required">*</sup></label>
        <div class="col-md-9 input-group">    
    <input type="text" required="required" class="form-control" name="startLoc" id="startLoc" placeholder="" value="${origin}">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker start-icon"></i></span>
        </div>
        </div>
              <div class="form-group">
       <label class="col-md-3 control-label" for="">Start Time</label>
              <div class="col-md-5 input-group date timepicker">
                    <input class="form-control" id="startTime" name="startTime" value="${startTime}"/>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
         <div class="form-group required">
        <label class="col-md-3 control-label" for="">Destination<sup class="required">*</sup></label>
        <div class="col-md-9 input-group">    
    <input type="text" required="required" class="form-control" name="endLoc" id="endLoc" placeholder="" value="${destination}">
    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker dest-icon"></i></span>
        </div>
        </div>
           <div class="form-group">
       <label class="col-md-3 control-label" for="">Return Time</label>
              <div class="col-md-5 input-group date timepicker">
                    <input class="form-control" id="returnTime" name="returnTime" value="${returnTime}"/>
                </div>
            </div>
                  </div>
        </div>
            <div class="row">
            <div class="col-md-12 search-btns">
              <input type="button" value="Search" onclick="javascript:searchRides()">
                <input type="button" class="button" value="Clear" onclick="javascript:clearBasic()">
            </div>
            </div>
          
    

       


            </div>
<hr/>
 <div id="" class="form-section-wrapper col-xs-12 col-sm-12 col-md-12">
<div class="adv-srh">
<a data-toggle="collapse" data-parent="#accordion" href="#collapsesection"><i class="glyphicon glyphicon-plus-sign"></i>Advanced Search</a>
</div>
<div id="collapsesection" class="panel-collapse
<c:if test="${empty wayPoints}"> 
collapse 
</c:if>
row">
<div class="col-md-6">
    <div class="form-group">
       <label class="col-sm-4 control-label" for="">Checkpoints</label>
        <div class="col-sm-6 input-group">    
      <input type="text" required="required" class="form-control" name="wayPoint" id="wayPoint" placeholder="">
    <span class="input-group-addon" onclick="javascript:addWayPoint()"><i class="glyphicon"><img src="images/plus-add.png" height="15px"/></i></span>
        </div>
       </div>
    <div class="form-group">
       <label class="col-sm-4 control-label" for=""></label>
        <div class="col-sm-6 input-group">    
     <select class="form-control" size="5" id="wayPoints" name="wayPoints" multiple>
<c:forEach items="${wayPoints}" var="wayPoint">
    <option value="${wayPoint}">${wayPoint}</option>
  </tr>
</c:forEach>
      </select>
        </div>
       </div>
 </div>
<div class="col-md-12 search-btns">
              <input type="button" value="Search" onclick="javascript:searchRides()">
                <input type="button" value="Clear" class="button" onclick="javascript:clearAdvanced()">
            </div>
<hr class="adv-srch-end">
</div>
   <div id="" class="form_section_title">Search Results</div>
   <div class="table-responsive" style="height:300px;overflow-y:auto">
  <table class="table table-striped results_grid" id="searchResults">
  <thead>
        <tr>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Start Time</th>
          <th>Return Time</th>
          <th>From</th>
          <th>To</th>
          <th class="center-text">Boarding Points</th>
          <th>User</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
<c:forEach items="${rides}" var="ride"> 
  <tr>
  	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${ride['startdate']}"/></td>
  	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${ride['enddate']}"/></td>
  	<td><fmt:formatDate pattern="hh:mm a" value="${ride['starttime']}"/></td>
  	<td><fmt:formatDate pattern="hh:mm a" value="${ride['returntime']}"/></td>
  	<td>${ride.origin}</td>
  	<td>${ride.destination}</td>
  	<td><a class="show-pop directions ride${ride.id}" href="#" data-content="" rel="popover" data-placement="bottom" data-original-title="Boarding Points" data-trigger="hover"><div class="diff-dir"></div></a>
  	<script>
  		settings.content = "<c:if test='${empty ride.forwardcheckpoints and empty ride.backwardcheckpoints}'>No checkpoints to display.</c:if><c:forEach var='waypoint' items='${fn:split(ride.forwardcheckpoints, \'~\')}'><c:out value='${waypoint}' /><br/></c:forEach><c:forEach var='waypoint' items='${fn:split(ride.backwardcheckpoints, \'~\')}'><c:out value='${waypoint}' /><br/></c:forEach>";
  		$('a.ride${ride.id}').webuiPopover('destroy').webuiPopover(settings);
  	</script>
  	</td>
  	<td><a href="userProfile.jsp?profileId=${ride.user.email}">${ride.user.email}</a></td>
  	<td><button type="button" class="btn btn-primary btn-xs" onclick="javascript:requestRide(${ride.id})">Send Request</button></td>
  </tr>
</c:forEach>
      </tbody>
  </table>
</div>



</div>
</div>
</form>



 </div>
  
</div>



</div>

  </div>
 
    </section>
    <!-- Recent Benefits Feedback Section End Here -->
    
    <!-- Footer Start Here -->
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enter email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
  </body>
</html>