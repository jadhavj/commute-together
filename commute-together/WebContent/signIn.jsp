<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to Commute together</title>
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
	<!-- Header End Here -->

	<!-- Recent Benefits Feedback Section Start Here -->
	<section class="ride_details_main">
		<div class="container">
			<div class="col-md-12">
				<div class="bs-example">
					<div class="ItemHeader">
						<span class="ProfileHeader"> <span id="header_label">Create
								Ride</span></span>
					</div>
					<form role="form" class="form-horizontal">
						<div class="form-section-wrapper">
							<div class="btn-group details-types" data-toggle="buttons">
								<a class="btn btn-primary active" href="#oneway"> <input
									type="radio" name="options" id="option1" checked>One
									Way
								</a> <a class="btn btn-primary" href="#twoways"> <input
									type="radio" name="options" id="option2" href="#twoways">Two
									Ways
								</a> <a class="btn btn-primary" href="#multipleways"> <input
									type="radio" name="options" id="option3" href="#multipleways">Multiple
									Ways
								</a>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-4 row">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Ride Type</label>
									<div class="col-sm-8">
										<select >
											<option>Weekday Ride (MON-FRI)</option>
											<option>Weekend Ride (SAT-SUN)</option>
											<option>One Time Ride</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-8 row">
								<div class="form-group col-md-6">
									<label class="col-xs-12 col-sm-4 control-label nopadding"
										for="">Start Date<sup class="required">*</sup></label>
									<div class="col-sm-4 col-md-8 input-group date datepicker">
										<input type="date" class="form-control"><span
											class="input-group-addon"><i
											class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</div>
								<div class="form-group col-md-6">
									<label class="col-xs-12 col-sm-4 control-label nopadding"
										for="">End Date</label>
									<div class="col-sm-4 col-md-8 input-group date datepicker">
										<input type="date" class="form-control"><span
											class="input-group-addon"><i
											class="glyphicon glyphicon-calendar"></i></span>
									</div>
								</div>
							</div>
						</div>
						<hr />

						<div id="oneway"
							class="form-section-wrapper col-xs-12 col-sm-12 col-md-12 row tab-content">
							<div id="" name="" class="form_section_title">Ride Details</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Start<sup
									class="required">*</sup></label>
								<div class="col-sm-6 input-group">
									<input type="text" required="required" class="form-control"
										name="location" id="" placeholder=""> <span
										class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker start-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Destination<sup
									class="required">*</sup></label>
								<div class="col-sm-6 input-group">
									<input type="text" required="required" class="form-control"
										name="" placeholder=""> <span
										class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker dest-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Route<sup
									class="required">*</sup></label>
								<div class="col-sm-6 input-group route-selection">
									<select class="selectpicker">

									</select> <span class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker route-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Checkpoints</label>
								<div class="col-sm-6">
									<select class="selectpicker"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Start Time<sup
									class="required">*</sup></label>
								<div class="col-sm-3 col-md-2 input-group date timepicker">
									<input type='time' class="form-control" /> <span
										class="input-group-addon"><span
										class="glyphicon glyphicon-time"></span> </span>
								</div>
							</div>
						</div>
						<div id="twoways"
							class="form-section-wrapper col-xs-12 col-sm-12 col-md-12 row tab-content">
							<div id="" class="form_section_title">Ride Details</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Start<sup
									class="required">*</sup></label>
								<div class="col-sm-6 input-group">
									<input type="text" required="required" class="form-control"
										name="location" id="" placeholder=""> <span
										class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker start-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Destination<sup
									class="required">*</sup></label>
								<div class="col-sm-6 input-group">
									<input type="text" required="required" class="form-control"
										name="dest-location" placeholder=""> <span
										class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker dest-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Forward
									Route<sup class="required">*</sup>
								</label>
								<div class="col-sm-6 input-group route-selection">
									<select class="selectpicker">

									</select> <span class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker route-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Checkpoints</label>
								<div class="col-sm-6">
									<select class="selectpicker"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Backward
									Route<sup class="required">*</sup>
								</label>
								<div class="col-sm-6 input-group route-selection">
									<select class="selectpicker">

									</select> <span class="input-group-addon"><i
										class="glyphicon glyphicon-map-marker route-icon"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Checkpoints</label>
								<div class="col-sm-6">
									<select class="selectpicker"></select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Start Time<sup
									class="required">*</sup></label>
								<div class="col-sm-3 col-md-2 input-group date timepicker">
									<input type='time' class="form-control" /> <span
										class="input-group-addon"><span
										class="glyphicon glyphicon-time"></span> </span>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="">Return Time<sup
									class="required">*</sup></label>
								<div class="col-sm-3 col-md-2 input-group date timepicker">
									<input type='time' class="form-control" /> <span
										class="input-group-addon"><span
										class="glyphicon glyphicon-time"></span> </span>
								</div>
							</div>
						</div>
						<div id="multipleways" class="form-section-wrapper tab-content">
							<div id="entry1"
								class="col-xs-12 col-sm-12 col-md-12 row clonedInput">
								<div id="reference" name="reference" class="form_section_title">Ride
									Details #1</div>
								<div class="form-group">
									<label class="col-sm-4 control-label label_dest"
										for="strtplace">Start<sup class="required"></sup>*
									</label>

									<div class="col-sm-6 input-group">
										<input type="text" required="required"
											class="form-control start_loc" name="location" id="location"
											placeholder=""> <span class="input-group-addon"><i
											class="glyphicon glyphicon-map-marker start-icon"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label label_dest"
										for="destplace">Destination <sup class="required">*</sup></label>
									<div class="col-sm-6 input-group">
										<input type="text" required="required"
											class="form-control dest_loc" name="dest-location"
											placeholder=""> <span class="input-group-addon"><i
											class="glyphicon glyphicon-map-marker dest-icon"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Route<sup
										class="required">*</sup></label>
									<div class="col-sm-6 input-group route-selection">
										<select class="selectpicker">

										</select> <span class="input-group-addon"><i
											class="glyphicon glyphicon-map-marker route-icon"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Checkpoints</label>
									<div class="col-sm-6">
										<select class="selectpicker"></select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Start Time<sup
										class="required">*</sup></label>
									<div class="col-sm-3 col-md-2 input-group date timepicker">
										<input type='time' class="form-control" /> <span
											class="input-group-addon"><span
											class="glyphicon glyphicon-time"></span> </span>
									</div>
								</div>
							</div>

							<div class="clearfix"></div>
							<div id="addDelButtons" class="form-group">
								<label class="col-sm-4 control-label" for=""></label>
								<div class="col-sm-8">
									<button type="button" id="btnAdd" name="btnAdd"
										class="btn btn-info">add route</button>
									<button type="button" id="btnDel" name="btnDel"
										class="btn btn-danger">remove above route</button>
								</div>
							</div>

						</div>



						<hr />
						<div class="form-section-wrapper">
							<div class="col-xs-12 col-sm-12 col-md-6 row">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Vehicle
										Type</label>
									<div class="col-sm-8 veh-type">
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-primary active"> <input
												type="radio" name="options" id="option1" checked>4
												Wheeler
											</label> <label class="btn btn-primary"> <input type="radio"
												name="options" id="option2">2 Wheeler
											</label>
										</div>
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-primary active"> <input
												type="radio" name="options" id="option1" checked>AC
											</label> <label class="btn btn-primary"> <input type="radio"
												name="options" id="option2">Non AC
											</label>
										</div>
										<div class="btn-group" data-toggle="buttons">
											<label class="btn btn-primary active"> <input
												type="radio" name="options" id="option1" checked>Hatch
												Back
											</label> <label class="btn btn-primary"> <input type="radio"
												name="options" id="option2">Sedan
											</label> <label class="btn btn-primary"> <input type="radio"
												name="options" id="option3">Suv
											</label>
										</div>
									</div>

								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 row">
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Vehicle
										No.<sup class="required">*</sup>
									</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" placeholder=""
											required="required" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Vehicle
										Color</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" placeholder="" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">License
										No.<sup class="required">*</sup>
									</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" placeholder=""
											required="required" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Vehicle
										Model</label>
									<div class="col-sm-8">
										<input type="text" class="form-control" placeholder="" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label" for="">Vehicle
										Image</label>
									<div class="col-sm-8 file-wrapper">
										<input type="file" name="photo" id="veh_photo" /> <span
											class="button upload_pic_btn">Upload Photo</span>
									</div>
								</div>
							</div>
						</div>
						<div class="req-section">
							<p>
								<sup class="required">*</sup>Required Fields
							</p>
						</div>

						<div class="page_submit">
							<input type="button" value="Submit">
						</div>

					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- Recent Benefits Feedback Section End Here -->

	<!-- Footer Start Here -->
	<footer>
		<div class="container">
			<div class="col-md-4 about_us">
				<h3>About Carpooling</h3>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum
					fermentum placerat convallis. Quisque pharetra cursus enim et
					fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis
					sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean
					at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam
					porttitor erat id libero dapibus, at dictum nulla tempus. <a
						href="Javascript:void(0)">More >></a>
				</p>
			</div>
			<div class="col-md-3">
				<div class="footer_links">
					<ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-5">
				<div class="follow_us">
					<div class="clearfix">
						<h3>Follow Us:</h3>
						<div class="social_icons">
							<ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="news_letter_main">
						<h3>Sign Up for Newsletter</h3>
						<input type="text" placeholder="Enrer email address"
							name="newsletter"><input type="button" value="Sign Up">
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer End Here -->
	<link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css"
		media="screen" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/bootstrap-select.min.css" rel="stylesheet">
	<link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="js/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/jquery-ui.js"></script>
	<script type="text/javascript" src="js/clone-form-td.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/bootstrap-select.min.js"></script>
	<script src="js/moment.js"></script>
	<script src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/jquery.ui.touch.js"></script>
	<script type="text/javascript" src="js/QapTcha.jquery.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>