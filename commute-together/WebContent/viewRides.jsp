<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="url">${req.requestURL}</c:set>
<c:set var="uri" value="${req.requestURI}" />
<!DOCTYPE html>
<c:if test="${param['rideUpdated'] eq 'true'}">
<script>
alert('Ride successfully updated.');
</script>
</c:if>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome to Commute together</title>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery-ui.js"></script>
  	<script type="text/javascript" src="js/jquery.timepicker.js"></script>
  	<link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />
	<link rel="stylesheet" href="http://jqueryui.com/resources/demos/style.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/QapTcha.jquery.css" media="screen" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/clone-form-td.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/bootstrap-select.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch.js"></script>
    <script type="text/javascript" src="js/QapTcha.jquery.js"></script>
    <script src="js/custom.js"></script>
	<script src="js/jquery.webui-popover.min.js"></script>	
	<link rel="stylesheet" href="css/jquery.webui-popover.min.css">
    <script>
	var settings = {
			trigger:'hover',
			title:'Boarding Points',
			content:'<p>This is webui popover demo.</p><p>just enjoy it and have fun !</p>',
			width:300,						
			multi:true,						
			closeable:false,
			style:'',
			delay:300,
			padding:true
	};
	
	function leaveRide(requestId) {
		if (confirm("Sure you want to leave the ride?")) {
			$("form").attr("action", "leaveRide");
			$("form").append("<input type='hidden' name='requestId' value='" + requestId + "'/>");
			$("form").submit();
		}
	}

	function deleteRide(rideId) {
		if (confirm("Sure you want to delete the ride?")) {
			$("form").attr("action", "deleteRide");
			$("form").append("<input type='hidden' name='rideId' value='" + rideId + "'/>");
			$("form").submit();
		}
	}

	function editRide(rideId) {
		$("form").attr("action", "rideDetails");
		$("form").append("<input type='hidden' name='rideId' value='" + rideId + "'/>");
		$("form").submit();
	}
	
	function rejectRequest(id) {
		$("form").attr("action", "updateRequest?id=" + id + "&action=reject&redirect=${requestScope['javax.servlet.forward.request_uri']}");
		$("form").submit();
	}
    </script>
  </head>
  <body>

 <!-- Header Start Here -->
    <jsp:include page="header.jsp"/>
    <!-- Header End Here -->


    <!-- Breadcum start Here -->
    <div class="bread-crumb">
    <div class="container">
    <ol class="breadcrumb">
          <li><a href="dashboard.jsp">Home</a></li>
      <li class="active">View Rides</li>
    </ol>
    </div>
    </div>
<!-- Breadcum end Here -->

    <!-- Recent Benefits Feedback Section Start Here -->
    <section class="search_results_main">
        <div class="container">
<div class="row">
        <!-- left sidebar start Here -->
 <div class="left-side">
  <div class="col-sm-4 col-md-2 left-sidebar">
      <nav class="navbar navbar-default" role="navigation">
  <div>
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav nav-pills navbar-nav nav-stacked  custom-nav ">
                <li class=""><a href="dashboard.jsp"><i class="fa fa-home"></i> <span>Dashboard</span>
                </a></li>
                <li class=""><a href="myProfile.jsp"><i class="fa fa-user"></i> <span>Profile</span></a>
                  
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-cab"></i> <span>Rides</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="viewRides"> View Rides</a></li>
                        <li><a href="rideDetails.jsp"> Create Rides</a></li>
                        <li><a href="searchPage.jsp"> Request A Ride</a></li>     
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Messages</span></a>
                    <ul class="sub-menu-list" style="">
                        <li><a href="newMessage"> New Message</a></li>
                        <li><a href="showMessages"> Inbox</a></li>
                        <li><a href="showOutbox"> Outbox</a></li>
                    </ul>
                </li>
                <li class=""><a href="accountSettings.jsp"><i class="fa fa-cog"></i> <span>Settings</span></a></li>   
            </ul>
            </div>
            </div>
        </nav>

  </div>  
 </div>

  <div id="#page-content-wrapper" class="col-sm-8 col-md-10">
          <div class="bs-example">
          <!-- start header title -->
           <div class="ItemHeader">
            <span class="ProfileHeader">
             <span id="header_label">View Rides</span></span>
                </div>
                <!-- end header title -->
     <form role="form" class="form-horizontal acnt_stngs" method="post" action="">
     <div class="form-section-wrapper">
       <div class="form_section_title">Created Rides</div>
          <div class="table-responsive" style="height:300px;overflow-x:auto">
  <table class="table table-small-font table-striped rides_grid">
  <thead>
        <tr>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Start Time</th>
          <th>Return Time</th>
          <th>From</th>
          <th>To</th>
          <th class="center-text">Check Points</th>
          <th>Way Type</th>
          <th>Ride Type</th>
          <th class="center-text">No. of Users</th>
          <th>Vehicle No.</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
<c:forEach items="${created}" var="ride"> 
  <tr>
  	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${ride['startdate']}"/></td>
  	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${ride['enddate']}"/></td>
  	<td><fmt:formatDate pattern="hh:mm a" value="${ride['starttime']}"/></td>
  	<td><fmt:formatDate pattern="hh:mm a" value="${ride['returntime']}"/></td>
  	<td>${ride.origin}</td>
  	<td>${ride.destination}</td>
  	<td><a class="show-pop directions ride${ride.id}" href="#" data-content="" rel="popover" data-placement="bottom" data-original-title="Boarding Points" data-trigger="hover"><div class="diff-dir"></div></a>
  	<script>
  		settings.content = "<c:if test='${empty ride.forwardcheckpoints and empty ride.backwardcheckpoints}'>No checkpoints to display.</c:if><c:forEach var='waypoint' items='${fn:split(ride.forwardcheckpoints, \'~\')}'><c:out value='${waypoint}' /><br/></c:forEach><c:forEach var='waypoint' items='${fn:split(ride.backwardcheckpoints, \'~\')}'><c:out value='${waypoint}' /><br/></c:forEach>";
  		$('a.ride${ride.id}').webuiPopover('destroy').webuiPopover(settings);
  	</script>
  	</td>
<td>
<c:choose>
  <c:when test="${ride.whichway == 1}">
    One-Way
  </c:when>
  <c:when test="${ride.whichway == 2}">
    Two-Way
  </c:when>
  <c:otherwise>
  	Multiple-Way
  </c:otherwise>
</c:choose>
</td>  	
<td>
<c:choose>
  <c:when test="${ride.ridetype == 0}">
    Weekday
  </c:when>
  <c:when test="${ride.ridetype == 1}">
    Weekend
  </c:when>
  <c:otherwise>
  	One-Time
  </c:otherwise>
</c:choose>
</td>  	
<td>
  <div class="dropdown userlistdrop directions">
<c:set var="users" scope="session" value="0"/>
<c:forEach items="${ride.riderequestses}" var="rideRequest">
<c:if test="${rideRequest.status == 1}"> 
<c:set var="users" scope="session" value="${users + 1}"/>
  </c:if>
  </c:forEach>
  <a  id="UsersLabel" role="button" data-toggle="dropdown" data-target="#" href="#">
     ${users} <span class="caret"></span>

  </a>
  <ul id="userlist" class="dropdown-menu" role="menu" aria-labelledby="UsersLabel">
<c:forEach items="${ride.riderequestses}" var="rideRequest">
<c:if test="${rideRequest.status == 1}"> 
  <li role="presentation">
  <div><div style="float:left;width:80%"><a href="userProfile.jsp?profileId=${rideRequest.user.email}">&nbsp;&nbsp;&nbsp;${rideRequest.user.fname} ${rideRequest.user.lname}</a></div><div><a><span class="glyphicon glyphicon-remove" onclick="javascript:rejectRequest(${rideRequest.id})"></span></a></div></div>
  </li>
  </c:if>
  </c:forEach>
  </ul>

          </td>
          <td>${ride.vehicleno}</td>
          <td class="butn_actions">
          <span title="edit" class="glyphicon glyphicon-pencil" onclick="javascript:editRide(${ride.id})"></span>
          <span title="delete" class="glyphicon glyphicon-remove" onclick="javascript:deleteRide(${ride.id})"></span>
          </td>
        </tr>
</c:forEach>
      </tbody>
  </table>
</div>


     </div>
     <hr>
     <div class="form-section-wrapper">
       <div class="form_section_title">Joined Rides</div>
          <div class="table-responsive" style="height:300px;overflow-x:auto">
  <table class="table table-small-font table-striped results_grid">
  <thead>
        <tr>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Start Time</th>
          <th>Return Time</th>
          <th>From</th>
          <th>To</th>
          <th class="center-text">Boarding Points</th>
          <th>Vehicle No.</th>
          <th>User</th>
          <th>Status</th>
          <th>Manage</th>
        </tr>
      </thead>
      <tbody>
<c:forEach items="${joined}" var="ride"> 
        <tr>
  	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${ride.createdrides['startdate']}"/></td>
  	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${ride.createdrides['enddate']}"/></td>
  	<td><fmt:formatDate pattern="hh:mm a" value="${ride.createdrides['starttime']}"/></td>
  	<td><fmt:formatDate pattern="hh:mm a" value="${ride.createdrides['returntime']}"/></td>
  	<td>${ride.createdrides.origin}</td>
  	<td>${ride.createdrides.destination}</td>
  	<td><a class="show-pop directions ride${ride.id}" href="#" data-content="" rel="popover" data-placement="bottom" data-original-title="Boarding Points" data-trigger="hover"><div class="diff-dir"></div></a>
  	<script>
  		settings.content = "<c:if test='${empty ride.createdrides.forwardcheckpoints and empty ride.createdrides.backwardcheckpoints}'>No checkpoints to display.</c:if><c:forEach var='waypoint' items='${fn:split(ride.createdrides.forwardcheckpoints, \'~\')}'><c:out value='${waypoint}' /><br/></c:forEach><c:forEach var='waypoint' items='${fn:split(ride.createdrides.backwardcheckpoints, \'~\')}'><c:out value='${waypoint}' /><br/></c:forEach>";
  		$('a.ride${ride.id}').webuiPopover('destroy').webuiPopover(settings);
  	</script>
  	</td>
          <td>${ride.createdrides.vehicleno}</td>
          <td><a href="userProfile.jsp?profileId=${ride.createdrides.user.email}">${ride.createdrides.user.email}</a></td>
          <td>
<c:choose>
  <c:when test="${ride.status == 0}">
    Unapproved
  </c:when>
  <c:when test="${ride.status == 1}">
    Approved
  </c:when>
  <c:otherwise>
  	Rejected	
  </c:otherwise>
</c:choose>
          </td>
          <td class="butn_actions"><span title="Leave ride" class="glyphicon glyphicon-remove" onclick="javascript:leaveRide(${ride.id})"></span></td>
        </tr>
        </c:forEach>
      </tbody>
  </table>
</div>
       </div>
   </form>
 </div>
</div>
</div>
  </div>
 
    </section>
    <!-- Recent Benefits Feedback Section End Here -->
    
    <!-- Footer Start Here -->
    <footer>
        <div class="container">
        <div class="row">
            <div class="col-md-4 about_us">
                <h3>About Carpooling</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum fermentum placerat convallis. Quisque pharetra cursus enim et fermentum. Etiam at erat enim. Morbi ipsum nunc, vulputate quis sollicitudin sit amet, pretium a odio. Sed a accumsan nibh. Aenean at quam faucibus lacus ullamcorper dapibus sit amet at neque. Etiam porttitor erat id libero dapibus, at dictum nulla tempus.  <a href="Javascript:void(0)">More >></a></p>
            </div>
            <div class="col-md-3">
                <div class="footer_links">
                    <ul>
							<li><a href="aboutus.jsp">About Us</a></li>
							<li><a href="faqs.jsp">FAQs</a></li>
							<li><a href="privacy.jsp">Privacy Policy</a></li>
							<li><a href="contactus.jsp">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5">
                <div class="follow_us">
                    <div class="clearfix">
                        <h3>Follow Us:</h3>
                        <div class="social_icons">
                            <ul>
                                <li><a href="https://www.facebook.com/pages/Commute-Together/637377536390247" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                <li><a href="Javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>
                                <li><a href="https://plus.google.com/b/113325515553599389049/113325515553599389049/" target="_blank"><i class="fa fa-google-plus-square"></i></a></li>
                                <li><a href="https://www.linkedin.com/company/541305" target="_blank"><i class="fa fa-linkedin-square"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="news_letter_main">
                        <h3>Sign Up for Newsletter</h3>
                        <input type="text" placeholder="Enter email address" name="newsletter" ><input type="button" value="Sign Up">
                    </div>
                </div>
            </div>
            </div>
        </div>
    </footer>  
    <!-- Footer End Here -->
  </body>
</html>