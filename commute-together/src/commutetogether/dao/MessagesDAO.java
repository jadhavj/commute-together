package commutetogether.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Transaction;

import commutetogether.data.Createdrides;
import commutetogether.data.Database;
import commutetogether.data.Messages;
import commutetogether.data.Riderequests;
import commutetogether.data.User;
import commutetogether.models.Message;

public class MessagesDAO {

    public static List<User> getUsers(String email) {
	Query q = Database.getInstance().getSession().createQuery("from User where email != '" + email + "'");
	List<User> users = q.list();
	return users;
    }

    public static List<Message> getMessagesFor(String toUser) {
	Query q = Database.getInstance().getSession().createQuery("from Messages where (touser = :touser or (fromuser != :fromuser and rideid in (:rideids))) order by createdtime desc");
	q.setString("touser", toUser);
	q.setString("fromuser", toUser);
	List<Integer> rideIds = new ArrayList<Integer>();
	List<Createdrides> rides = RidesDAO.getRidesCreatedByAndJoinedBy(toUser);
	if (rides.isEmpty()) {
	    rideIds.add(-1);
	}
	for (Createdrides ride: rides) {
	    rideIds.add(ride.getId());
	}
	List<Riderequests> requests = RidesDAO.getRidesJoinedBy(toUser);
	for (Riderequests request: requests) {
	    rideIds.add(request.getCreatedrides().getId());
	}
	q.setParameterList("rideids", rideIds);
	List<Messages> messages = q.list();
	List<Message> results = new ArrayList<Message>();
	for (Messages message : messages) {
	    results.add(new Message(message));
	}
	return results;
    }
    public static List<Message> getMessagesFor(String toUser, int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Messages where touser = :touser and isread = false or (fromuser != :fromuser and rideid in (:rideids)) and isread=0 order by createdtime desc");
	q.setString("touser", toUser);
	q.setString("fromuser", toUser);
	List<Integer> rideIds = new ArrayList<Integer>();
	List<Createdrides> rides = RidesDAO.getRidesCreatedByAndJoinedBy(toUser);
	if (rides.isEmpty()) {
	    rideIds.add(-1);
	}
	for (Createdrides ride: rides) {
	    rideIds.add(ride.getId());
	}
	List<Riderequests> requests = RidesDAO.getRidesJoinedBy(toUser);
	for (Riderequests request: requests) {
	    rideIds.add(request.getCreatedrides().getId());
	}
	q.setParameterList("rideids", rideIds);
	q.setMaxResults(limit);
	List<Messages> messages = q.list();
	List<Message> results = new ArrayList<Message>();
	for (Messages message : messages) {
	    results.add(new Message(message));
	}
	return results;
    }
    public static List<Message> getOutboxMessagesFor(String fromUser) {
	Query q = Database.getInstance().getSession().createQuery("from Messages where fromuser = :fromuser and showinoutbox = 1 and rideid is null order by createdtime desc");
	q.setString("fromuser", fromUser);
	List<Messages> messages = q.list();
	List<Message> results = new ArrayList<Message>();
	for (Messages message : messages) {
	    results.add(new Message(message));
	}
	return results;
    }
    
    public static void deleteMessages(String []ids) {
	Integer []messageIds = new Integer[ids.length];
	int i = 0;
	for (String id : ids) {
	    messageIds[i++] = Integer.parseInt(id);
	}
	Transaction t = Database.getInstance().getSession().beginTransaction();
	Query q = Database.getInstance().getSession().createQuery("delete from Messages where id in (:ids)");
	q.setParameterList("ids", Arrays.asList(messageIds));
	q.executeUpdate();
	t.commit();
    }
    public static void deleteOutboxMessages(String []ids) {
	Integer []messageIds = new Integer[ids.length];
	int i = 0;
	for (String id : ids) {
	    messageIds[i++] = Integer.parseInt(id);
	}
	Transaction t = Database.getInstance().getSession().beginTransaction();
	Query q = Database.getInstance().getSession().createQuery("update Messages set showinoutbox = 0 where id in (:ids)");
	q.setParameterList("ids", Arrays.asList(messageIds));
	q.executeUpdate();
	t.commit();
    }
    
    public static Messages findMessageById(String id) {
	Query q = Database.getInstance().getSession().createQuery("from Messages where id = :messageId");
	q.setInteger("messageId", Integer.parseInt(id));
	List<Messages> messages = q.list();
	if (messages != null && !messages.isEmpty()) {
	    return messages.get(0);
	}
	return null;
    }
    
    public static List<Messages> findRelatedMessagesFor(String toUser, String id) {
	Messages original = findMessageById(id);
	Query q = Database.getInstance().getSession().createQuery("from Messages where touser = :touser and id != :original and linkedid = :linkedid order by createdtime asc");
	q.setString("touser", toUser);
	q.setInteger("original", Integer.parseInt(id));
	q.setString("linkedid", original.getLinkedid());
	List<Messages> messages = q.list();
	return messages;
    }

    public static List<Message> findLatestMessages(String toUser, int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Messages where touser = :touser and isread = 0 order by createdtime desc");
	q.setString("touser", toUser);
	q.setMaxResults(limit);
	List<Messages> messages = q.list();
	List<Message> results = new ArrayList<Message>();
	for (Messages message : messages) {
	    results.add(new Message(message));
	}
	return results;
    }

    public static List<Message> getAllMessagesForRide(int rideId) {
	Query q = Database.getInstance().getSession().createQuery("from Messages where rideid = :rideid order by createdtime asc");
	q.setInteger("rideid", rideId);
	List<Messages> messages = q.list();
	List<Message> results = new ArrayList<Message>();
	for (Messages message : messages) {
	    results.add(new Message(message));
	}
	return results;
    }
}
