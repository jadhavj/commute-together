package commutetogether.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import commutetogether.data.Createdrides;
import commutetogether.data.Database;
import commutetogether.data.Riderequests;
import commutetogether.data.Ridesearches;
import commutetogether.models.RideRequest;

public class RidesDAO {

    public static List<Createdrides> searchRides(String email, String origin, String destination, Date startTime, Date returnTime, String wayPoints) {
	String query = "from Createdrides where createdby != :createdby and whichway != 2 and origin like :origin and destination like :destination and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	if (startTime != null) {
	    query += " and starttime = :startTime ";
	}
	if (returnTime != null) {
	    query += " and returntime = :returnTime ";
	}
	if (wayPoints != null) {
	    query += " and (";
	    String[] wayPointsArr = wayPoints.split("~");
	    int i = 0;
	    for (String wayPointSingle : wayPointsArr) {
		if (i != 0) {
		    query += " or ";
		}
		query += " forwardcheckpoints like '%" + wayPointSingle + "%' ";
		query += " or backwardcheckpoints like '%" + wayPointSingle + "%' ";
		i++;
	    }
	    query += " ) ";
	}
	Query q = Database.getInstance().getSession().createQuery(query);
	q.setString("createdby", email);
	q.setString("origin", "%" + origin + "%");
	q.setString("destination", "%" + destination + "%");
	if (startTime != null) {
	    q.setTimestamp("startTime", startTime);
	}
	if (returnTime != null) {
	    q.setTimestamp("returnTime", returnTime);
	}
	List<Createdrides> rides = q.list();

	query = "from Createdrides where createdby != :createdby and whichway = 2 and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	if (startTime != null) {
	    query += " and starttime = :startTime ";
	}
	if (returnTime != null) {
	    query += " and returntime = :returnTime ";
	}
	if (wayPoints != null) {
	    query += " and (";
	    String[] wayPointsArr = wayPoints.split("~");
	    int i = 0;
	    for (String wayPointSingle : wayPointsArr) {
		if (i != 0) {
		    query += " or ";
		}
		query += " forwardcheckpoints like '%" + wayPointSingle + "%' ";
		query += " or backwardcheckpoints like '%" + wayPointSingle + "%' ";
		i++;
	    }
	    query += " ) ";
	}
	query += " and ((origin like '%" + origin + "%' and destination like '%" + destination + "%') or (destination like '%" + origin + "%' and origin like '%" + destination + "%')) ";
	q = Database.getInstance().getSession().createQuery(query);
	q.setString("createdby", email);
	if (startTime != null) {
	    q.setTimestamp("startTime", startTime);
	}
	if (returnTime != null) {
	    q.setTimestamp("returnTime", returnTime);
	}
	rides.addAll(q.list());

	if (wayPoints != null) {
	    query = "from Createdrides where createdby != :createdby and whichway != 2 and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	    if (startTime != null) {
		query += " and starttime = :startTime ";
	    }
	    if (returnTime != null) {
		query += " and returntime = :returnTime ";
	    }
	    if (wayPoints != null) {
		query += " and (";
		String[] wayPointsArr = wayPoints.split("~");
		int i = 0;
		for (String wayPointSingle : wayPointsArr) {
		    if (i != 0) {
			query += " or ";
		    }
		    query += " forwardcheckpoints like '%" + wayPointSingle + "%' ";
		    query += " or backwardcheckpoints like '%" + wayPointSingle + "%' ";
		    if (wayPointsArr.length > 1) {
			query += " or (origin like '%" + wayPointSingle + "%' and ( ";
			int j = 0;
			for (String wayPointDestination : wayPointsArr) {
			    if (wayPointDestination.equals(wayPointDestination)) {
				continue;
			    }
			    if (j != 0) {
				query += " or ";
			    }
			    query += " destination like '%" + wayPointDestination + "%' ";
			    j++;
			}
			query += " )) ";
		    } else {
			query += " or (origin like '%" + wayPointSingle + "%' and destination like '%" + destination + "%') ";
		    }
		    i++;
		}
		query += " ) ";
	    }
	    q = Database.getInstance().getSession().createQuery(query);
	    q.setString("createdby", email);
	    if (startTime != null) {
		q.setTimestamp("startTime", startTime);
	    }
	    if (returnTime != null) {
		q.setTimestamp("returnTime", returnTime);
	    }
	    rides.addAll(q.list());

	    query = "from Createdrides where createdby != :createdby and whichway = 2 and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	    if (startTime != null) {
		query += " and starttime = :startTime ";
	    }
	    if (returnTime != null) {
		query += " and returntime = :returnTime ";
	    }
	    if (wayPoints != null) {
		query += " and (";
		String[] wayPointsArr = wayPoints.split("~");
		int i = 0;
		for (String wayPointSingle : wayPointsArr) {
		    if (i != 0) {
			query += " or ";
		    }
		    query += " forwardcheckpoints like '%" + wayPointSingle + "%' ";
		    query += " or backwardcheckpoints like '%" + wayPointSingle + "%' ";
		    if (wayPointsArr.length > 1) {
			query += " or (origin like '%" + wayPointSingle + "%' and ( ";
			int j = 0;
			for (String wayPointDestination : wayPointsArr) {
			    if (wayPointDestination.equals(wayPointSingle)) {
				continue;
			    }
			    if (j != 0) {
				query += " or ";
			    }
			    query += " destination like '%" + wayPointDestination + "%' ";
			    j++;
			}
			query += " )) ";
		    } else {
			query += " or (origin like '%" + wayPointSingle + "%' and destination like '%" + destination + "%') ";
		    }
		    if (wayPointsArr.length > 1) {
			query += " or (destination like '%" + wayPointSingle + "%' and ( ";
			int j = 0;
			for (String wayPointOrigin : wayPointsArr) {
			    if (wayPointOrigin.equals(wayPointSingle)) {
				continue;
			    }
			    if (j != 0) {
				query += " or ";
			    }
			    query += " origin like '%" + wayPointOrigin + "%' ";
			    j++;
			}
			query += " )) ";
		    } else {
			query += " or (destination like '%" + wayPointSingle + "%' and origin like '%" + origin + "%') ";
		    }
		    i++;
		}
		query += " ) ";
	    }
	    q = Database.getInstance().getSession().createQuery(query);
	    q.setString("createdby", email);
	    if (startTime != null) {
		q.setTimestamp("startTime", startTime);
	    }
	    if (returnTime != null) {
		q.setTimestamp("returnTime", returnTime);
	    }
	    rides.addAll(q.list());
	}

	query = "from Createdrides where createdby != :createdby and whichway != 2 and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	if (startTime != null) {
	    query += " and starttime = :startTime ";
	}
	if (returnTime != null) {
	    query += " and returntime = :returnTime ";
	}
	query += " and (forwardcheckpoints like '%" + origin+ "%" + destination + "%' or (forwardcheckpoints like '%" + origin + "%' and destination like '%" + destination + "%') or (origin like '%" + origin + "%' and forwardcheckpoints like '%" + destination + "%'))";
	q = Database.getInstance().getSession().createQuery(query);
	q.setString("createdby", email);
	if (startTime != null) {
	    q.setTimestamp("startTime", startTime);
	}
	if (returnTime != null) {
	    q.setTimestamp("returnTime", returnTime);
	}
	rides.addAll(q.list());

	query = "from Createdrides where createdby != :createdby and whichway = 2 and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	if (startTime != null) {
	    query += " and starttime = :startTime ";
	}
	if (returnTime != null) {
	    query += " and returntime = :returnTime ";
	}
	query += " and (forwardcheckpoints like '%" + origin+ "%" + destination + "%' or backwardcheckpoints like '%" + origin+ "%" + destination + "%' or (forwardcheckpoints like '%" + origin + "%' and destination like '%" + destination + "%') or (backwardcheckpoints like '%" + origin + "%' and origin like '%" + destination + "%') or (origin like '%" + origin + "%' and forwardcheckpoints like '%" + destination + "%')) ";
	q = Database.getInstance().getSession().createQuery(query);
	q.setString("createdby", email);
	if (startTime != null) {
	    q.setTimestamp("startTime", startTime);
	}
	if (returnTime != null) {
	    q.setTimestamp("returnTime", returnTime);
	}
	rides.addAll(q.list());
	return rides;
    }

    public static List<Createdrides> searchRides(String email, String city, String origin, String destination) {
	String query = "from Createdrides where createdby != :fromUser and origin like :origin and destination like :destination and origin like :city1 and destination like :city2 and id not in (select r.createdrides.id from Riderequests r where r.status != 2)";
	Query q = Database.getInstance().getSession().createQuery(query);
	q.setString("fromUser", "%" + email + "%");
	q.setString("origin", "%" + city + "%");
	q.setString("destination", "%" + destination + "%");
	q.setString("city1", "%" + city + "%");
	q.setString("city2", "%" + city + "%");
	List<Createdrides> rides = q.list();
	return rides;
    }

    public static Createdrides getRide(int rideId) {
	List<Createdrides> results = (List<Createdrides>) Database.getInstance().getSession().createCriteria(Createdrides.class).add(Restrictions.eq("id", rideId)).list();
	if (results.size() > 1) {
	    throw new IllegalStateException("Not just one found.");
	}
	if (results == null || results.size() == 0) {
	    return null;
	}
	return results.get(0);
    }

    public static void requestRide(int rideId, String email) {
	List<Riderequests> results = (List<Riderequests>) Database.getInstance().getSession().createCriteria(Riderequests.class).add(Restrictions.eq("user.email", email)).add(Restrictions.eq("createdrides.id", rideId)).add(Restrictions.ne("status", 2)).list();
	if (!results.isEmpty()) {
	    return;
	}
	Transaction tx = Database.getInstance().getSession().beginTransaction();
	Query q = Database.getInstance().getSession().createQuery("delete from Riderequests where user.email = :email and createdrides.id = :rideid");
	q.setString("email", email);
	q.setInteger("rideid", rideId);
	q.executeUpdate();
	tx.commit();
	Riderequests request = new Riderequests();
	request.setUser(Database.getInstance().findUserByEmail(email));
	request.setCreatedrides(getRide(rideId));
	request.setStatus(0);
	request.setCreatedtime(new Date());
	request.setNotificationRead(0);
	Database.getInstance().save(request);
	Database.getInstance().getSession().flush();
	Database.getInstance().getSession().clear();
    }

    public static List<Createdrides> getRidesCreatedByAndJoinedBy(String userEmail) {
	List<Createdrides> results = (List<Createdrides>) Database.getInstance().getSession().createCriteria(Createdrides.class).add(Restrictions.eq("user.email", userEmail)).list();
	List<Riderequests> requests = getRidesJoinedBy(userEmail);
	for (Riderequests request : requests) {
	    results.add(request.getCreatedrides());
	}
	return results;
    }

    public static List<Createdrides> getRidesCreatedBy(String userEmail) {
	List<Createdrides> results = (List<Createdrides>) Database.getInstance().getSession().createCriteria(Createdrides.class).add(Restrictions.eq("user.email", userEmail)).list();
	return results;
    }

    public static List<Riderequests> getRidesJoinedBy(String userEmail) {
	String query = "from Riderequests where user = :userEmail";
	Query q = Database.getInstance().getSession().createQuery(query);
	q.setString("userEmail", userEmail);
	List<Riderequests> rides = q.list();
	return rides;
    }

    public static Riderequests getRideRequest(int requestId) {
	List<Riderequests> results = Database.getInstance().getSession().createCriteria(Riderequests.class).add(Restrictions.eq("id", requestId)).list();
	if (results.size() != 0) {
	    return results.get(0);
	}
	return null;
    }

    public static void leaveRide(int requestId) {
	Database.getInstance().delete(getRideRequest(requestId));
    }

    public static void deleteRide(int rideId) {
	Transaction tx = Database.getInstance().getSession().beginTransaction();
	String query = "delete from riderequests where rideid = :rideId";
	Query q = Database.getInstance().getSession().createSQLQuery(query);
	q.setInteger("rideId", rideId);
	q.executeUpdate();
	query = "delete from createdrides where id = :rideId";
	q = Database.getInstance().getSession().createSQLQuery(query);
	q.setInteger("rideId", rideId);
	q.executeUpdate();
	tx.commit();
    }

    public static List<RideRequest> findLatestRequests(String email, int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Riderequests where createdrides.id in (select id from Createdrides where createdby = :email) and status = 0 order by createdtime desc");
	q.setString("email", email);
	List<Riderequests> requests = q.setMaxResults(limit).list();
	List<RideRequest> results = new ArrayList<RideRequest>();
	for (Riderequests request : requests) {
	    results.add(new RideRequest(request));
	}
	return results;
    }

    public static List<RideRequest> findLatestSubscriptions(String email, int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Riderequests where createdrides.id in (select id from Createdrides where createdby = :email) and status = 1 order by createdtime desc");
	q.setString("email", email);
	List<Riderequests> requests = q.setMaxResults(limit).list();
	List<RideRequest> results = new ArrayList<RideRequest>();
	for (Riderequests request : requests) {
	    results.add(new RideRequest(request));
	}
	return results;
    }

    public static List<RideRequest> findLatestJoinies(String email, int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Riderequests where user=:email and status = 1 and notification_read = 0 order by createdtime desc");
	q.setString("email", email);
	List<Riderequests> requests = q.setMaxResults(limit).list();
	List<RideRequest> results = new ArrayList<RideRequest>();
	for (Riderequests request : requests) {
	    results.add(new RideRequest(request));
	}
	return results;
    }

    public static void updateReadNotifications(String email) {
	Transaction t = Database.getInstance().getSession().beginTransaction();
	Query q = Database.getInstance().getSession().createQuery("update Riderequests set notification_read = 1 where user=:email and status = 1 and notification_read = 0");
	q.setString("email", email);
	q.executeUpdate();
	t.commit();
    }

    public static List<Createdrides> findLatestRidesOf(String email, int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Createdrides where createdby=:email order by createdtime desc");
	q.setString("email", email);
	List<Createdrides> rides = q.setMaxResults(limit).list();
	return rides;
    }

    public static List<Createdrides> findLatestRides(int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Createdrides order by createdtime desc");
	q.setMaxResults(limit);
	List<Createdrides> rides = q.setMaxResults(limit).list();
	return rides;
    }

    public static List<Createdrides> findLatestRides() {
	Query q = Database.getInstance().getSession().createQuery("from Createdrides order by createdtime desc");
	List<Createdrides> rides = q.list();
	return rides;
    }

    public static List<Createdrides> findLatestPools() {
	List<Createdrides> rides = new ArrayList<Createdrides>();
	Query q = Database.getInstance().getSession().createQuery("from Createdrides where origin like '%hyderabad%' order by createdtime desc");
	rides.addAll(q.setMaxResults(3).list());
	q = Database.getInstance().getSession().createQuery("from Createdrides where origin like '%bengaluru%' order by createdtime desc");
	rides.addAll(q.setMaxResults(3).list());
	q = Database.getInstance().getSession().createQuery("from Createdrides where origin like '%chennai%' order by createdtime desc");
	rides.addAll(q.setMaxResults(3).list());
	return rides;
    }

    public static List<Createdrides> findHyderabadRides() {
	List<Createdrides> rides = new ArrayList<Createdrides>();
	Query q = Database.getInstance().getSession().createQuery("from Createdrides where origin like '%hyderabad%' order by createdtime desc");
	rides.addAll(q.setMaxResults(3).list());
	return rides;
    }

    public static List<Createdrides> findBengaluruRides() {
	List<Createdrides> rides = new ArrayList<Createdrides>();
	Query q = Database.getInstance().getSession().createQuery("from Createdrides where origin like '%bengaluru%' order by createdtime desc");
	rides.addAll(q.setMaxResults(3).list());
	return rides;
    }

    public static List<Createdrides> findChennaiRides() {
	List<Createdrides> rides = new ArrayList<Createdrides>();
	Query q = Database.getInstance().getSession().createQuery("from Createdrides where origin like '%chennai%' order by createdtime desc");
	rides.addAll(q.setMaxResults(3).list());
	return rides;
    }

    public static void saveSearches(List<Createdrides> rides) {
	for (Createdrides ride : rides) {
	    Ridesearches search = new Ridesearches();
	    search.setCreatedrides(ride);
	    search.setCreatedtime(new Date());
	    Database.getInstance().save(search);
	}
    }

    public static List<Ridesearches> findLatestSearches(int limit) {
	Query q = Database.getInstance().getSession().createQuery("from Ridesearches order by createdtime desc");
	List<Ridesearches> rides = q.setMaxResults(limit).list();
	return rides;
    }
}
