package commutetogether.dao;

import commutetogether.data.Database;
import commutetogether.data.User;

public class UserDAO {
    
    public static boolean activateUser(String email) {
	User user = Database.getInstance().findUserByEmail(email);
	if (user == null) {
	    return false;
	}
	user.setDeactivateReason(null);
	Database.getInstance().update(user);
	return true;
    }
}
