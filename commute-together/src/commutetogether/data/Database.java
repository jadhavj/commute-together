package commutetogether.data;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;

public class Database {

    private Session session;
    private static Database INSTANCE;

    private static class GenericClass<T extends Serializable> {

	private final Class<T> type;

	public GenericClass(Class<T> type) {
	    this.type = type;
	}

	public Class<T> getType() {
	    return this.type;
	}
    }

    private Database() {
	initialize();
    };

    public static Database getInstance() {
	if (INSTANCE == null) {
	    INSTANCE = new Database();
	}
	return INSTANCE;
    }

    public void initialize() {
	Configuration cfg = new Configuration();
	cfg.configure("hibernate.cfg.xml");

	SessionFactory factory = cfg.buildSessionFactory();
	session = factory.openSession();
    }

    public void save(Serializable o) {
	Transaction t = session.beginTransaction();
	session.save(o);
	t.commit();
    }

    public void delete(Serializable o) {
	Transaction t = session.beginTransaction();
	session.delete(o);
	t.commit();
	session.flush();
	session.clear();
    }

    public void update(Serializable o) {
	Transaction t = session.beginTransaction();
	session.update(o);
	t.commit();

    }

    public <T extends Serializable> List<T> list(T o) {
	GenericClass<T> c = new GenericClass(o.getClass());
	List<T> results = (List<T>) session.createCriteria(c.getType()).add(Example.create(o)).list();
	return results;
    }

    public User findUserByEmail(String email) {
	List<User> results = (List<User>) session.createCriteria(User.class).add(Restrictions.eq("email", email)).list();
	if (results.size() > 1) {
	    throw new IllegalStateException("Not just one found.");
	}
	if (results == null || results.size() == 0) {
	    return null;
	}
	return results.get(0);
    }

    public Session getSession() {
	return session;
    }
}
