package commutetogether.models;

import java.util.List;

import commutetogether.dao.MessagesDAO;
import commutetogether.data.Database;
import commutetogether.data.Messages;
import commutetogether.data.User;

public class Message {
    private Messages messages;
    private User toUser;
    private User fromUser;
    private List<Messages> related;

    public Message(Messages messages) {
	this.messages = messages;
	toUser = Database.getInstance().findUserByEmail(messages.getTouser());
	fromUser = Database.getInstance().findUserByEmail(messages.getFromuser());
	if (toUser != null) {
	    related = MessagesDAO.findRelatedMessagesFor(toUser.getEmail(), messages.getId().toString());
	}
    }

    public List<Messages> getRelated() {
	return related;
    }

    public Messages getMessages() {
	return messages;
    }

    public User getToUser() {
	return toUser;
    }

    public User getFromUser() {
	return fromUser;
    }
}
