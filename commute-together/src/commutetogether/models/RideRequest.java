package commutetogether.models;

import commutetogether.data.Database;
import commutetogether.data.Riderequests;
import commutetogether.data.User;

public class RideRequest {

    private Riderequests request;
    private User creator;
    private User requester;
    
    public RideRequest(Riderequests request) {
	this.request = request;
	this.creator = Database.getInstance().findUserByEmail(request.getCreatedrides().getUser().getEmail());
	this.requester = request.getUser(); 
    }

    public Riderequests getRequest() {
        return request;
    }

    public User getCreator() {
        return creator;
    }

    public User getRequester() {
        return requester;
    }
}
