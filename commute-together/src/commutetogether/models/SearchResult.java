package commutetogether.models;

import commutetogether.data.Createdrides;

public class SearchResult {
    private Createdrides createdRide;
    private int status;
    
    public Createdrides getCreatedRide() {
        return createdRide;
    }
    public void setCreatedRide(Createdrides createdRide) {
        this.createdRide = createdRide;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
}
