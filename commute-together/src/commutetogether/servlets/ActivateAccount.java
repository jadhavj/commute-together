package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.UserDAO;
import commutetogether.util.validator.Validator;

public class ActivateAccount extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ActivateAccount() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String email = request.getParameter("email");
	Validator validator = new Validator();
	if (email == null || email.isEmpty()) {
	    validator.hasErrors();
	    validator.getErrors().put("invalid", "Please specify a valid email.");
	} else if (UserDAO.activateUser(email)) {
	    validator.hasErrors();
	    validator.getErrors().put("activated", "User activated.");
	}
	request.setAttribute("errors", validator.getErrors());
	RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
