package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Transaction;

import commutetogether.data.Database;
import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class ChangeEmail extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ChangeEmail() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String oldEmail = request.getParameter("oldEmail");
	String newEmail = request.getParameter("newEmail");

	Validator validator = new Validator();
	User user = Database.getInstance().findUserByEmail(oldEmail);
	String key1 = user != null ? user.getChangeemailkey() : null;
	String key2 = request.getParameter("changeemailkey");
	validator.hasErrors();
	if (key1 == null || !key1.equals(key2)) {
	    validator.getErrors().put("email", "Your link has expired.");
	    request.setAttribute("errors", validator.getErrors());
	    RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
	    rd.forward(request, response);
	    return;
	} else {
	    validator.getErrors().put("email", "Your email has been successfully changed.");
	    request.setAttribute("errors", validator.getErrors());
	}
	Transaction tx = Database.getInstance().getSession().beginTransaction();
	Query q = Database.getInstance().getSession().createQuery("update User set email = ?, changeemailkey='' where email = ?");
	q.setString(0, newEmail);
	q.setString(1, oldEmail);
	q.executeUpdate();
	tx.commit();
	RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
