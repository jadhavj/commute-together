package commutetogether.servlets;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.util.validator.Validator;

public class ContactUs extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ContactUs() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String fullName = request.getParameter("fullName");
	String email = request.getParameter("email");
	String subject = request.getParameter("subject");
	String message = request.getParameter("message");

	Properties props = new Properties();
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.socketFactory.port", "465");
	props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.port", "465");

	Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
	    protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication("kensiumtech@gmail.com", "kensium123");
	    }
	});

	MimeMessage msg = new MimeMessage(session);
	try {
	    msg.setFrom(new InternetAddress("kensiumtech@gmail.com"));
	    msg.addRecipient(Message.RecipientType.TO, new InternetAddress("kensiumtech@gmail.com"));
	    msg.setSubject("Query from " + email);
	    msg.setText("We've recieved a query.<br/><br/>Name: " + fullName + "<br/>Email: " + email + "<br/><br/>Subject: " + subject + "<br/>Message: " + message, "utf-8", "html");
	    Transport.send(msg);
	} catch (MessagingException e) {
	    throw new RuntimeException(e);
	}

	try {
	    msg = new MimeMessage(session);
	    msg.setFrom(new InternetAddress("kensiumtech@gmail.com"));
	    msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
	    msg.setSubject("Commute Together: Query Received");
	    msg.setText("Dear " + fullName + "<br/><br/>We've received your query and will follow up shortly." + "<br/><br/>Thanks,<br/>Commute Together Team<br/><br/>" + "Subject: " + subject + "<br/>Message: " + message, "utf-8", "html");
	    Transport.send(msg);
	} catch (MessagingException e) {
	    throw new RuntimeException(e);
	}
	
	Validator validator = new Validator();
	validator.getErrors().put("queryReceived", "We've received your query and will follow up shortly.");
	request.setAttribute("errors", validator.getErrors());
	RequestDispatcher rd = request.getRequestDispatcher("contactus.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
