package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.MessagesDAO;
import commutetogether.data.Messages;

public class CreateForward extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CreateForward() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String id = request.getParameter("id");
	Messages message = MessagesDAO.findMessageById(id);
	request.setAttribute("message", message);
	RequestDispatcher rd = request.getRequestDispatcher("newMessage");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
