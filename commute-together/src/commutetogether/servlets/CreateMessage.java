package commutetogether.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.Createdrides;
import commutetogether.data.Database;
import commutetogether.data.Messages;
import commutetogether.data.User;

public class CreateMessage extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CreateMessage() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = (User) request.getSession().getAttribute("user");
	String[] toUsers = request.getParameterValues("emails");
	String message = request.getParameter("message");
	String subject = message.length() < 50 ? message : message.substring(0, 49);
	subject.replace("\n", " ");
	String linkedId = UUID.randomUUID().toString();
	Date date = new Date();
	String rideId = request.getParameter("rideId");
	if (rideId != null) {
		Messages messages = new Messages();
		messages.setTouser(null);
		messages.setFromuser(user.getEmail());
		messages.setCreatedtime(date);
		messages.setLinkedid(linkedId);
		messages.setMessage(message);
		messages.setSubject(subject);
		messages.setShowinoutbox(1);
		messages.setIsread(0);
		Createdrides ride = RidesDAO.getRide(Integer.parseInt(rideId));
		messages.setCreatedrides(ride);
		Database.getInstance().save(messages);
		response.sendRedirect("dashboard.jsp?rideId=" + Integer.parseInt(rideId));
	} else {
	    for (String toUser : toUsers) {
		Messages messages = new Messages();
		messages.setTouser(toUser);
		messages.setFromuser(user.getEmail());
		messages.setCreatedtime(date);
		messages.setLinkedid(linkedId);
		messages.setMessage(message);
		messages.setSubject(subject);
		messages.setShowinoutbox(1);
		messages.setIsread(0);
		Database.getInstance().save(messages);
	    }
	    response.sendRedirect("showMessages");
	}
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
