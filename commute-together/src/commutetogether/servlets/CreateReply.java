package commutetogether.servlets;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.MessagesDAO;
import commutetogether.data.Database;
import commutetogether.data.Messages;

public class CreateReply extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public CreateReply() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String id = request.getParameter("id");
	Messages message = MessagesDAO.findMessageById(id);
	String subject = request.getParameter("message").length() < 50 ? request.getParameter("message") : request.getParameter("message").substring(0, 49);
	subject.replace("\n", " ");
	Messages reply = new Messages();
	reply.setTouser(message.getFromuser());
	reply.setFromuser(message.getTouser());
	reply.setCreatedtime(new Date());
	reply.setLinkedid(message.getLinkedid());
	reply.setMessage(request.getParameter("message"));
	reply.setSubject(subject);
	reply.setShowinoutbox(1);
	reply.setIsread(0);
	Database.getInstance().save(reply);
	response.sendRedirect("showMessages");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
