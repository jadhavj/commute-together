package commutetogether.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import commutetogether.data.Createdrides;
import commutetogether.data.Database;
import commutetogether.data.User;

public class CreateRide extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int maxFileSize = 50 * 1024;
	private int maxMemSize = 4 * 1024;
	private String filePath;
	private File file;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a");

	public CreateRide() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Createdrides ride = new Createdrides();
		ride.setUser((User) request.getSession().getAttribute("user"));
		ride.setCreatedtime(new Date());

		int multiways = 1;
		String startLoc = null;
		String endLoc = null;
		String twoWayStartLoc = null;
		String twoWayEndLoc = null;
		String forwardCheckpoints = null;
		String twoWayForwardCheckpoints = null;
		String twoWayBackwardCheckpoints = null;
		String multiwayForwardCheckpoints = null;
		String multiwayStartLoc = null;
		String multiwayEndLoc = null;
		String mw2StartLoc = null;
		String mw2EndLoc = null;
		String mw2ForwardCheckpoints = null;
		String mw2StartTime = null;
		String mw3StartLoc = null;
		String mw3EndLoc = null;
		String mw3ForwardCheckpoints = null;
		String mw3StartTime = null;
		String mw4StartLoc = null;
		String mw4EndLoc = null;
		String mw4ForwardCheckpoints = null;
		String mw4StartTime = null;
		String mw5StartLoc = null;
		String mw5EndLoc = null;
		String mw5ForwardCheckpoints = null;
		String mw5StartTime = null;

		if (ServletFileUpload.isMultipartContent(request)) {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(maxMemSize);
			factory.setRepository(new File(getInitParameter("tempFiles")));
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(maxFileSize);
			filePath = getInitParameter("tempFiles");
			List<FileItem> fileItems = null;
			try {
				fileItems = upload.parseRequest(request);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}

			for (FileItem fi : fileItems) {
				if (!fi.isFormField()) {
					String fileName = fi.getName();
					if (fileName == null || fileName.isEmpty()) {
						continue;
					}
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					try {
						fi.write(file);
						if (fileName.lastIndexOf("\\") >= 0) {
							file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
						} else {
							file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
						}
						FileInputStream inputStream = new FileInputStream(file);
						byte[] fileBytes = new byte[(int) file.length()];
						inputStream.read(fileBytes);
						inputStream.close();
						ride.setPhoto(fileBytes);
						file.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					InputStream in = fi.getInputStream();
					if (fi.getFieldName().equals("whichWay")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String whichWay = new String(str, "UTF8");
						ride.setWhichway(whichWay != null ? Integer.parseInt(whichWay) : null);
					} else if (fi.getFieldName().equals("multiways")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						multiways = Integer.parseInt(new String(str, "UTF8"));
					} else if (fi.getFieldName().equals("rideType")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String rideType = new String(str, "UTF8");
						ride.setRidetype(rideType != null ? Integer.parseInt(rideType) : null);
					} else if (fi.getFieldName().equals("startDate")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String startDate = new String(str, "UTF8");
						try {
							ride.setStartdate(dateFormat.parse(startDate));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else if (fi.getFieldName().equals("endDate")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String endDate = new String(str, "UTF8");
						try {
							ride.setEnddate(dateFormat.parse(endDate));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					else if (fi.getFieldName().equals("startLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						startLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("endLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						endLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("forwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						forwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("startTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String startTime = new String(str, "UTF8");
						try {
							ride.setStarttime(timeFormat.parse(startTime));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					else if (fi.getFieldName().equals("twoWayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayStartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayEndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayBackwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayBackwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String twoWayStartTime = new String(str, "UTF8");
						try {
							ride.setStarttime(timeFormat.parse(twoWayStartTime));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else if (fi.getFieldName().equals("twoWayReturnTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String twoWayReturnTime = new String(str, "UTF8");
						try {
							ride.setReturntime(timeFormat.parse(twoWayReturnTime));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					else if (fi.getFieldName().equals("multiwayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						multiwayStartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("multiwayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						multiwayEndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("multiwayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						multiwayForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("multiwayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String multiwayStartTime = new String(str, "UTF8");
						try {
							ride.setStarttime(timeFormat.parse(multiwayStartTime));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					else if (fi.getFieldName().equals("ID2_multiwayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw2StartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID2_multiwayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw2EndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID2_multiwayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw2ForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID2_multiwayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw2StartTime = new String(str, "UTF8");
					}

					else if (fi.getFieldName().equals("ID3_multiwayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw3StartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID3_multiwayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw3EndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID3_multiwayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw3ForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID3_multiwayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw3StartTime = new String(str, "UTF8");
					}

					else if (fi.getFieldName().equals("ID4_multiwayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw4StartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID4_multiwayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw4EndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID4_multiwayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw4ForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID4_multiwayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw4StartTime = new String(str, "UTF8");
					}

					else if (fi.getFieldName().equals("ID5_multiwayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw5StartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID5_multiwayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw5EndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID5_multiwayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw5ForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("ID5_multiwayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						mw5StartTime = new String(str, "UTF8");
					}

					else if (fi.getFieldName().equals("wheels")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String wheels = new String(str, "UTF8");
						ride.setWheels(wheels != null ? Integer.parseInt(wheels) : null);
					} else if (fi.getFieldName().equals("ac")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String ac = new String(str, "UTF8");
						ride.setAc(ac != null ? Integer.parseInt(ac) : null);
					} else if (fi.getFieldName().equals("cartype")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String cartype = new String(str, "UTF8");
						ride.setCartype(cartype != null ? Integer.parseInt(cartype) : null);
					} else if (fi.getFieldName().equals("vehicleNo")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String vehilceNo = new String(str, "UTF8");
						ride.setVehicleno(vehilceNo);
					} else if (fi.getFieldName().equals("vehicleColor")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String vehilceColor = new String(str, "UTF8");
						ride.setVehiclecolor(vehilceColor);
					} else if (fi.getFieldName().equals("licenseNo")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String licenseNo = new String(str, "UTF8");
						ride.setLicenseno(licenseNo);
					} else if (fi.getFieldName().equals("model")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String model = new String(str, "UTF8");
						ride.setModel(model);
					}
				}
			}
		}

		if (ride.getWhichway() == 1) {
			ride.setOrigin(startLoc);
			ride.setDestination(endLoc);
			ride.setForwardcheckpoints(forwardCheckpoints);
		} else if (ride.getWhichway() == 2) {
			ride.setOrigin(twoWayStartLoc);
			ride.setDestination(twoWayEndLoc);
			ride.setForwardcheckpoints(twoWayForwardCheckpoints);
			ride.setBackwardcheckpoints(twoWayBackwardCheckpoints);
		} else if (ride.getWhichway() == 3) {
			String multipleUUID = UUID.randomUUID().toString();
			ride.setMultipleuuid(multipleUUID);
			ride.setOrigin(multiwayStartLoc);
			ride.setDestination(multiwayEndLoc);
			ride.setForwardcheckpoints(multiwayForwardCheckpoints);

			for (int i = 2; i <= multiways; i++) {
				Createdrides multipleRide = new Createdrides();
				multipleRide.setMultipleuuid(multipleUUID);
				multipleRide.setWhichway(ride.getWhichway());
				multipleRide.setRidetype(ride.getRidetype());
				multipleRide.setStartdate(new Date(ride.getStartdate().getTime()));
				multipleRide.setEnddate(ride.getEnddate() != null ? new Date(ride.getEnddate().getTime()) : null);
				if (i == 2) {
					multipleRide.setOrigin(mw2StartLoc);
					multipleRide.setDestination(mw2EndLoc);
					multipleRide.setForwardcheckpoints(mw2ForwardCheckpoints);
					if (mw2StartTime == null) {
						continue;
					}
					try {
						multipleRide.setStarttime(timeFormat.parse(mw2StartTime));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				if (i == 3) {
					multipleRide.setOrigin(mw3StartLoc);
					multipleRide.setDestination(mw3EndLoc);
					multipleRide.setForwardcheckpoints(mw3ForwardCheckpoints);
					if (mw3StartTime == null) {
						continue;
					}
					try {
						multipleRide.setStarttime(timeFormat.parse(mw3StartTime));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else if (i == 4) {
					multipleRide.setOrigin(mw4StartLoc);
					multipleRide.setDestination(mw4EndLoc);
					multipleRide.setForwardcheckpoints(mw4ForwardCheckpoints);
					if (mw4StartTime == null) {
						continue;
					}
					try {
						multipleRide.setStarttime(timeFormat.parse(mw4StartTime));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} else if (i == 5) {
					multipleRide.setOrigin(mw5StartLoc);
					multipleRide.setDestination(mw5EndLoc);
					multipleRide.setForwardcheckpoints(mw5ForwardCheckpoints);
					if (mw5StartTime == null) {
						continue;
					}
					try {
						multipleRide.setStarttime(timeFormat.parse(mw5StartTime));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				multipleRide.setWheels(ride.getWheels());
				multipleRide.setAc(ride.getAc());
				multipleRide.setCartype(ride.getCartype());
				multipleRide.setModel(ride.getModel());
				multipleRide.setVehicleno(new String(ride.getVehicleno()));
				multipleRide.setVehiclecolor(new String(ride.getVehiclecolor()));
				multipleRide.setLicenseno(new String(ride.getLicenseno()));
				multipleRide.setModel(new String(ride.getModel()));
				multipleRide.setPhoto(ride.getPhoto());
				multipleRide.setUser(ride.getUser());
				multipleRide.setCreatedtime(new Date());
				Database.getInstance().save(multipleRide);
			}
		}
		Database.getInstance().save(ride);
		request.getSession().setAttribute("created", true);
		response.sendRedirect("rideDetails.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
