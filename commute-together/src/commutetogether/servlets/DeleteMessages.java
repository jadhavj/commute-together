package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.MessagesDAO;

public class DeleteMessages extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DeleteMessages() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String[] ids = request.getParameterValues("ids");
	String outbox = request.getParameter("outbox");
	if (ids != null && (outbox == null || (outbox != null && outbox.equals("false")))) {
	    MessagesDAO.deleteMessages(ids);
	    response.sendRedirect("showMessages");
	} else if (ids != null && outbox != null) {
	    MessagesDAO.deleteOutboxMessages(ids);
	    response.sendRedirect("showOutbox");

	}

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
