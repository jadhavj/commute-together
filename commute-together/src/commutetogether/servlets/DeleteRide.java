package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;

public class DeleteRide extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public DeleteRide() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	RidesDAO.deleteRide(Integer.parseInt(request.getParameter("rideId")));
	response.sendRedirect("viewRides");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
