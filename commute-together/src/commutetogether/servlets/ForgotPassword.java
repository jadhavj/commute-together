package commutetogether.servlets;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.data.Database;
import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class ForgotPassword extends HttpServlet {

    public ForgotPassword() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
	String to = request.getParameter("email");
	User user = Database.getInstance().findUserByEmail(to);
	Validator validator = new Validator();
	validator.hasErrors();
	if (user == null) {
	    request.setAttribute("errors", validator.getErrors());
	    validator.getErrors().put("email", "The email you entered is not registered with us.");
	    rd.forward(request, response);
	    return;
	}
	validator.getErrors().put("password", "Your password has been sent to your email.");
	request.setAttribute("errors", validator.getErrors());

	Properties props = new Properties();
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.socketFactory.port", "465");
	props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.port", "465");

	Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
	    protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication("kensiumtech@gmail.com", "kensium123");
	    }
	});

	user.setPassword(UUID.randomUUID().toString().substring(0, 8));
	Database.getInstance().update(user);
	try {
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress("kensiumtech@gmail.com"));
	    message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	    message.setSubject("Your forgotten password for Commute Together");
	    message.setText("We have reset your password you forgot for Commute Together which is: " + user.getPassword());

	    Transport.send(message);
	} catch (MessagingException e) {
	    throw new RuntimeException(e);
	}
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
