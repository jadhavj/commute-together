package commutetogether.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.Createdrides;
import commutetogether.data.Database;
import commutetogether.data.User;

public class ImageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String rideId = request.getParameter("rideId");
	if (rideId != null) {
	    Createdrides ride = RidesDAO.getRide(Integer.parseInt(rideId));
	    byte[] photoBytes = null;
	    if (ride.getPhoto() == null) {
		File file = new File(getInitParameter("images") + "//userpic.jpg");
		FileInputStream inputStream = new FileInputStream(file);
		photoBytes = new byte[(int) file.length()];
		inputStream.read(photoBytes);
		inputStream.close();
	    } else {
		photoBytes = ride.getPhoto();
	    }

	    response.reset();
	    response.setContentType("image/jped");
	    response.setHeader("Content-Length", String.valueOf(photoBytes.length));
	    response.getOutputStream().write(photoBytes);
	} else {
	    User user = new User();
	    user.setEmail(request.getParameter("email"));
	    user = Database.getInstance().findUserByEmail(user.getEmail());
	    byte[] photoBytes = null;
	    if (user.getPhoto() == null) {
		File file = new File(getInitParameter("images") + "//userpic.jpg");
		FileInputStream inputStream = new FileInputStream(file);
		photoBytes = new byte[(int) file.length()];
		inputStream.read(photoBytes);
		inputStream.close();
	    } else {
		photoBytes = user.getPhoto();
	    }

	    response.reset();
	    response.setContentType("image/jped");
	    response.setHeader("Content-Length", String.valueOf(photoBytes.length));
	    response.getOutputStream().write(photoBytes);
	}
    }

}