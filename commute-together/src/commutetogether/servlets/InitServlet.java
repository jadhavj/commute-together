package commutetogether.servlets;

import javax.servlet.http.HttpServlet;

import commutetogether.data.Database;

public class InitServlet extends HttpServlet {

    public void init() {
	Database.getInstance();
    }

}
