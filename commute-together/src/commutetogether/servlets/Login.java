package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.data.Database;
import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class Login extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public Login() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String email = request.getParameter("email");
	String password = request.getParameter("password");

	Validator validator = new Validator();
	validator.validatePassword(email, password);
	if (!validator.isAllClear()) {
	    request.setAttribute("errors", validator.getErrors());
	    RequestDispatcher rd = request.getRequestDispatcher("/");
	    rd.forward(request, response);
	} else {
	    User user = new User();
	    user.setEmail(email);
	    user = Database.getInstance().findUserByEmail(user.getEmail());
	    if (user.getDeleteReason() != null) {
		validator.hasErrors();
		validator.getErrors().put("login", "Either of the Email or Password you entered is incorrect.");
		request.setAttribute("errors", validator.getErrors());
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	    } else if (user.getDeactivateReason() != null) {
		validator.hasErrors();
		validator.getErrors().put("login", "Your profile is in deactivated state. Please call us to reactivate your profile.");
		request.setAttribute("errors", validator.getErrors());
		RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
		rd.forward(request, response);
	    } else {
		request.getSession().setAttribute("user", user);
		if (request.getParameter("rememberMe") != null) {
		    Cookie rememberMe = new Cookie("email", user.getEmail());
		    response.addCookie(rememberMe);
		} else {
		    Cookie rememberMe = new Cookie("email", "");
		    response.addCookie(rememberMe);
		}
		RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
		rd.forward(request, response);
	    }
	}
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
