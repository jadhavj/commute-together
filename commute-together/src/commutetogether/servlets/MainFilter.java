package commutetogether.servlets;

//Import required java libraries
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.data.User;

//Implements Filter class
public class MainFilter implements Filter {
    FilterConfig config;

    public void setFilterConfig(FilterConfig config) {
      this.config = config;
    }

    public FilterConfig getFilterConfig() {
      return config;
    }

    public void init(FilterConfig config) throws ServletException {
	setFilterConfig(config);
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws java.io.IOException, ServletException {
	User user = (User) ((HttpServletRequest)request).getSession().getAttribute("user");
	String resource = ((HttpServletRequest)request).getRequestURI().substring(((HttpServletRequest)request).getContextPath().length());
	if (user == null 
		&& !resource.equals("/activateAccount") 
		&& !resource.equals("/admin.jsp") 
		&& !resource.equals("/index.jsp") 
		&& !resource.equals("/")
		&& !resource.startsWith("/css")
		&& !resource.startsWith("/js")
		&& !resource.startsWith("/fonts")
		&& !resource.startsWith("/images")
		&& !resource.startsWith("/login")
		&& !resource.startsWith("/imageServlet")
		&& !resource.startsWith("/aboutus.jsp")
		&& !resource.startsWith("/privacy.jsp")
		&& !resource.startsWith("/faqs.jsp")
		&& !resource.startsWith("/contactus.jsp")
		&& !resource.startsWith("/forgotPassword")
		&& !resource.startsWith("/registration.jsp")
		&& !resource.startsWith("/registerFirst")
		&& !resource.startsWith("/static-content")
		) {
	    ((HttpServletResponse) response).sendRedirect("?login=true");
	    
	} else {
		chain.doFilter(request, response);
	}
    }

    public void destroy() {
    }
}