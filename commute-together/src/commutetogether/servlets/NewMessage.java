package commutetogether.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.MessagesDAO;
import commutetogether.data.User;

public class NewMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public NewMessage() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    User user = (User) request.getSession().getAttribute("user");
	    String toUser = request.getParameter("toUser");
	    if (toUser != null) {
		request.setAttribute("toUser", toUser);
	    }
	    
	    List<User> users = MessagesDAO.getUsers(user.getEmail());
	    request.setAttribute("users", users);
	    RequestDispatcher rd = request.getRequestDispatcher("newMessage.jsp");
	    rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    doGet(request, response);
	}

}
