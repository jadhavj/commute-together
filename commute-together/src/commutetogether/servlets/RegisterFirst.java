package commutetogether.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class RegisterFirst extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RegisterFirst() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	SimpleDateFormat dobFormat = new SimpleDateFormat("dd/MM/yyyy");
	String email = request.getParameter("email");
	String fname = request.getParameter("fname");
	String organization = request.getParameter("organization");
	String gender = request.getParameter("gender");
	String mobile = request.getParameter("mobile");
	String lname = request.getParameter("lname");
	String occupation = request.getParameter("occupation");
	String dob = request.getParameter("dob");
	String idProof = request.getParameter("idproof");
	String emergency = request.getParameter("emergency");
	String bloodGroup = request.getParameter("bloodgroup");
	String insurance = request.getParameter("insurance");

	User user = new User();
	user.setEmail(email);
	user.setFname(fname);
	user.setOrganization(organization);
	user.setGender(gender);
	user.setMobile(mobile);
	user.setLname(lname);
	user.setOccupation(occupation);
	try {
	    user.setDob(dobFormat.parse(dob));
	} catch (ParseException e) {
	    e.printStackTrace();
	}
	user.setIdproof(idProof);
	user.setEmergency(emergency);
	user.setBloodgroup(Integer.parseInt(bloodGroup));
	user.setInsurance(insurance);
	request.getSession().setAttribute("user", user);

	Validator validator = new Validator();
	validator.validateEmail(email, true);
	validator.validateMobile(mobile);
	validator.validateDob(dob);
	validator.validateIdProof(idProof);
	validator.validateEmergency(emergency);
	validator.validateInsurance(insurance);

	if (!validator.isAllClear()) {
	    request.setAttribute("allClear", "" + validator.isAllClear());
	    request.setAttribute("errors", validator.getErrors());
	    RequestDispatcher rd = request.getRequestDispatcher("registration.jsp");
	    rd.forward(request, response);
	} else {
	    RequestDispatcher rd = request.getRequestDispatcher("aboutMe.jsp");
	    rd.forward(request, response);
	}
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
