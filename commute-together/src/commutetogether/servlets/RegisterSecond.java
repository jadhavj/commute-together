package commutetogether.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import commutetogether.data.Database;
import commutetogether.data.User;

public class RegisterSecond extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int maxFileSize = 50 * 1024;
	private int maxMemSize = 4 * 1024;
	private String filePath;
	private File file;

	public RegisterSecond() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("user");
		user.setPassword(UUID.randomUUID().toString().substring(0, 8));

		user.setHabitchat(0);
		user.setHabiteating(0);
		user.setHabitmusic(0);
		user.setHabitphone(0);
		user.setHabitreading(0);
		user.setHabitsleeping(0);
		user.setHabitsmoking(0);
		user.setHabitworking(0);
		if (ServletFileUpload.isMultipartContent(request)) {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(maxMemSize);
			factory.setRepository(new File(getInitParameter("tempFiles")));
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(maxFileSize);
			filePath = getInitParameter("tempFiles");
			List<FileItem> fileItems = null;
			try {
				fileItems = upload.parseRequest(request);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}

			for (FileItem fi : fileItems) {
				if (!fi.isFormField()) {
					String fileName = fi.getName();
					if (fileName == null || fileName.isEmpty()) {
						continue;
					}
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					try {
						fi.write(file);
						if (fileName.lastIndexOf("\\") >= 0) {
							file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
						} else {
							file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
						}
						FileInputStream inputStream = new FileInputStream(file);
						byte[] fileBytes = new byte[(int) file.length()];
						inputStream.read(fileBytes);
						inputStream.close();
						user.setPhoto(fileBytes);
						file.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					InputStream in = fi.getInputStream();
					if (fi.getFieldName().equals("body")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String body = new String(str, "UTF8");
						user.setBody(body != null ? Integer.parseInt(body) : null);
					} else if (fi.getFieldName().equals("language")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String language = new String(str, "UTF8");
						user.setLanguage(language);
					} else if (fi.getFieldName().equals("ethnicity")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String ethnicity = new String(str, "UTF8");
						user.setEthnicity(ethnicity != null ? Integer.parseInt(ethnicity) : null);
					} else if (fi.getFieldName().equals("religion")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String religion = new String(str, "UTF8");
						user.setReligion(religion != null ? Integer.parseInt(religion) : null);
					} else if (fi.getFieldName().equals("religiousViews")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String religiousViews = new String(str, "UTF8");
						user.setReligiousview(religiousViews);
					} else if (fi.getFieldName().equals("diet")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String diet = new String(str, "UTF8");
						user.setDiet(diet != null ? Integer.parseInt(diet) : null);
					} else if (fi.getFieldName().equals("habitReading")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitReading = new String(str, "UTF8");
						user.setHabitreading(habitReading != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitSmoking")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitSmoking = new String(str, "UTF8");
						user.setHabitsmoking(habitSmoking != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitSleeping")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitSleeping = new String(str, "UTF8");
						user.setHabitsleeping(habitSleeping != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitWorking")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitWorking = new String(str, "UTF8");
						user.setHabitworking(habitWorking != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitChat")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitChat = new String(str, "UTF8");
						user.setHabitchat(habitChat != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitPhone")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitPhone = new String(str, "UTF8");
						user.setHabitphone(habitPhone != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitEating")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitEating = new String(str, "UTF8");
						user.setHabiteating(habitEating != null ? 1 : 0);
					} else if (fi.getFieldName().equals("habitMusic")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String habitMusic = new String(str, "UTF8");
						user.setHabitmusic(habitMusic != null ? 1 : 0);
					} else if (fi.getFieldName().equals("likes")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String likes = new String(str, "UTF8");
						user.setLikes(likes);
					} else if (fi.getFieldName().equals("dislikes")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String dislikes = new String(str, "UTF8");
						user.setDislikes(dislikes);
					}
				}
			}

		}
		Database.getInstance().save(user);
		request.getSession().removeAttribute("user");

		String to = user.getEmail();

		Properties props = new Properties();
		props.put("mail.smtp.host", getInitParameter("smtpHost"));
		props.put("mail.smtp.socketFactory.port", getInitParameter("socketFactoryPort"));
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", getInitParameter("auth"));
		props.put("mail.smtp.port", getInitParameter("smtpPort"));

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("kensiumtech@gmail.com", "kensium123");
			}
		});

		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("kensiumtech@gmail.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Your password for Commute Together");
			message.setText("Hi " + user.getFname()
					+ ",<br/><br/>Welcome to Commute Together. Please Login to active your Account.<br/><br/>"
					+ "User ID: " + user.getEmail() + "<br/>" + "Password:" + user.getPassword() + "<br/><br/>"
					+ "Please click <a href='www.commutetogether.com'>here</a> to Login" + "<br/><br/>" + "Thanks,"
					+ "<br/>" + "www.commutetogether.com", "utf-8", "html");

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		response.sendRedirect("?success=true");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
