package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.Createdrides;

public class RideDetails extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RideDetails() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	Createdrides ride = RidesDAO.getRide(Integer.parseInt(request.getParameter("rideId")));
	request.setAttribute("ride", ride);
	RequestDispatcher rd = request.getRequestDispatcher("editRide.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
