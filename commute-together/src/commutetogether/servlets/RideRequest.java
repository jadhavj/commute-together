package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.User;

public class RideRequest extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RideRequest() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String rideId = request.getParameter("rideId");
	if (rideId == null) {
	    response.sendRedirect("searchRides");
	    return;
	}
	User user = (User) request.getSession().getAttribute("user");
	RidesDAO.requestRide(Integer.parseInt(rideId), user.getEmail());
	request.setAttribute("redirect", true);
	RequestDispatcher rd = request.getRequestDispatcher("searchRides");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
