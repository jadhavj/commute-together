package commutetogether.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.Createdrides;
import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class SearchRides extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");

    public SearchRides() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = (User) request.getSession().getAttribute("user");
	String origin = request.getParameter("startLoc");
	String destination = request.getParameter("endLoc");
	String startTime = request.getParameter("startTime");
	String returnTime = request.getParameter("returnTime");
	String wayPointsString = request.getParameter("forwardCheckpoints");
	String city = request.getParameter("city");

	Validator validator = new Validator();
	try {
	    List<Createdrides> rides;
	    if (city != null) {
		rides = RidesDAO.searchRides(user.getEmail(), city, origin, destination);
		request.setAttribute("rides", rides);
	    } else {
		rides = RidesDAO.searchRides(user.getEmail(), origin, destination, startTime != null && !startTime.isEmpty() ? timeFormat.parse(startTime) : null, returnTime != null && !returnTime.isEmpty() ? timeFormat.parse(returnTime) : null, wayPointsString != null && !wayPointsString.isEmpty() ? wayPointsString : "");
		request.setAttribute("rides", rides);
	    }
	    if (request.getAttribute("redirect") == null) {
		    if (rides == null || rides.isEmpty()) {
			validator.getErrors().put("empty", "No results found.");
			request.setAttribute("errors", validator.getErrors());
		    }
	    } else {
		validator.getErrors().put("ride", "Request to join ride sent.");
		request.setAttribute("errors", validator.getErrors());
	    }
	    RidesDAO.saveSearches(rides);
	} catch (ParseException e) {
	    e.printStackTrace();
	}

	request.setAttribute("origin", origin);
	request.setAttribute("destination", destination);
	request.setAttribute("startTime", startTime);
	request.setAttribute("returnTime", returnTime);
	request.setAttribute("wayPointsString", wayPointsString);
	if (wayPointsString != null && !wayPointsString.isEmpty()) {
	    request.setAttribute("wayPoints", wayPointsString.split("~"));
	}
	RequestDispatcher rd = request.getRequestDispatcher("searchPage.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
