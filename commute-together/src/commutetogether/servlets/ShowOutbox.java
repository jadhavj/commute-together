package commutetogether.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.MessagesDAO;
import commutetogether.data.User;
import commutetogether.models.Message;

public class ShowOutbox extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ShowOutbox() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = (User) request.getSession().getAttribute("user");
	List<Message> messages = MessagesDAO.getOutboxMessagesFor(user.getEmail());
	request.getSession().setAttribute("messages", messages);
	RequestDispatcher rd = request.getRequestDispatcher("outbox.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
