package commutetogether.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;
import org.hibernate.Transaction;

import commutetogether.data.Database;
import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class UpdateProfile extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private int maxFileSize = 50 * 1024;
    private int maxMemSize = 4 * 1024;
    private String filePath;
    private File file;

    public UpdateProfile() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = (User) request.getSession().getAttribute("user");
	user = Database.getInstance().findUserByEmail(user.getEmail());
	SimpleDateFormat dobFormat = new SimpleDateFormat("dd/MM/yyyy");
	String email = user.getEmail();
	Validator validator = new Validator();
	user.setHabitchat(0);
	user.setHabiteating(0);
	user.setHabitmusic(0);
	user.setHabitphone(0);
	user.setHabitreading(0);
	user.setHabitsleeping(0);
	user.setHabitsmoking(0);
	user.setHabitworking(0);
	if (ServletFileUpload.isMultipartContent(request)) {
	    DiskFileItemFactory factory = new DiskFileItemFactory();
	    factory.setSizeThreshold(maxMemSize);
	    factory.setRepository(new File(getInitParameter("tempFiles")));
	    ServletFileUpload upload = new ServletFileUpload(factory);
	    upload.setSizeMax(maxFileSize);
	    filePath = getInitParameter("tempFiles");
	    List<FileItem> fileItems = null;
	    try {
		fileItems = upload.parseRequest(request);
	    } catch (FileUploadException e) {
		e.printStackTrace();
	    }

	    for (FileItem fi : fileItems) {
		if (!fi.isFormField()) {
		    String fileName = fi.getName();
		    if (fileName == null || fileName.isEmpty()) {
			continue;
		    }
		    if (fileName.lastIndexOf("\\") >= 0) {
			file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
		    } else {
			file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
		    }
		    try {
			fi.write(file);
			if (fileName.lastIndexOf("\\") >= 0) {
			    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
			} else {
			    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
			}
			FileInputStream inputStream = new FileInputStream(file);
			byte[] fileBytes = new byte[(int) file.length()];
			inputStream.read(fileBytes);
			inputStream.close();
			user.setPhoto(fileBytes);
			file.delete();
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		} else {
		    InputStream in = fi.getInputStream();
		    if (fi.getFieldName().equals("body")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String body = new String(str, "UTF8");
			user.setBody(body != null ? Integer.parseInt(body) : null);
		    } else if (fi.getFieldName().equals("language")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String language = new String(str, "UTF8");
			user.setLanguage(language);
		    } else if (fi.getFieldName().equals("ethnicity")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String ethnicity = new String(str, "UTF8");
			user.setEthnicity(ethnicity != null ? Integer.parseInt(ethnicity) : null);
		    } else if (fi.getFieldName().equals("religion")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String religion = new String(str, "UTF8");
			user.setReligion(religion != null ? Integer.parseInt(religion) : null);
		    } else if (fi.getFieldName().equals("religiousViews")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String religiousViews = new String(str, "UTF8");
			user.setReligiousview(religiousViews);
		    } else if (fi.getFieldName().equals("diet")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String diet = new String(str, "UTF8");
			user.setDiet(diet != null ? Integer.parseInt(diet) : null);
		    } else if (fi.getFieldName().equals("habitreading")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitReading = new String(str, "UTF8");
			user.setHabitreading(habitReading != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("habitsmoking")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitSmoking = new String(str, "UTF8");
			user.setHabitsmoking(habitSmoking != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("habitsleeping")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitSleeping = new String(str, "UTF8");
			user.setHabitsleeping(habitSleeping != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("habitworking")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitWorking = new String(str, "UTF8");
			user.setHabitworking(habitWorking != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("habitchat")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitChat = new String(str, "UTF8");
			user.setHabitchat(habitChat != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("habitphone")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitPhone = new String(str, "UTF8");
			user.setHabitphone(!habitPhone.isEmpty() ? 1 : 0);
		    } else if (fi.getFieldName().equals("habiteating")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitEating = new String(str, "UTF8");
			user.setHabiteating(habitEating != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("habitmusic")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String habitMusic = new String(str, "UTF8");
			user.setHabitmusic(habitMusic != null ? 1 : 0);
		    } else if (fi.getFieldName().equals("likes")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String likes = new String(str, "UTF8");
			user.setLikes(likes);
		    } else if (fi.getFieldName().equals("dislikes")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String dislikes = new String(str, "UTF8");
			user.setDislikes(dislikes);
		    } else if (fi.getFieldName().equals("email")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			email = new String(str, "UTF8");
		    } else if (fi.getFieldName().equals("fname")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String fname = new String(str, "UTF8");
			user.setFname(fname);
		    } else if (fi.getFieldName().equals("organization")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String organization = new String(str, "UTF8");
			user.setOrganization(organization);
		    } else if (fi.getFieldName().equals("gender")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String gender = new String(str, "UTF8");
			user.setGender(gender);
		    } else if (fi.getFieldName().equals("mobile")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String mobile = new String(str, "UTF8");
			user.setMobile(mobile);
			if (mobile != null)
			    validator.validateMobile(mobile);
		    } else if (fi.getFieldName().equals("lname")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String lname = new String(str, "UTF8");
			user.setLname(lname);
		    } else if (fi.getFieldName().equals("occupation")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String occupation = new String(str, "UTF8");
			user.setOccupation(occupation);
		    } else if (fi.getFieldName().equals("dob")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String dob = new String(str, "UTF8");
			try {
			    user.setDob(dobFormat.parse(dob));
			} catch (ParseException e) {
			    e.printStackTrace();
			}
			if (dob != null)
			    validator.validateDob(dob);
		    } else if (fi.getFieldName().equals("idproof")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String idproof = new String(str, "UTF8");
			user.setIdproof(idproof);
			if (idproof != null)
			    validator.validateIdProof(idproof);
		    } else if (fi.getFieldName().equals("emergency")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String emergency = new String(str, "UTF8");
			user.setEmergency(emergency);
			if (emergency != null)
			    validator.validateEmergency(emergency);
		    } else if (fi.getFieldName().equals("bloodgroup")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String bloodgroup = new String(str, "UTF8");
			user.setBloodgroup(Integer.parseInt(bloodgroup));
		    } else if (fi.getFieldName().equals("insurance")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String insurance = new String(str, "UTF8");
			user.setInsurance(insurance);
			if (insurance != null)
			    validator.validateInsurance(insurance);
		    } else if (fi.getFieldName().equals("insurance")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String insurance = new String(str, "UTF8");
			user.setInsurance(insurance);
			if (insurance != null)
			    validator.validateInsurance(insurance);
		    } else if (fi.getFieldName().equals("organizationvisible")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String organizationvisible = new String(str, "UTF8");
			if (!organizationvisible.isEmpty())
			    user.setOrganizationvisible(Integer.parseInt(organizationvisible));
		    } else if (fi.getFieldName().equals("occupationvisible")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String occupationvisible = new String(str, "UTF8");
			if (!occupationvisible.isEmpty())
			    user.setOccupationvisible(Integer.parseInt(occupationvisible));
		    } else if (fi.getFieldName().equals("mobilevisible")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String mobilevisible = new String(str, "UTF8");
			if (!mobilevisible.isEmpty())
			    user.setMobilevisible(Integer.parseInt(mobilevisible));
		    } else if (fi.getFieldName().equals("emailvisible")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String emailvisible = new String(str, "UTF8");
			if (!emailvisible.isEmpty())
			    user.setEmailvisible(Integer.parseInt(emailvisible));
		    } else if (fi.getFieldName().equals("lnamevisible")) {
			byte[] str = new byte[in.available()];
			in.read(str);
			String lnamevisible = new String(str, "UTF8");
			if (!lnamevisible.isEmpty())
			    user.setLnamevisible(Integer.parseInt(lnamevisible));
		    }
		}
	    }

	}

	if (!validator.isAllClear()) {
	    request.setAttribute("allClear", "" + validator.isAllClear());
	    request.setAttribute("errors", validator.getErrors());
	    user = Database.getInstance().findUserByEmail(user.getEmail());
	} else {
	    Validator validator1 = new Validator();
	    if (!user.getEmail().equals(email)) {
		validator1.validateEmail(email, true);
	    }
	    if (validator1.isAllClear()) {
		user.setChangeemailkey(UUID.randomUUID().toString());
		if (Database.getInstance().findUserByEmail(email) == null) {
		    String to = email;

		    Properties props = new Properties();
		    props.put("mail.smtp.host", "smtp.gmail.com");
		    props.put("mail.smtp.socketFactory.port", "465");
		    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		    props.put("mail.smtp.auth", "true");
		    props.put("mail.smtp.port", "465");

		    Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
			    return new PasswordAuthentication("kensiumtech@gmail.com", "kensium123");
			}
		    });

		    try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("kensiumtech@gmail.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Commute Together - Change Email Request");

			String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
			message.setText("Please click <a href='" + uri + "/changeEmail?oldEmail=" + user.getEmail() + "&newEmail=" + email
				+ "&changeemailkey=" + user.getChangeemailkey() + "'>here</a> to change your email address on Commute Together.",
				"utf-8", "html");

			Transport.send(message);
		    } catch (MessagingException e) {
			throw new RuntimeException(e);
		    }
		    validator.hasErrors();
		    validator
			    .getErrors()
			    .put("email",
				    "An email has been sent to the new email address. Please click on the link in the email to change your email address to the new one.");
		    request.setAttribute("errors", validator.getErrors());
		}
	    } else {
		user = Database.getInstance().findUserByEmail(user.getEmail());
		request.setAttribute("errors", validator1.getErrors());
	    }
	    Transaction tx = Database.getInstance().getSession().beginTransaction();
	    Database.getInstance().getSession().saveOrUpdate(user);
	    tx.commit();
	}
	request.getSession().setAttribute("user", user);

	
	if (request.getAttribute("errors") == null || ((java.util.Map<String, String>) request.getAttribute("errors")).isEmpty()) {
	    validator.getErrors().put("email", "Information updated successfully.");
	    request.setAttribute("errors", validator.getErrors());
	    
	}
	RequestDispatcher rd = request.getRequestDispatcher("myProfile.jsp");
	rd.forward(request, response);

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
