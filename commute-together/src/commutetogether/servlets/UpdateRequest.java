package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.Database;
import commutetogether.data.Riderequests;

public class UpdateRequest extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UpdateRequest() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String id = request.getParameter("id");
	if (id == null) {
	    return;
	}
	String action = request.getParameter("action");
	Riderequests rideRequest = RidesDAO.getRideRequest(Integer.parseInt(id));
	if (action.equals("approve")) {
		rideRequest.setStatus(1);
	} else if (action.equals("reject")) {
	    rideRequest.setStatus(2);
	}
	Database.getInstance().save(rideRequest);
	response.sendRedirect("viewRides");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
