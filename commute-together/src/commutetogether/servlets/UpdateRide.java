package commutetogether.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Transaction;

import commutetogether.dao.RidesDAO;
import commutetogether.data.Createdrides;
import commutetogether.data.Database;

public class UpdateRide extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private int maxFileSize = 50 * 1024;
	private int maxMemSize = 4 * 1024;
	private String filePath;
	private File file;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a");

	public UpdateRide() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Createdrides ride = new Createdrides();
		int multiways = 1;
		String startLoc = null;
		String endLoc = null;
		String twoWayStartLoc = null;
		String twoWayEndLoc = null;
		String forwardCheckpoints = null;
		String twoWayForwardCheckpoints = null;
		String twoWayBackwardCheckpoints = null;
		String multiwayForwardCheckpoints = null;
		String multiwayStartLoc = null;
		String multiwayEndLoc = null;
		String mw2StartLoc = null;
		String mw2EndLoc = null;
		String mw2ForwardCheckpoints = null;
		String mw2StartTime = null;
		String mw3StartLoc = null;
		String mw3EndLoc = null;
		String mw3ForwardCheckpoints = null;
		String mw3StartTime = null;
		String mw4StartLoc = null;
		String mw4EndLoc = null;
		String mw4ForwardCheckpoints = null;
		String mw4StartTime = null;
		String mw5StartLoc = null;
		String mw5EndLoc = null;
		String mw5ForwardCheckpoints = null;
		String mw5StartTime = null;

		if (ServletFileUpload.isMultipartContent(request)) {
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(maxMemSize);
			factory.setRepository(new File(getInitParameter("tempFiles")));
			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setSizeMax(maxFileSize);
			filePath = getInitParameter("tempFiles");
			List<FileItem> fileItems = null;
			try {
				fileItems = upload.parseRequest(request);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}

			for (FileItem fi : fileItems) {
				if (!fi.isFormField()) {
					String fileName = fi.getName();
					if (fileName == null || fileName.isEmpty()) {
						continue;
					}
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					try {
						fi.write(file);
						if (fileName.lastIndexOf("\\") >= 0) {
							file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
						} else {
							file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
						}
						FileInputStream inputStream = new FileInputStream(file);
						byte[] fileBytes = new byte[(int) file.length()];
						inputStream.read(fileBytes);
						inputStream.close();
						ride.setPhoto(fileBytes);
						file.delete();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					InputStream in = fi.getInputStream();
					if (fi.getFieldName().equals("whichWay")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String whichWay = new String(str, "UTF8");
						ride.setWhichway(whichWay != null ? Integer.parseInt(whichWay) : null);
					} else if (fi.getFieldName().equals("rideType")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String rideType = new String(str, "UTF8");
						ride.setRidetype(rideType != null ? Integer.parseInt(rideType) : null);
					} else if (fi.getFieldName().equals("startDate")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String startDate = new String(str, "UTF8");
						try {
							ride.setStartdate(dateFormat.parse(startDate));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else if (fi.getFieldName().equals("endDate")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String endDate = new String(str, "UTF8");
						try {
							ride.setEnddate(dateFormat.parse(endDate));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					else if (fi.getFieldName().equals("twoWayStartLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayStartLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayEndLoc")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayEndLoc = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayForwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayForwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayBackwardCheckpoints")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						twoWayBackwardCheckpoints = new String(str, "UTF8");
					} else if (fi.getFieldName().equals("twoWayStartTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String twoWayStartTime = new String(str, "UTF8");
						try {
							ride.setStarttime(timeFormat.parse(twoWayStartTime));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					} else if (fi.getFieldName().equals("twoWayReturnTime")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String twoWayReturnTime = new String(str, "UTF8");
						try {
							ride.setReturntime(timeFormat.parse(twoWayReturnTime));
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}

					else if (fi.getFieldName().equals("wheels")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String wheels = new String(str, "UTF8");
						ride.setWheels(wheels != null ? Integer.parseInt(wheels) : null);
					} else if (fi.getFieldName().equals("ac")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String ac = new String(str, "UTF8");
						ride.setAc(ac != null ? Integer.parseInt(ac) : null);
					} else if (fi.getFieldName().equals("cartype")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String cartype = new String(str, "UTF8");
						ride.setCartype(cartype != null ? Integer.parseInt(cartype) : null);
					} else if (fi.getFieldName().equals("vehicleNo")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String vehilceNo = new String(str, "UTF8");
						ride.setVehicleno(vehilceNo);
					} else if (fi.getFieldName().equals("vehicleColor")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String vehilceColor = new String(str, "UTF8");
						ride.setVehiclecolor(vehilceColor);
					} else if (fi.getFieldName().equals("licenseNo")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String licenseNo = new String(str, "UTF8");
						ride.setLicenseno(licenseNo);
					} else if (fi.getFieldName().equals("model")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String model = new String(str, "UTF8");
						ride.setModel(model);
					} else if (fi.getFieldName().equals("rideId")) {
						byte[] str = new byte[in.available()];
						in.read(str);
						String rideId = new String(str, "UTF8");
						ride.setId(Integer.parseInt(rideId));
					}
				}
			}
		}

		ride.setOrigin(twoWayStartLoc);
		ride.setDestination(twoWayEndLoc);
		ride.setForwardcheckpoints(twoWayForwardCheckpoints);
		ride.setBackwardcheckpoints(twoWayBackwardCheckpoints);

		Createdrides oldRide = RidesDAO.getRide(ride.getId());
		oldRide.setRidetype(ride.getRidetype());
		oldRide.setStartdate(ride.getStartdate());
		oldRide.setEnddate(ride.getEnddate());
		oldRide.setOrigin(ride.getOrigin());
		oldRide.setDestination(ride.getDestination());
		oldRide.setForwardcheckpoints(ride.getForwardcheckpoints());
		oldRide.setBackwardcheckpoints(ride.getBackwardcheckpoints());
		oldRide.setStarttime(ride.getStarttime());
		oldRide.setReturntime(ride.getReturntime());
		oldRide.setCartype(ride.getCartype());
		oldRide.setAc(ride.getAc());
		oldRide.setWheels(ride.getWheels());
		oldRide.setVehicleno(ride.getVehicleno());
		oldRide.setVehiclecolor(ride.getVehiclecolor());
		oldRide.setLicenseno(ride.getLicenseno());
		oldRide.setModel(ride.getModel());
		oldRide.setPhoto(ride.getPhoto());
		Transaction tx = Database.getInstance().getSession().beginTransaction();
		Database.getInstance().getSession().saveOrUpdate(oldRide);
		tx.commit();
		response.sendRedirect("viewRides?rideUpdated=true");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
