package commutetogether.servlets;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.data.Database;
import commutetogether.data.User;
import commutetogether.util.validator.Validator;

public class UpdateSettings extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public UpdateSettings() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = (User) request.getSession().getAttribute("user");
	user = Database.getInstance().findUserByEmail(user.getEmail());
	Validator validator = new Validator();
	RequestDispatcher rd = null;
	String email = request.getParameter("email");
	String currentPwd = request.getParameter("currentPwd");
	String newPwd = request.getParameter("newPwd");
	String confirmPwd = request.getParameter("confirmPwd");
	String mobile = request.getParameter("mobile");
	String deactivateReason = request.getParameter("deactivateReason");
	String deactivateMore = request.getParameter("deactivateMore");
	String deleteReason = request.getParameter("deleteReason");
	String deleteMore = request.getParameter("deleteMore");
	String delYes = request.getParameter("delYes");
	String emailYes = request.getParameter("emailYes");
	String passwordYes = request.getParameter("passwordYes");
	String mobileYes = request.getParameter("mobileYes");
	String deacYes = request.getParameter("deacYes");

	if (emailYes != null) {
	    Validator v1 = new Validator();
	    v1.validateEmail(email, true);
	    if (v1.isAllClear()) {
		user.setChangeemailkey(UUID.randomUUID().toString());
		if (Database.getInstance().findUserByEmail(email) == null) {
		    String to = email;

		    Properties props = new Properties();
		    props.put("mail.smtp.host", "smtp.gmail.com");
		    props.put("mail.smtp.socketFactory.port", "465");
		    props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		    props.put("mail.smtp.auth", "true");
		    props.put("mail.smtp.port", "465");

		    Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
			    return new PasswordAuthentication("kensiumtech@gmail.com", "kensium123");
			}
		    });

		    try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("kensiumtech@gmail.com"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Commute Together - Change Email Request");

			String uri = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
			message.setText("Please click <a href='" + uri + "/changeEmail?oldEmail=" + user.getEmail() + "&newEmail=" + email
				+ "&changeemailkey=" + user.getChangeemailkey() + "'>here</a> to change your email address on Commute Together.",
				"utf-8", "html");

			Transport.send(message);
		    } catch (MessagingException e) {
			throw new RuntimeException(e);
		    }
		    validator.hasErrors();
		    validator
			    .getErrors()
			    .put("email",
				    "An email has been sent to the new email address. Please click on the link in the email to change your email address to the new one.");
		    request.setAttribute("errors", validator.getErrors());
		}
	    } else {
		request.setAttribute("errors", v1.getErrors());
	    }
	    rd = request.getRequestDispatcher("accountSettings.jsp");
	} else if (passwordYes != null) {
	    if (!currentPwd.equals(user.getPassword())) {
		validator.hasErrors();
		validator.getErrors().put("password", "Current password is incorrect.");
	    } else {
		if (newPwd.equals(currentPwd)) {
		    validator.hasErrors();
		    validator.getErrors().put("password", "Current password and new password should not be the same.");
		} else if (newPwd.equals(confirmPwd)) {
		    user.setPassword(newPwd);
		} else {
		    validator.hasErrors();
		    validator.getErrors().put("password", "New password and confirm password does not match.");
		}

	    }
	    request.setAttribute("errors", validator.getErrors());
	    rd = request.getRequestDispatcher("accountSettings.jsp");
	} else if (mobileYes != null) {
	    validator.validateMobile(mobile);
	    if (!validator.isAllClear()) {
		request.setAttribute("errors", validator.getErrors());
	    } else {
		user.setMobile(mobile);
	    }
	    rd = request.getRequestDispatcher("accountSettings.jsp");
	} else if (deacYes != null) {
	    user.setDeactivateMore(deactivateMore);
	    user.setDeactivateReason(Integer.parseInt(deactivateReason));
	    rd = request.getRequestDispatcher("index.jsp");
	    Cookie cookie = new Cookie("rememberMe", null);
	    cookie.setMaxAge(0);
	    response.addCookie(cookie);
	} else if (delYes != null) {
	    Database.getInstance().delete(user);
	    Cookie cookie = new Cookie("rememberMe", null);
	    cookie.setMaxAge(0);
	    response.addCookie(cookie);
	    rd = request.getRequestDispatcher("index.jsp");
	}
	if (delYes == null) {
	    Database.getInstance().update(user);
	}
	request.getSession().setAttribute("user", user);
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
