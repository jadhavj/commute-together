package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.MessagesDAO;
import commutetogether.data.Database;
import commutetogether.data.Messages;
import commutetogether.models.Message;

public class ViewMessage extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ViewMessage() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String outbox = request.getParameter("outbox");
	Messages messages = MessagesDAO.findMessageById(request.getParameter("id"));
	if (outbox == null || (outbox != null && outbox.equals("false"))) {
	    messages.setIsread(1);
	    Database.getInstance().update(messages);
	}
	Message message = new Message(messages);
	request.setAttribute("message", message);
	RequestDispatcher rd = request.getRequestDispatcher("viewMessage.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
