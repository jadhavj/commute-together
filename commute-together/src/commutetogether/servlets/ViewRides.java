package commutetogether.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import commutetogether.dao.RidesDAO;
import commutetogether.data.User;

public class ViewRides extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ViewRides() {
	super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	User user = (User) request.getSession().getAttribute("user");
	request.setAttribute("created", RidesDAO.getRidesCreatedBy(user.getEmail()));
	request.setAttribute("joined", RidesDAO.getRidesJoinedBy(user.getEmail()));
	RequestDispatcher rd = request.getRequestDispatcher("viewRides.jsp");
	rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
    }

}
