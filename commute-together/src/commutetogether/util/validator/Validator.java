package commutetogether.util.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import commutetogether.data.Database;
import commutetogether.data.User;

public class Validator {

    private Map<String, String> errors = new LinkedHashMap<String, String>();
    private boolean allClear = true;

    public Validator() {
    };

    public void validateEmail(String email, boolean checkExisting) {
	EmailValidator vEmail = new EmailValidator();
	if (!vEmail.validate(email)) {
	    allClear = false;
	    errors.put("email", "Email is in invalid format.");
	    return;
	}
	if (checkExisting) {
	    User user = Database.getInstance().findUserByEmail(email);
	    if (user != null) {
		allClear = false;
		errors.put("email", "This email is already registered with us.");
	    }
	}
    }

    public void validatePassword(String email, String password) {
	if (password != null && !password.isEmpty()) {
	    User user = Database.getInstance().findUserByEmail(email);
	    if (user != null) {
		if (user.getPassword().equals(password)) {
		    return;
		}
	    }
	}
	allClear = false;
	errors.put("login", "Either of the Email or Password you entered is incorrect.");
    }

    public void validateDob(String dob) {
	SimpleDateFormat dobFormat = new SimpleDateFormat("dd/MM/yyyy");
	if (dob == null || dob.isEmpty()) {
	    allClear = false;
	    errors.put("dob", "DOB is invalid.");
	    return;
	} else {
	    try {
		Date dobDate = dobFormat.parse(dob);
		Calendar back18Years = Calendar.getInstance();
		back18Years.add(Calendar.YEAR, -18);
		if (dobDate.after(back18Years.getTime())) {
		    allClear = false;
		    errors.put("dob", "You must be at least 18 year old to register.");
		}
	    } catch (ParseException e) {
		allClear = false;
		errors.put("dob", "DOB is invalid.");
	    }
	}
    }

    public void validateMobile(String mobile) {
	if (mobile == null || mobile.isEmpty() || mobile.length() != 10) {
	    allClear = false;
	    errors.put("mobile", "Mobile no. is invalid.");
	}
	if (mobile.length() != 10) {
	    allClear = false;
	    errors.put("mobile", "Mobile no. should be of 10 digits.");
	}
    }

    public void validateIdProof(String idProof) {
	if (idProof == null || idProof.isEmpty()) {
	    allClear = false;
	    errors.put("idproof", "ID Proof is invalid.");
	}
    }

    public void validateEmergency(String emergency) {
	if (emergency == null || emergency.isEmpty()) {
	    allClear = false;
	    errors.put("emergency", "Emergency contact no. is invalid.");
	}
	if (emergency.length() != 10) {
	    allClear = false;
	    errors.put("emergency", "Emergency contact no. should be of 10 digits.");
	}
    }

    public void validateInsurance(String insurance) {
	if (insurance == null || insurance.isEmpty()) {
	    allClear = false;
	    errors.put("insurance", "Insurance no. is invalid.");
	}
    }

    public Map<String, String> getErrors() {
	return errors;
    }

    public void setErrors(Map<String, String> errors) {
	this.errors = errors;
    }

    public boolean isAllClear() {
	return allClear;
    }

    public void reset() {
	allClear = true;
	errors.clear();
    }

    public void hasErrors() {
	allClear = false;
    }
}
